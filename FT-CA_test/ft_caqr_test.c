#include "ft_caqr.h"

void write_times(int M,int N,MPI_data *mpid,char *option);

int main(int argc,char *argv[]){
	if(argc!=5){
		fprintf(stderr,"Uso: %s M N q_calc input_file\n",argv[0]);
		return(EXIT_FAILURE);
	}
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int q_c=(atoi(argv[3])!=0);
	char in[strlen(argv[4])];
	memcpy(in,argv[4],strlen(argv[4])*sizeof(char));
//	INICIALIZA AMBIENTE MPI (EN TODOS LOS NODOS)
	MPI_data *mpid=MPI_data_init(&argc,&argv);
	if(mpid==NULL){
		fprintf(stderr,"Proceso desconocido: Error al inicializar ambiente MPI\n");
		return(EXIT_FAILURE);
	}
	if(mpid->world_size<1){
		fprintf(stderr,"Proceso [%d] en [%s]: Numero de procesos inferior a 1\n",mpid->world_rank,mpid->processor_name);
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	stage=STAGE_COMM_DIVISION;
	currCol=currRow=0;
	int ret;
	init_variables_qr(q_c,mpid->world_size);
	init_timers(mpid->world_size);
	if(mpid->spawned){
		time_rest=MPI_Wtime();
//		RECIBO FILA/COLUNMA DE INICIO
		ret=safe_receive_times((void *)(&currCol),1,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			fprintf(stderr,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al recibir fila/columna actual\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			return(EXIT_FAILURE);
		}
		currRow=currCol;
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibido fila/columna actual [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//		RECIBO ETAPA DE INICIO
		ret=safe_receive_times((void *)(&stage),1,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			fprintf(stderr,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al recibir etapa actual\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			return(EXIT_FAILURE);
		}
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibido etapa actual [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,stage);
#endif /* _VERB_ */
		time_rest_final+=MPI_Wtime()-time_rest;
	}
#ifndef _OMPI_
	while(1){
//		usleep(WAIT_TIME*mpid->world_rank);
//		printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Restaurando grid:[%dx%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,rowM,colN);
		ret=create_grid_process(mpid,&cart_2D,&col_comm,&row_comm,&rowM,&colN,&rowNum,&colNum);
		if(ret!=MPI_SUCCESS){
			verify_error(mpid,&cart_2D,&col_comm,&row_comm,&rowM,&colN,&rowNum,&colNum,0,'R');
			continue;
		}
//		printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Grid:[%dx%d] restaurado\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,rowM,colN);
		break;
	}
#else
	ret=create_grid_process(mpid,&cart_2D,&col_comm,&row_comm,&rowM,&colN,&rowNum,&colNum);
	if(ret!=MPI_SUCCESS){
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
#endif /* _OMPI_ */
//	TODOS LOS PROCESOS GENERAN DATOS INICIALES
	if(mpid->spawned){
		time_rest=MPI_Wtime();
		md_local=matrix_data_init(M/rowM,N/colN,M/rowM,N/colN,1,'R');
		read_subblock(md_local,mpid,rowNum,colNum,"FTCAQR",'R');
		time_rest_final+=MPI_Wtime()-time_rest;
	}
	else if(strcmp(in,"random")==0){
		md_local=matrix_data_init_with_random(M/rowM,N/colN,M/rowM,N/colN,mpid->world_rank,1,'R');
		write_subblock(md_local->A,md_local->M,md_local->N,mpid,rowNum,colNum,"FTCAQR",'R');
	}
	else
		md_local=matrix_data_init_with_file(M,N,M/rowM,N/colN,in,1,colNum*rowM+rowNum);
//		md_local=matrix_data_init_with_file(M/rowM,N/colN,M/rowM,N/colN,in,1);
	if(md_local==NULL){
		fprintf(stderr,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al inicializar subbloque inicial\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
		free_timers(mpid->world_size);
		MPI_Comm_free(&col_comm);
		MPI_Comm_free(&row_comm);
		MPI_Comm_free(&cart_2D);
//		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	panelN=md_local->Nb;
//	INICIALIZA AMBIENTE PARA EJECUTAR FT-TSQR/FT-CAQR
	fttsqr_data *ftd=(fttsqr_data *)malloc(sizeof(fttsqr_data));
	ret=ft_caqr(ftd,mpid,M,N,q_c,in,argc,argv);
//	SI OCURRIO ALGUN ERROR EN LA RECEPCION
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al ejecutar FT-CAQR\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
		MPI_Comm_free(&col_comm);
		MPI_Comm_free(&row_comm);
		MPI_Comm_free(&cart_2D);
		return(EXIT_FAILURE);
	}
	else{
//		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: grid:[%d,%d] - Termino\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
		write_times(M,N,mpid,"FTCAQR");
		free_timers(mpid->world_size);
//		free_fttsqr_data(ftd);
//		MPI_Comm_free(&col_comm);
//		MPI_Comm_free(&row_comm);
//		MPI_Comm_free(&cart_2D);
		MPI_Finalize();
		return(EXIT_SUCCESS);
	}
}

void write_times(int M,int N,MPI_data *mpid,char *option){
	int i;
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char q_c[3];
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	sprintf(q_c,"%s",(q_calc) ? "QR" : "R");
	sprintf(filename,"[%s][%s][%s][%dx%d][%d]_Process[%d]_Times.res",option,compilation,q_c,M,N,mpid->world_size,mpid->world_rank);
	FILE *out=fopen(filename,"w");
	if(out==NULL)
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
	else{
		fprintf(out,"exec: [%.6f] seg.\n",time_exec_final);
		fprintf(out,"tsqr: [%.6f] seg.\n",time_tsqr_final);
		fprintf(out,"mult: [%.6f] seg.\n",time_mult_final);
		fprintf(out,"solv: [%.6f] seg.\n",time_solv_final);
		fprintf(out,"inv: [%.6f] seg.\n",time_inv_final);
		fprintf(out,"add: [%.6f] seg.\n",time_add_final);
		fprintf(out,"swap: [%.6f] seg.\n",time_swap_final);
		fprintf(out,"rest: [%.6f] seg.\n",time_rest_final);
		fprintf(out,"comm: [%.6f] seg.\n",time_comm_final);
		fprintf(out,"copy: [%.6f] seg.\n",time_copy_final);
		for(i=0;i<step_lim;i++)
			fprintf(out,"step[%d]: [%.6f] seg.\n",i,time_steps[i]);
		fprintf(out,"caqr: [%.6f] seg.\n",time_caqr_final);
		fprintf(out,"exec_tm: [%.6f] seg.\n",time_exec_tm_final);
		fprintf(out,"read: [%.6f] seg.\n",time_read_final);
		fprintf(out,"write: [%.6f] seg.\n",time_write_final);
		fclose(out);
	}
}

