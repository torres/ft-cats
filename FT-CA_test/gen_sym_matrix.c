//-D_FILE_OFFSET_BITS=64
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

void mult_matrix_scalar(double *A,int MA,int NA,double scalar);
void add_matrix(double *A,double *B,int M,int N,char op);
void rand_matrix(double *A,int M,int N,int r);
double *rand_symmetric_matrix(int M,int N,int r);
int random_number();
void print_matrix(double *A,int M,int N);

void dgemm_(char *TRANSA,char *TRANSB,int *M,int *N,int *K,double *ALPHA,double *A,int *LDA,double *B,int *LDB,double *BETA,double *C,int *LDC);
void daxpy_(int *N,double *DA,double *DX,int *INCX,double *DY,int *INCY);
void dscal_(int *N,double *DA,double *DX,int *INCX);
void dlarnv_(int *IDIST,int *ISEED,int *N,double *X);

int main(int argc,char *argv[]){
	if(argc!=4){
		fprintf(stderr,"Uso: %s [M] [N] [archivo]\n",argv[0]);
		return(-1);
	}
	srand(time(NULL));
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	FILE *out=fopen(argv[3],"w");
	if(out==NULL){
		fprintf(stderr,"Error al crear el archivo [%s]\n",argv[3]);
		return(-2);
	}
	srand(time(NULL));
	double *A=rand_symmetric_matrix(M,N,rand());
//	print_matrix(A,M,N);
	fwrite(A,sizeof(double),M*N,out);
	fclose(out);
	return(0);
}

void mult_matrix_scalar(double *A,int MA,int NA,double scalar){
	int MN=MA*NA;
	int INCX=1;
	dscal_(&MN,&scalar,A,&INCX);
}

void add_matrix(double *A,double *B,int M,int N,char op){
	double DA=1.0;
	int INCX=1;
	int INCY=1;
	int MN=M*N;
	if(op=='S'){
		mult_matrix_scalar(B,M,N,-1.0f);
	}
	daxpy_(&MN,&DA,A,&INCX,B,&INCY);
}

void rand_matrix(double *A,int M,int N,int r){
	int seed[4]={0,0,0,2*r+1};
	int ONE=1;
	int MN=M*N;
	dlarnv_(&ONE,seed,&MN,A);
}

double *rand_symmetric_matrix(int M,int N,int r){
	int i,j;
	double *A=(double *)calloc(M*N,sizeof(double));
	double *At=(double *)calloc(M*N,sizeof(double));
	srand(time(NULL));
	rand_matrix(A,M,N,r+random_number()*rand());
	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			At[j*M+i]=A[i*M+j];
		}
	}
	add_matrix(A,At,M,N,'A');
	mult_matrix_scalar(At,M,N,0.5f);
	free(A);
	for(i=0;i<M;i++)
		At[i*M+i]+=M;
	return(At);
}

int random_number(){
	int i;
	int SIZE=100;
	int IDIST=1;
	int SEED_SIZE=4;
	int SEED[SEED_SIZE];
	int SEED_MAX=4096;
	int sum=0;
	for(i=0;i<SEED_SIZE;i++)
		SEED[i]=rand()%SEED_MAX;
	SEED[SEED_SIZE-1]=SEED[SEED_SIZE-1]+((SEED[SEED_SIZE-1]&1) ? 0 : 1);
	double *X=(double *)malloc(SIZE*sizeof(double));
	dlarnv_(&IDIST,SEED,&SIZE,X);
	for(i=0;i<SIZE;i++)
		sum+=(int)floor(X[i]*SIZE*SEED_MAX);
	free(X);
	return(sum%SIZE);
}

void print_matrix(double *A,int M,int N){
	int i,j;
	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			if(A[j*M+i]>=0.0)
				printf("+");
			printf("%.4lf ",A[j*M+i]);
		}
		printf("\n");
	}
}

