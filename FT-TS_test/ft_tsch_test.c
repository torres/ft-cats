#include "ft_tsch.h"

void write_times(matrix_data *md,MPI_data *mpid,char *option);

int main(int argc,char *argv[]){
	if(argc!=4){
		fprintf(stderr,"Uso: %s M N input_file\n",argv[0]);
		return(EXIT_FAILURE);
	}
	fttsch_data *ftd=(fttsch_data *)malloc(sizeof(fttsch_data));
//	INICIALIZA AMBIENTE MPI (EN TODOS LOS NODOS)
	ftd->mpid=MPI_data_init(&argc,&argv);
	if(ftd->mpid==NULL){
		fprintf(stderr,"Proceso desconocido: Error al inicializar ambiente MPI\n");
		free_fttsch_data(ftd);
		return(EXIT_FAILURE);
	}
	if(ftd->mpid->world_size<1){
		fprintf(stderr,"Proceso [%d] en [%s]: Numero de procesos inferior a 1\n",ftd->mpid->world_rank,ftd->mpid->processor_name);
		free_fttsch_data(ftd);
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	int ret;
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int slen=strlen(argv[3]);
	char in[slen];
	memcpy(in,argv[3],slen*sizeof(char));
	in[slen]='\0';
//	TODOS LOS PROCESOS OBTIENEN DATOS INICIALES
	if(ftd->mpid->spawned)
		ftd->md=matrix_data_init(M,N,M/ftd->mpid->world_size,N,1,'L');
	else if(strcmp(in,"random")==0)
		ftd->md=matrix_data_init_with_random_symmetric(M,N,M/ftd->mpid->world_size,N,ftd->mpid->world_rank,1);
	else
		ftd->md=matrix_data_init_with_file_symmetric(M,N,M/ftd->mpid->world_size,N,in,1,-1);
	if(ftd->md==NULL){
		fprintf(stderr,"Proceso [%d] en [%s]: Error al inicializar datos\n",ftd->mpid->world_rank,ftd->mpid->processor_name);
		free_fttsch_data(ftd);
		MPI_Finalize();
		return(EXIT_FAILURE);
	}
	init_timers(ftd->mpid->world_size);
//#ifndef _OMPI_
/*	if(ftd->mpid->spawned){
		time_rest=MPI_Wtime();
		ret=restore_data(ftd);
		time_rest_final+=MPI_Wtime()-time_rest;
		if(ret!=MPI_SUCCESS){
			fprintf(stderr,"Proceso [%d] en [%s]: Error al restaurar datos\n",ftd->mpid->world_rank,ftd->mpid->processor_name);
			free_timers(ftd->mpid->world_size);
			free_fttsch_data(ftd);
			MPI_Finalize();
			return(EXIT_FAILURE);
		}
	}*/
//#endif /* _OMPI_ */
//-------------INFORMATIVO----------------------------------------------------------------------------------------
/*	usleep(WAIT_TIME*ftd->mpid->world_rank*2);
	printf("Proceso [%d] en [%s]: A\n",ftd->mpid->world_rank,ftd->mpid->processor_name);
	print_matrix_data(ftd->md);*/
//-------------INFORMATIVO----------------------------------------------------------------------------------------

//	EJECUCION DE TSCH
	ret=ft_tsch_init(ftd,ftd->md->A,ftd->md->M,ftd->md->N,in,argc,argv);
	ret=ft_tsch(ftd);
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: Error al ejecutar tsch\n",ftd->mpid->world_rank,ftd->mpid->processor_name);
		free_timers(ftd->mpid->world_size);
		free_fttsch_data(ftd);
		return(EXIT_FAILURE);
	}
//-------------INFORMATIVO----------------------------------------------------------------------------------------
	usleep(WAIT_TIME*ftd->mpid->world_rank);
	printf("Proceso [%d] en [%s]: Termino\n",ftd->mpid->world_rank,ftd->mpid->processor_name);
//	if(ftd->mpid->world_rank==0){
//		print_matrix_data(ftd->md);
//	}
//-------------INFORMATIVO----------------------------------------------------------------------------------------
//	ESCRIBE ARCHIVOS DE RESULTADO
//	write_results(ftd->md,ftd->mpid);
	write_times(ftd->md,ftd->mpid,"FTTSCH");
	MPI_Barrier(ftd->mpid->world);
//	LIBERA RECURSOS
	free_timers(ftd->mpid->world_size);
//	free_fttsch_data(ftd);
	MPI_Finalize();
	return(EXIT_SUCCESS);
}

void write_times(matrix_data *md,MPI_data *mpid,char *option){
	int i;
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char l_c[3]="LL\0";
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	sprintf(filename,"[%s][%s][%s][%dx%d][%d]_Process[%d]_Times.res",option,compilation,l_c,md->M,md->N,mpid->world_size,mpid->world_rank);
	FILE *out=fopen(filename,"w");
	if(out==NULL)
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
	else{
		fprintf(out,"exec: [%.6f] seg.\n",time_exec_final);
		fprintf(out,"tsch: [%.6f] seg.\n",time_tsch_final);
		fprintf(out,"mult: [%.6f] seg.\n",time_mult_final);
		fprintf(out,"solv: [%.6f] seg.\n",time_solv_final);
		fprintf(out,"inv: [%.6f] seg.\n",time_inv_final);
		fprintf(out,"add: [%.6f] seg.\n",time_add_final);
		fprintf(out,"swap: [%.6f] seg.\n",time_swap_final);
		fprintf(out,"rest: [%.6f] seg.\n",time_rest_final);
		fprintf(out,"comm: [%.6f] seg.\n",time_comm_final);
		fprintf(out,"copy: [%.6f] seg.\n",time_copy_final);
		for(i=0;i<step_lim;i++)
			fprintf(out,"step[%d]: [%.6f] seg.\n",i,time_steps[i]);
		fclose(out);
	}
}

