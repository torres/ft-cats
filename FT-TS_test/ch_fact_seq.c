#include "ft_tsch.h"

void write_times(matrix_data *md);

int main(int argc,char *argv[]){
	if(argc!=4){
		fprintf(stderr,"Uso: %s M N input_file\n",argv[0]);
		return(EXIT_FAILURE);
	}
	fttsch_data *ftd=(fttsch_data *)malloc(sizeof(fttsch_data));

	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	char in[strlen(argv[3])];
	memcpy(in,argv[3],strlen(argv[3])*sizeof(char));
	if(strcmp(in,"random")==0)
		ftd->md=matrix_data_init_with_random_symmetric(M,N,M,N,0,0);
	else
		ftd->md=matrix_data_init_with_file_symmetric(M,N,M,N,in,0,-1);
	if(ftd->md==NULL){
		fprintf(stderr,"Error al inicializar datos\n");
		free_fttsch_data(ftd);
		return(EXIT_FAILURE);
	}
	init_timers(1);
	time_exec=MPI_Wtime();
	ch_matrix(ftd->md->A,N);
//	set_L(ftd->md);
	time_exec_final+=MPI_Wtime()-time_exec;
	printf("Proceso [0]: Termino\n");
//	ESCRIBE ARCHIVOS DE RESULTADO
	write_times(ftd->md);
	free_fttsch_data(ftd);
	return(EXIT_SUCCESS);
}

void write_times(matrix_data *md){
	int i;
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char l_c[3]="LL\0";
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	sprintf(filename,"[%s][%s][%s][%dx%d][%d]_Process[%d]_Times.res","DPOTRF","SEQ",l_c,md->M,md->N,1,0);
	FILE *out=fopen(filename,"w");
	if(out==NULL)
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
	else{
		fprintf(out,"exec: [%.6f] seg.\n",time_exec_final);
		fprintf(out,"tsch: [%.6f] seg.\n",time_tsch_final);
		fprintf(out,"mult: [%.6f] seg.\n",time_mult_final);
		fprintf(out,"solv: [%.6f] seg.\n",time_solv_final);
		fprintf(out,"inv: [%.6f] seg.\n",time_inv_final);
		fprintf(out,"add: [%.6f] seg.\n",time_add_final);
		fprintf(out,"swap: [%.6f] seg.\n",time_swap_final);
		fprintf(out,"rest: [%.6f] seg.\n",time_rest_final);
		fprintf(out,"comm: [%.6f] seg.\n",time_comm_final);
		fprintf(out,"copy: [%.6f] seg.\n",time_copy_final);
		for(i=0;i<step_lim;i++)
			fprintf(out,"step[%d]: [%.6f] seg.\n",i,time_steps[i]);
		fclose(out);
	}
}

