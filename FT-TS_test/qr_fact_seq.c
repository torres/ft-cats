#include "ft_tsqr.h"

void write_times(matrix_data *md);

int main(int argc,char *argv[]){
	if(argc!=5){
		fprintf(stderr,"Uso: %s M N q_calc input_file\n",argv[0]);
		return(EXIT_FAILURE);
	}
	fttsqr_data *ftd=(fttsqr_data *)malloc(sizeof(fttsqr_data));

	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int q_c=(atoi(argv[3])!=0);
	char in[strlen(argv[4])];
	memcpy(in,argv[4],strlen(argv[4])*sizeof(char));
	if(strcmp(in,"random")==0)
		ftd->md=matrix_data_init_with_random(M,N,M,N,0,0,'R');
	else
		ftd->md=matrix_data_init_with_file_block(M,N,M,N,in,0,-1,'R');
	if(ftd->md==NULL){
		fprintf(stderr,"Error al inicializar datos\n");
		free_fttsqr_data(ftd);
		return(EXIT_FAILURE);
	}
	init_timers(1);
	time_exec=MPI_Wtime();
	qr_matrix(ftd->md->A,M,N,ftd->md->TAU,ftd->md->WORK);
	set_R(ftd->md);
	time_exec_final+=MPI_Wtime()-time_exec;
	printf("Proceso [0]: Termino\n");
//	ESCRIBE ARCHIVOS DE RESULTADO
	write_times(ftd->md);
	free_fttsqr_data(ftd);
	return(EXIT_SUCCESS);
}

void write_times(matrix_data *md){
	int i;
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char q_c[3];
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	sprintf(q_c,"%s",(q_calc) ? "QR" : "R");
	sprintf(filename,"[%s][%s][%s][%dx%d][%d]_Process[%d]_Times.res","DGEQRF","SEQ",q_c,md->M,md->N,1,0);
	FILE *out=fopen(filename,"w");
	if(out==NULL)
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
	else{
		fprintf(out,"exec: [%.6f] seg.\n",time_exec_final);
		fprintf(out,"tsqr: [%.6f] seg.\n",time_tsqr_final);
		fprintf(out,"mult: [%.6f] seg.\n",time_mult_final);
		fprintf(out,"solv: [%.6f] seg.\n",time_solv_final);
		fprintf(out,"inv: [%.6f] seg.\n",time_inv_final);
		fprintf(out,"add: [%.6f] seg.\n",time_add_final);
		fprintf(out,"swap: [%.6f] seg.\n",time_swap_final);
		fprintf(out,"rest: [%.6f] seg.\n",time_rest_final);
		fprintf(out,"comm: [%.6f] seg.\n",time_comm_final);
		fprintf(out,"copy: [%.6f] seg.\n",time_copy_final);
		for(i=0;i<step_lim;i++)
			fprintf(out,"step[%d]: [%.6f] seg.\n",i,time_steps[i]);
		fclose(out);
	}
}

