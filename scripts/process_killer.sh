#! /bin/bash

if [ "$#" -ne 2 ]; then
	echo "Uso: process_killer.sh -p [process_name]"
	exit 1
fi

while getopts p: option
do
	case "${option}"
	in
		p) PROCESS=${OPTARG};;
	esac
done

while :
do
	PIDS=$(ps -aux | grep "./${PROCESS}" | grep -v "mpiexec" | grep -v "bash" | grep -v "grep" | awk -F " " '{print $2}')
	if [ -z "$PIDS" ]; then
		echo "No ${PROCESS} detected"
		sleep 1
		continue
	fi
	echo "${PROCESS} detected: ${PIDS//$'\n'/ }"
	for PID in $PIDS
	do
		RAND=$(( ( RANDOM % 100 ) ))
		if [ "$RAND" -eq 0 ]; then
			echo "Killing process with PID [$PID]"
			kill -9 $PID
			read -n1 -r -p "Continue killing? Yes(space bar) / No(something else)" key
			if [ "$key" = '' ]; then
				break
			else
				exit 0
			fi
		fi
	done
done

