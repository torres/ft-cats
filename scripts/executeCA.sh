#! /bin/bash

if [ "$#" -ne 20 ]; then
	echo "Uso: executeCA.sh -F [U | R | L] -f [inputfile | random] -M [M] -N [N] -L [true | false] -P [max_process] -c [OMPI | ULFM] -o [true | false] -V [true | false] -h [hostfile]"
	exit 1
fi

while getopts F:f:M:N:L:P:c:o:V:h: option
do
	case "${option}"
	in
		F) FACT=${OPTARG};;
		f) INPUT_FILE=${OPTARG};;
		M) M=${OPTARG};;
		N) N=${OPTARG};;
		L) LCALC=${OPTARG};;
		P) MAX_PROCESS=${OPTARG};;
		c) COMPILE=${OPTARG};;
		o) OVER=${OPTARG};;
		V) VERB=${OPTARG};;
		h) HOST=${OPTARG};;
	esac
done

#--------------------------------------------------PREPARE ARGUMENTS AND OPTIONS
if [ "$LCALC" = "true" ]; then
	L="1"
	if [ "$FACT" = "U" ]; then
		LOWER="LU"
	elif [ "$FACT" = "R" ]; then
		LOWER="QR"
	else
		LOWER="LL"
	fi
else
	L="0"
	if [ "$FACT" = "U" ]; then
		LOWER="U"
	elif [ "$FACT" = "R" ]; then
		LOWER="R"
	else
		LOWER="L"
	fi
fi

if [ "$VERB" = "true" ]; then
	VERBOSE="VERB=-D_VERB_"
else
	VERBOSE=""
fi

if [ "$OVER" = "true" ]; then
	OVERSUBSCRIBE="--map-by :oversubscribe"
else
	OVERSUBSCRIBE=""
#	OVERSUBSCRIBE="--map-by node"
fi

EXTRA_FLAGS=""
#EXTRA_FLAGS="--mca btl_base_verbose 100"
#EXTRA_FLAGS="--mca btl tcp,vader,self"
if [ "$COMPILE" = "OMPI" ]; then
	OMPI_ULFM="OMPI=-D_OMPI_"
	EXTRA_FLAGS="${EXTRA_FLAGS} ${OVERSUBSCRIBE}"
else
	OMPI_ULFM="OMPI=-D_ULFM_"
	EXTRA_FLAGS="${EXTRA_FLAGS} ${OVERSUBSCRIBE} --with-ft mpi --mca mpi_ft_verbose 1"
#	EXTRA_FLAGS="${EXTRA_FLAGS} ${OVERSUBSCRIBE} --with-ft mpi"
fi

MAKE="${OMPI_ULFM} ${VERBOSE}"
#--------------------------------------------------GENERATE INCLUDE AND LIBRARY DIRECTORIES IF NECCESARY
INC_DIR=$PWD/include
LIB_DIR=$PWD/lib
if [ ! -d "$INC_DIR" ] || [ ! -d "$LIB_DIR" ]; then
	cd FT-TS/
#--------------------------------------------------COMPILE ACCORDING TO USER'S SELECTION
	make $MAKE
	make clean
	cd ..
	cd FT-CA/
	make $MAKE
	make clean
	cd ..
fi
cd FT-CA_test/
make $MAKE

#--------------------------------------------------EXECUTE
MPI_EXEC=mpiexec
N_PROCESS="--np ${MAX_PROCESS}"
HOSTFILE="--machinefile ${HOST}"
#HOSTFILE="--machinefile $OAR_FILE_NODES"

if [ "$FACT" = "U" ]; then
	EXECUTABLE=ft_calu_test
	ALG="FTCALU"
	ARGS="${M} ${N} ${L} ${INPUT_FILE}"
elif [ "$FACT" = "R" ]; then
	EXECUTABLE=ft_caqr_test
	ALG="FTCAQR"
	ARGS="${M} ${N} ${L} ${INPUT_FILE}"
else
	EXECUTABLE=ft_cach_test
	ALG="FTCACH"
	ARGS="${M} ${N} ${INPUT_FILE}"
fi

#VAL="valgrind --num-callers=50 --tool=memcheck --leak-check=yes --show-reachable=yes --track-origins=yes --log-file=%p"
#VAL="valgrind --num-callers=50 --tool=memcheck --leak-check=yes --show-reachable=yes --track-origins=yes"
#VAL="xterm -e gdb"

echo "********************************************************************"
echo "$MPI_EXEC $N_PROCESS $HOSTFILE $EXTRA_FLAGS $VAL ./$EXECUTABLE $ARGS"
time $MPI_EXEC $N_PROCESS $HOSTFILE $EXTRA_FLAGS $VAL ./$EXECUTABLE $ARGS
echo "********************************************************************"

#--------------------------------------------------PREPARE RESULTS AND FINISH
RES_DIR=$PWD/results
FILE_RES="*.res"

mkdir -p $RES_DIR/"[${ALG}][${COMPILE}][${LOWER}][${M}x${N}][${MAX_PROCESS}]_Processes"
mv $FILE_RES $RES_DIR/"[${ALG}][${COMPILE}][${LOWER}][${M}x${N}][${MAX_PROCESS}]_Processes"/

make clean
rm -rf $INC_DIR
rm -rf $LIB_DIR

