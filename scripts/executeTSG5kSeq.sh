#! /bin/bash

if [ "$#" -ne 12 ]; then
	echo "Uso: executeTSG5kSeq.sh -t [test_times] -F [U | R | L] -f [inputfile] -M [M] -N [N] -L [true | false]"
	exit 1
fi

while getopts t:F:f:M:N:L: option
do
	case "${option}"
	in
		t) TEST_TIMES=${OPTARG};;
		F) FACT=${OPTARG};;
		f) INPUT_FILE=${OPTARG};;
		M) M=${OPTARG};;
		N) N=${OPTARG};;
		L) LCALC=${OPTARG};;
	esac
done

if [ "$LCALC" = "true" ]; then
	L="1"
	if [ "$FACT" = "U" ]; then
		LOWER="LU"
	elif [ "$FACT" = "R" ]; then
		LOWER="QR"
	else
		LOWER="LL"
	fi
else
	L="0"
	if [ "$FACT" = "U" ]; then
		LOWER="U"
	elif [ "$FACT" = "R" ]; then
		LOWER="R"
	else
		LOWER="L"
	fi
fi

if [ "$FACT" = "U" ]; then
	EXECUTABLE=lu_fact_seq
	ALG="DGETRF"
	ARGS="${M} ${N} ${L} ${INPUT_FILE}"
elif [ "$FACT" = "R" ]; then
	EXECUTABLE=qr_fact_seq
	ALG="DGEQRF"
	ARGS="${M} ${N} ${L} ${INPUT_FILE}"
else
	EXECUTABLE=ch_fact_seq
	ALG="DPOTRF"
	ARGS="${M} ${N} ${INPUT_FILE}"
fi

MAKE="OMPI=-D_OMPI_ ${VERBOSE}"
#--------------------------------------------------GENERATE INCLUDE AND LIBRARY DIRECTORIES IF NECCESARY
INC_DIR=$PWD/include
LIB_DIR=$PWD/lib
if [ ! -d "$INC_DIR" ] || [ ! -d "$LIB_DIR" ]; then
	cd FT-TS/
	make $MAKE
	make clean
	cd ..
fi
cd FT-TS_test/
make $MAKE

RES_DIR=$PWD/results
FILE_RES="*.res"

for (( i = 0; i < TEST_TIMES; i++ )); do
	echo "********************************************************************"
	echo "./$EXECUTABLE ${ARGS}"
	time ./$EXECUTABLE $ARGS
	echo "********************************************************************"
	mkdir -p $RES_DIR/"[${ALG}][SEQ][${LOWER}][${M}x${N}][1]_Processes_execution[${i}]"
	mv $FILE_RES $RES_DIR/"[${ALG}][SEQ][${LOWER}][${M}x${N}][1]_Processes_execution[${i}]"/
done

make clean
rm -rf $INC_DIR
rm -rf $LIB_DIR

