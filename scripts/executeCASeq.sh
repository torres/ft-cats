#! /bin/bash

if [ "$#" -ne 10 ]; then
	echo "Uso: executeCASeq.sh -F [U | R | L] -f [inputfile | random] -M [M] -N [N] -L [true | false]"
	exit 1
fi

while getopts F:f:M:N:L: option
do
	case "${option}"
	in
		F) FACT=${OPTARG};;
		f) INPUT_FILE=${OPTARG};;
		M) M=${OPTARG};;
		N) N=${OPTARG};;
		L) LCALC=${OPTARG};;
	esac
done

#--------------------------------------------------PREPARE ARGUMENTS AND OPTIONS
if [ "$LCALC" = "true" ]; then
	L="1"
	if [ "$FACT" = "U" ]; then
		LOWER="LU"
	elif [ "$FACT" = "R" ]; then
		LOWER="QR"
	else
		LOWER="LL"
	fi
else
	L="0"
	if [ "$FACT" = "U" ]; then
		LOWER="U"
	elif [ "$FACT" = "R" ]; then
		LOWER="R"
	else
		LOWER="L"
	fi
fi

MAKE="${OMPI_ULFM} ${VERBOSE}"
#--------------------------------------------------GENERATE INCLUDE AND LIBRARY DIRECTORIES IF NECCESARY
INC_DIR=$PWD/include
LIB_DIR=$PWD/lib
if [ ! -d "$INC_DIR" ] || [ ! -d "$LIB_DIR" ]; then
	cd FT-TS/
#--------------------------------------------------COMPILE ACCORDING TO USER'S SELECTION
	make $MAKE
	make clean
	cd ..
	cd FT-CA/
	make $MAKE
	make clean
	cd ..
fi
cd FT-CA_test/
make $MAKE

#--------------------------------------------------EXECUTE
if [ "$FACT" = "U" ]; then
	EXECUTABLE=calu_seq_test
	ALG="DGETRF"
	ARGS="${M} ${N} ${L} ${INPUT_FILE}"
elif [ "$FACT" = "R" ]; then
	EXECUTABLE=caqr_seq_test
	ALG="DGEQRF"
	ARGS="${M} ${N} ${L} ${INPUT_FILE}"
else
	EXECUTABLE=cach_seq_test
	ALG="DPOTRF"
	ARGS="${M} ${N} ${INPUT_FILE}"
fi

echo "********************************************************************"
echo "./$EXECUTABLE $ARGS"
time ./$EXECUTABLE $ARGS
echo "********************************************************************"

#--------------------------------------------------PREPARE RESULTS AND FINISH
RES_DIR=$PWD/results
FILE_RES="*.res"

mkdir -p $RES_DIR/"[${ALG}][SEQ][${LOWER}][${M}x${N}][1]_Processes"
mv $FILE_RES $RES_DIR/"[${ALG}][SEQ][${LOWER}][${M}x${N}][1]_Processes"/

make clean
rm -rf $INC_DIR
rm -rf $LIB_DIR

