#! /bin/bash

if [ "$#" -ne 24 ]; then
	echo "Uso: executeTSG5k.sh -t [test_times] -F [U | R | L] -f [inputfile | random] -M [M] -N [N] -L [true | false] -p [min_process] -P [max_process] -c [OMPI | ULFM] -o [true | false] -V [true | false] -h [hostfile]"
	exit 1
fi

while getopts t:F:f:M:N:L:p:P:c:o:V:h: option
do
	case "${option}"
	in
		t) TEST_TIMES=${OPTARG};;
		F) FACT=${OPTARG};;
		f) INPUT_FILE=${OPTARG};;
		M) M=${OPTARG};;
		N) N=${OPTARG};;
		L) LCALC=${OPTARG};;
		p) MIN_PROCESS=${OPTARG};;
		P) MAX_PROCESS=${OPTARG};;
		c) COMPILE=${OPTARG};;
		o) OVER=${OPTARG};;
		V) VERB=${OPTARG};;
		h) HOST=${OPTARG};;
	esac
done

#--------------------------------------------------PREPARE ARGUMENTS AND OPTIONS
if [ "$LCALC" = "true" ]; then
	L="1"
	if [ "$FACT" = "U" ]; then
		LOWER="LU"
	elif [ "$FACT" = "R" ]; then
		LOWER="QR"
	else
		LOWER="LL"
	fi
else
	L="0"
	if [ "$FACT" = "U" ]; then
		LOWER="U"
	elif [ "$FACT" = "R" ]; then
		LOWER="R"
	else
		LOWER="L"
	fi
fi

if [ "$VERB" = "true" ]; then
	VERBOSE="VERB=-D_VERB_"
else
	VERBOSE=""
fi

if [ "$OVER" = "true" ]; then
	OVERSUBSCRIBE="--map-by :oversubscribe"
else
	OVERSUBSCRIBE=""
#	OVERSUBSCRIBE="--map-by node"
fi


EXTRA_FLAGS=""
#EXTRA_FLAGS="--mca btl_base_verbose 100"
#EXTRA_FLAGS="--mca btl tcp,vader,self"
if [ "$COMPILE" = "OMPI" ]; then
	OMPI_ULFM="OMPI=-D_OMPI_"
	EXTRA_FLAGS="${EXTRA_FLAGS} ${OVERSUBSCRIBE}"
else
	OMPI_ULFM="OMPI=-D_ULFM_"
	EXTRA_FLAGS="${EXTRA_FLAGS} ${OVERSUBSCRIBE} --with-ft mpi --mca mpi_ft_verbose 1"
#	EXTRA_FLAGS="${EXTRA_FLAGS} ${OVERSUBSCRIBE} --with-ft mpi"
fi

MAKE="${OMPI_ULFM} ${VERBOSE}"
#--------------------------------------------------GENERATE INCLUDE AND LIBRARY DIRECTORIES IF NECCESARY
INC_DIR=$PWD/include
LIB_DIR=$PWD/lib
if [ ! -d "$INC_DIR" ] || [ ! -d "$LIB_DIR" ]; then
	cd FT-TS/
#--------------------------------------------------COMPILE ACCORDING TO USER'S SELECTION
	make $MAKE
	make clean
	cd ..
fi
cd FT-TS_test/
make $MAKE

#--------------------------------------------------PREPARE RESULTS DIRECTORIES AND FILES
RES_DIR=$PWD/results
FILE_RES="*.res"

#--------------------------------------------------EXECUTE
MPI_EXEC=mpiexec
HOSTFILE="--machinefile ${HOST}"

if [ "$FACT" = "U" ]; then
	EXECUTABLE=ft_tslu_test
	ALG="FTTSLU"
	ARGS="${M} ${N} ${L} ${INPUT_FILE}"
elif [ "$FACT" = "R" ]; then
	EXECUTABLE=ft_tsqr_test
	ALG="FTTSQR"
	ARGS="${M} ${N} ${L} ${INPUT_FILE}"
else
	EXECUTABLE=ft_tsch_test
	ALG="FTTSCH"
	ARGS="${M} ${N} ${INPUT_FILE}"
fi

for (( j = MIN_PROCESS; j <= MAX_PROCESS; )); do
	N_PROCESS="--np ${j}"
	for (( i = 0; i < TEST_TIMES; i++ )); do
		echo "********************************************************************"
		echo "$MPI_EXEC $N_PROCESS $HOSTFILE $EXTRA_FLAGS ./$EXECUTABLE ${ARGS}"
		time $MPI_EXEC $N_PROCESS $HOSTFILE $EXTRA_FLAGS ./$EXECUTABLE ${ARGS}
		echo "********************************************************************"
		mkdir -p $RES_DIR/"[${ALG}][${COMPILE}][${LOWER}][${M}x${N}][${j}]_Processes_execution[${i}]"
		mv $FILE_RES $RES_DIR/"[${ALG}][${COMPILE}][${LOWER}][${M}x${N}][${j}]_Processes_execution[${i}]"/
		rm -rf *.dump
	done
	j=$(($j*2))
done

make clean
rm -rf $INC_DIR
rm -rf $LIB_DIR

