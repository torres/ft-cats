#ifndef _FT_CAQR_H_
#define _FT_CAQR_H_

#include "ca_util.h"

int ft_caqr(fttsqr_data *ftd,MPI_data *mpid,int M,int N,int q_c,char *in,int argc,char **argv);
int update_trailing_matrix(MPI_data *mpid);
double *get_T_matrix(double *Hv,double *TAU,int HvM,int HvN);

#endif /* _FT_CAQR_H_ */

