#include "ca_util.h"

double *get_panel(matrix_data *md,int panelM,int panelN,MPI_Comm *panel_comm){
	int i,ret;
	int panel_comm_size,panel_comm_rank,panelIndex;
	double *panel=(double *)malloc(panelM*panelN*sizeof(double));
	double *subP=(double *)malloc(md->M*md->N*sizeof(double));
//	DISTRIBUYE SUBBLOQUES A PROCESAR CON FT-TSQR
	MPI_Comm_size(*panel_comm,&panel_comm_size);
	MPI_Comm_rank(*panel_comm,&panel_comm_rank);
	for(i=0;i<panel_comm_size;i++){
		time_comm=MPI_Wtime();
		ret=MPI_Bcast((i==panel_comm_rank) ? md->A : subP,md->M*md->N,MPI_DOUBLE,i,*panel_comm);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret!=MPI_SUCCESS)
			return(NULL);
		MPI_Barrier(*panel_comm);
		panelIndex=get_block_index(md->M,md->N,panelM,panelN,i);
		set_sub_block(panel,panelM,panelIndex,(i==panel_comm_rank) ? md->A : subP,md->M,md->N);
	}
	free(subP);
	return(panel);
}

int create_grid_process(MPI_data *mpid,MPI_Comm *cart_2D,MPI_Comm *row_comm,MPI_Comm *col_comm,int *rowM,int *colN,int *rowNum,int *colNum){
	MPI_Comm new_cart_2D;
	MPI_Comm new_col_comm;
	MPI_Comm new_row_comm;
	char err[MPI_MAX_ERROR_STRING];
	int ret,err_len,flag;
	int dims[2],periods[2],remain_dims_row[2],remain_dims_col[2];
	dims[0]=dims[1]=(int)sqrt(mpid->world_size);
	periods[0]=0;
	periods[1]=1;
	remain_dims_row[0]=1;
	remain_dims_row[1]=0;
	remain_dims_col[0]=0;
	remain_dims_col[1]=1;
	ret=MPI_Cart_create(mpid->world,2,dims,periods,0,&new_cart_2D);
	flag=(ret==MPI_SUCCESS);
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
	MPIX_Comm_agree(mpid->world,&flag);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear topologia cartesiana[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		if(ret==MPI_SUCCESS){
			MPIX_Comm_revoke(new_cart_2D);
			MPI_Comm_free(&new_cart_2D);
		}
		return(ret);
	}
#else
	MPI_Barrier(mpid->world);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear topologia cartesiana[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		if(ret==MPI_SUCCESS){
			MPI_Comm_free(&new_cart_2D);
		}
		return(ret);
	}
#endif /* _OMPI_ */
	MPI_Comm_set_errhandler(new_cart_2D,MPI_ERRORS_RETURN);
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
	ret=MPI_Cart_sub(new_cart_2D,remain_dims_row,&new_row_comm);
	flag=(ret==MPI_SUCCESS);
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
	MPIX_Comm_agree(new_cart_2D,&flag);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear comunicador de filas[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		if(ret==MPI_SUCCESS){
			MPIX_Comm_revoke(new_row_comm);
			MPIX_Comm_revoke(new_cart_2D);
			MPI_Comm_free(&new_row_comm);
			MPI_Comm_free(&new_cart_2D);
		}
		return(ret);
	}
#else
	MPI_Barrier(new_cart_2D);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear comunicador de filas[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		if(ret==MPI_SUCCESS){
			MPI_Comm_free(&new_row_comm);
			MPI_Comm_free(&new_cart_2D);
		}
		return(ret);
	}
#endif /* _OMPI_ */
	MPI_Comm_set_errhandler(new_row_comm,MPI_ERRORS_RETURN);
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
	ret=MPI_Cart_sub(new_cart_2D,remain_dims_col,&new_col_comm);
	flag=(ret==MPI_SUCCESS);
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
	MPIX_Comm_agree(new_cart_2D,&flag);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear comunicador de columnas[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		if(ret==MPI_SUCCESS){
			MPIX_Comm_revoke(new_col_comm);
			MPIX_Comm_revoke(new_row_comm);
			MPIX_Comm_revoke(new_cart_2D);
			MPI_Comm_free(&new_col_comm);
			MPI_Comm_free(&new_row_comm);
			MPI_Comm_free(&new_cart_2D);
		}
		return(ret);
	}
#else
	MPI_Barrier(new_cart_2D);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear comunicador de columnas[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		if(ret==MPI_SUCCESS){
			MPI_Comm_free(&new_col_comm);
			MPI_Comm_free(&new_row_comm);
			MPI_Comm_free(&new_cart_2D);
		}
		return(ret);
	}
#endif /* _OMPI_ */
	MPI_Comm_set_errhandler(new_col_comm,MPI_ERRORS_RETURN);
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
	*cart_2D=new_cart_2D;
	*col_comm=new_col_comm;
	*row_comm=new_row_comm;
//	MPI_Comm_free(cart_2D);
//	MPI_Comm_free(col_comm);
//	MPI_Comm_free(row_comm);
	MPI_Comm_size(*row_comm,rowM);
	MPI_Comm_rank(*row_comm,rowNum);
	MPI_Comm_size(*col_comm,colN);
	MPI_Comm_rank(*col_comm,colNum);
	return(MPI_SUCCESS);
}

int read_subblock(matrix_data *md,MPI_data *mpid,int row,int col,char *option,char fact){
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char lower[3];
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	if(fact=='U')
		sprintf(lower,"%s",(l_calc) ? "LU" : "U");
	if(fact=='R')
		sprintf(lower,"%s",(q_calc) ? "QR" : "R");
	else
		sprintf(lower,"%s","LL");
	sprintf(filename,"[%s][%s][%s][%dx%d][%dx%d][%d]_Process[%d]_Backup.dump",option,compilation,lower,md->M,md->N,row,col,mpid->world_size,mpid->world_rank);
	FILE *in=fopen(filename,"r");
	if(in==NULL){
		fprintf(stderr,"Error al leer el archivo [%s]\n",filename);
		return(EXIT_FAILURE);
	}
	else{
		time_read=MPI_Wtime();
		fread(md->A,sizeof(double),md->M*md->N,in);
		time_read_final+=MPI_Wtime()-time_read;
		fclose(in);
	}
	return(MPI_SUCCESS);
}

int write_subblock(double *A,int M,int N,MPI_data *mpid,int row,int col,char *option,char fact){
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char lower[3];
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	if(fact=='U')
		sprintf(lower,"%s",(l_calc) ? "LU" : "U");
	if(fact=='R')
		sprintf(lower,"%s",(q_calc) ? "QR" : "R");
	else
		sprintf(lower,"%s","LL");
	sprintf(filename,"[%s][%s][%s][%dx%d][%dx%d][%d]_Process[%d]_Backup.dump",option,compilation,lower,M,N,row,col,mpid->world_size,mpid->world_rank);
	FILE *out=fopen(filename,"w");
	if(out==NULL){
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
		return(EXIT_FAILURE);
	}
	else{
		time_write=MPI_Wtime();
		fwrite(A,sizeof(double),M*N,out);
		time_write_final+=MPI_Wtime()-time_write;
		fclose(out);
	}
	return(MPI_SUCCESS);
}

int next_stage(int curr_s,int total){
	return((curr_s+1)%total);
}

#ifndef _OMPI_
void verify_error(MPI_data *mpid,MPI_Comm *cart_2D,MPI_Comm *col_comm,MPI_Comm *row_comm,int *rowM,int *colN,int *rowNum,int *colNum,int curr,char fact){
	char msg[MPI_MAX_ERROR_STRING];
	char err[MPI_MAX_ERROR_STRING];
	char alg[3];
	if(fact=='U'){
		alg[0]='L';
		alg[1]='U';
	}
	else if(fact=='R'){
		alg[0]='Q';
		alg[1]='R';
	}
	else{
		alg[0]='C';
		alg[1]='H';
	}
	int ret,err_len,i;
	int flag;
	int indexS;
	int dead_list_size;
	int surv_list_size;
	int *dead_rank_list;
	int *surv_rank_list;
	time_rest=MPI_Wtime();
	while(1){
//		CADA PROCESO OBTIENE UNA LISTA DE PROCESOS FALLIDOS
		dead_rank_list=dead_list(mpid,&dead_list_size);
//		CADA PROCESO OBTIENE UNA LISTA DE PROCESOS SOBREVIVIENTES
		surv_rank_list=survivor_list(mpid,dead_rank_list,dead_list_size,&surv_list_size);
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		sprintf(msg,"Proceso [%d] en [%s]: %s - grid:[%d,%d]: Procesos fallidos: ",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum);
		for(i=0;i<dead_list_size;i++)
			sprintf(msg,"%s[%d]",msg,dead_rank_list[i]);
		printf("%s\n",msg);
		sprintf(msg,"Proceso [%d] en [%s]: %s - grid:[%d,%d]: Procesos sobrevivientes: ",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum);
		for(i=0;i<surv_list_size;i++)
			sprintf(msg,"%s[%d]",msg,surv_rank_list[i]);
		usleep(WAIT_TIME*mpid->world_rank);
		printf("%s\n",msg);
#endif /* _VERB_ */
//		RESTAURA A LOS PROCESOS FALLIDOS
		ret=replace_partners(mpid,dead_rank_list,dead_list_size);
//		SI LA RESTAURACION FALLO, INTENTA DE NUEVO
		if(ret!=MPI_SUCCESS){
			free(dead_rank_list);
			free(surv_rank_list);
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: %s - grid:[%d,%d] - Error al restaurar procesos [%d][%s]\n",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum,ret,err);
			continue;
		}
//		SE BUSCA LA POSICION EN LA LISTA DE SOBREVIVIENTES
		indexS=0;
		while(mpid->world_rank!=surv_rank_list[indexS]){
			indexS++;
		}
//		SI ME CORRESPONDE ENVIAR DATOS A UN PROCESO FALLIDO
		if(indexS<dead_list_size){
			for(i=indexS;i<dead_list_size;i+=surv_list_size){
#ifdef _VERB_
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: %s - grid:[%d,%d] - Enviando fila/columna actual [%d] a [%d]\n",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum,curr,dead_rank_list[i]);
#endif /* _VERB_ */
				ret=safe_send_times((void *)(&curr),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//				SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
				if(ret!=MPI_SUCCESS){
					MPI_Error_string(ret,err,&err_len);
					fprintf(stderr,"Proceso [%d] en [%s]: %s - grid:[%d,%d] - Error al distribuir fila/columna actual [%d] a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum,curr,dead_rank_list[i],ret,err);
					continue;
				}
#ifdef _VERB_
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: %s - grid:[%d,%d] - Enviando etapa actual [%d] a [%d]\n",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum,stage,dead_rank_list[i]);
#endif /* _VERB_ */
				ret=safe_send_times((void *)(&stage),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//				SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
				if(ret!=MPI_SUCCESS){
					MPI_Error_string(ret,err,&err_len);
					fprintf(stderr,"Proceso [%d] en [%s]: %s - grid:[%d,%d] - Error al distribuir etapa actual [%d] a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum,stage,dead_rank_list[i],ret,err);
					continue;
				}
				printf("Proceso [%d] en [%s]: %s - grid:[%d,%d] - Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum,dead_rank_list[i]);
			}
		}
		ret=create_grid_process(mpid,cart_2D,col_comm,row_comm,rowM,colN,rowNum,colNum);
//		SI LA RESTAURACION FALLO, INTENTA DE NUEVO
		if(ret!=MPI_SUCCESS){
			free(dead_rank_list);
			free(surv_rank_list);
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: %s - grid:[%d,%d] - Reintentando... [%d][%s]\n",mpid->world_rank,mpid->processor_name,alg,*rowNum,*colNum,ret,err);
			continue;
		}
		break;
	}
	time_rest_final+=MPI_Wtime()-time_rest;
}
#endif /* _OMPI_ */

