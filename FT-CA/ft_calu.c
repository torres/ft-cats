#include "ft_calu.h"

int ft_calu(fttslu_data *ftd,MPI_data *mpid,int M,int N,int l_c,char *in,int argc,char **argv){
	char msg[MPI_MAX_ERROR_STRING*10];
	char err[MPI_MAX_ERROR_STRING];
	int ret=0,err_len;
	int flag;

//	int i;
//	int ok=1;
//	int check;
//	MPI_Win window;
//	MPI_Win_create(&check,sizeof(int),sizeof(int),MPI_INFO_NULL,mpid->world,&window);
//	MPI_Win_fence(0,window);

	time_calu=MPI_Wtime();
	while(currCol<colN-1){
		if(colNum==currCol)
			printf("Proceso [%d] en [%s]: grid:[%d,%d] - Procesando fila/columna[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
		switch(stage){
			case STAGE_COMM_DIVISION:
//------------------------------------------------------------PRUEBA DE ERROR------------------------------------------------------------
//				if(mpid->world_rank==1){
//					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Fallando...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					raise(SIGKILL);
//				}
//----------------------------------------------------------FIN PRUEBA DE ERROR----------------------------------------------------------
//				SI ME CORRESPONDE CALCULAR UN PANEL
				if(colNum==currCol){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Dividiendo comunicador de columnas[%d]...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					DIVIDE COMUNICADOR EXCLUYENDO PROCESOS QUE YA COMPLETARON SU CALCULO
					time_comm=MPI_Wtime();
					ret=MPI_Comm_split(col_comm,(rowNum>=currRow),rowNum,&panel_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando division de comunicador de columnas[%d]...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al dividir comunicador de columnas[%d]",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
			break;
			case STAGE_PANEL_GENERATION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Generando panel\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//					GENERA PANEL A PROCESAR CON FT-TSLU
					panelM=M-currRow*md_local->M;
					panel=get_panel(md_local,panelM,panelN,&panel_comm);
					flag=(panel!=NULL);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando generacion de panel...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
					flag=(ret==MPI_SUCCESS);
				}
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al generar panel",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_FTTSLU_INITIALIZATION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Panel generado. Iniciando FT-TSLU\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(panel,panelM,panelN);
#endif /* _VERB_ */
//					INCIALIZA AMBIENTE PARA EJECUTAR FT-TSLU
					mpid_col=MPI_data_init_with_comm(panel_comm,&argc,&argv);
					ftd->mpid=mpid_col;
					ret=ft_tslu_init(ftd,panel,panelM,panelN,l_c,in,argc,argv);
				}
//				else if(colNum==currCol && rowNum<currRow){
//					LIBERA EL COMUNICADOR TEMPORAL
//					MPI_Comm_free(&panel_comm);
//					ret=MPI_SUCCESS;
//				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando inicializacion FT-TSLU...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error de inicializacion FT-TSLU",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_FTTSLU_EXECUTION:
//				check=0;
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - FT-TSLU sobre panel\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(panel,panelM,panelN);
#endif /* _VERB_ */
//					EJECUCION DE FT-TSLU SOBRE EL PANEL
					ret=ft_tslu(ftd,l_c);
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - FT-TSLU sobre panel finalizado[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,ret);
#endif /* _VERB_ */
/*					for(i=0;i<mpid->world_size;i++){
						if(i==mpid->world_rank)
							continue;
						MPI_Put(&ok,1,MPI_INT,i,0,1,MPI_INT,window);
					}
					MPI_Win_fence(0,window);*/
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando fin FT-TSLU en row_comm[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					MPI_Win_fence(0,window);
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al ejecutar FT-TSLU",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_SEND_RECEIVE_L:
				time_exec_tm=MPI_Wtime();
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Distribuyendo subbloque L de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
//					print_matrix(md_local->A,md_local->M,md_local->N);
#endif /* _VERB_ */
//					RESTAURA FILAS INICIALES DE LA MATRIZ L (SE ALMACENA EN A_init PARA AHORRAR MEMORIA)
//					swap_sub_block(ftd->md->A_init,ftd->md->M,0,ftd->md->M,ftd->md->N,ftd->md->IPIV,-1);
					MPI_Comm_rank(panel_comm,&panelRowNum);
					panelIndex=get_block_index(md_local->M,md_local->N,ftd->md->M,ftd->md->N,panelRowNum);
					get_sub_block(ftd->md->A_init,ftd->md->M,panelIndex,md_local->A,md_local->M,md_local->N);
//					DISTRIBUYE SUBBLOQUE L RESULTANTE DE FT-TSLU SOBRE LINEAS
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(md_local->A,md_local->M*md_local->N,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
//					RESPALDA EL RESULTADO EN EL ARCHIVO LOCAL
					write_subblock(md_local->A,md_local->M,md_local->N,mpid,rowNum,colNum,"FTCALU",'U');
//					LIBERA EL COMUNICADOR TEMPORAL
//					MPI_Comm_free(&panel_comm);
				}
				else if(colNum>currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibiendo subbloque L de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					RECIBE SUBBLOQUE L RESULTANTE DE FT-TSLU SOBRE MI LINEA
					sub=(double *)malloc(md_local->M*md_local->N*sizeof(double));
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(sub,md_local->M*md_local->N,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibiendo subbloque L de proceso[%d] en row_comm para descartar\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					RECIBE SUBBLOQUE L RESULTANTE DE FT-TSLU SOBRE MI LINEA
					sub=(double *)malloc(md_local->M*md_local->N*sizeof(double));
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(sub,md_local->M*md_local->N,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
					free(sub);
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al %s subbloque L de proceso[%d] en row_comm",mpid->world_rank,mpid->processor_name,rowNum,colNum,(colNum==currCol && rowNum>=currRow) ? "distribuir" : (colNum>currCol && rowNum>=currRow) ? "recibir" : "distribuir/recibir",currCol);
			break;
			case STAGE_SEND_RECEIVE_U:
				time_exec_tm=MPI_Wtime();
				if(colNum>currCol && rowNum==currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Distribuyendo subbloque U de proceso[%d] en col_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currRow);
//					print_matrix(md_local->A,md_local->M,md_local->N);
#endif /* _VERB_ */
//					SI ME CORRESPONDE ACTUALIZAR EL PANEL SUPERIOR DE U's CALCULA EL SUBBLOQUE U FINAL DE LA LINEA
					solve_matrix(sub,md_local->A,md_local->M,md_local->N);
					free(md_local->A);
					md_local->A=sub;
//					DISTRIBUYE SUBBLOQUE U RESULTANTE SOBRE COLUMNAS
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(md_local->A,md_local->M*md_local->N,MPI_DOUBLE,currRow,col_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
//					RESPALDA EL RESULTADO EN EL ARCHIVO LOCAL
					write_subblock(md_local->A,md_local->M,md_local->N,mpid,rowNum,colNum,"FTCALU",'U');
				}
//				SI ME CORRESPONDE ACTUALIZAR EL RESTO DE LA TRAILING MATRIX
				else if(colNum>currCol && rowNum>currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibiendo subbloque U de proceso[%d] en col_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currRow);
#endif /* _VERB_ */
//					RECIBE SUBBLOQUE U RESULTANTE SOBRE MI COLUMNA
					subR=(double *)malloc(md_local->M*md_local->N*sizeof(double));
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(subR,md_local->M*md_local->N,MPI_DOUBLE,currRow,col_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibiendo subbloque U de proceso[%d] en col_comm para descartar\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currRow);
#endif /* _VERB_ */
//					RECIBE SUBBLOQUE U RESULTANTE SOBRE MI COLUMNA
					subR=(double *)malloc(md_local->M*md_local->N*sizeof(double));
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(subR,md_local->M*md_local->N,MPI_DOUBLE,currRow,col_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
					free(subR);
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al %s subbloque U de proceso[%d] en col_comm",mpid->world_rank,mpid->processor_name,rowNum,colNum,(colNum>currCol && rowNum==currRow) ? "distribuir" : (colNum>currCol && rowNum>currRow) ? "recibir" : "distribuir/recibir",currRow);
			break;
			case STAGE_UPDATE_TM:
				time_exec_tm=MPI_Wtime();
//				SI ME CORRESPONDE ACTUALIZAR EL RESTO DE LA TRAILING MATRIX
				if(colNum>currCol && rowNum>currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Actualizando trailing matrix\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//					CALCULA EL SUBBLOQUE A ACTUALIZADO DE LA LINEA
					temp=(double *)calloc(md_local->M*md_local->N,sizeof(double));
//					MULTIPLICA EL SUBBLOQUE DE LA TRAILING MATRIX
					mult_matrix(sub,md_local->M,md_local->N,subR,md_local->M,md_local->N,temp,0);
//					RESTA AL SUBBLOQUE A EL RESULTADO DE LA ULTIMA MULTIPLICACION
					add_matrix(md_local->A,temp,md_local->M,md_local->N,'S');
					free(md_local->A);
					md_local->A=temp;
					free(subR);
					free(sub);
//					RESPALDA EL RESULTADO EN EL ARCHIVO LOCAL
					write_subblock(md_local->A,md_local->M,md_local->N,mpid,rowNum,colNum,"FTCALU",'U');
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando actualizacion trailing matrix...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				ret=MPI_SUCCESS;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al actualizar la trailing matrix",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_LAST_BLOCK_LU:
				time_exec_tm=MPI_Wtime();
				if(rowNum==rowM && colNum==colN && rowNum==currRow+1 && colNum==currCol+1){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Procesando ultimo bloque\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					lu_matrix(md_local->A,md_local->M,md_local->N,md_local->IPIV);
//					RESPALDA EL RESULTADO EN EL ARCHIVO LOCAL
					write_subblock(md_local->A,md_local->M,md_local->N,mpid,rowNum,colNum,"FTCALU",'U');
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando ultimo bloque...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				ret=MPI_SUCCESS;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al procesar ultimo subbloque",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
		}
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
		MPIX_Comm_agree(mpid->world,&flag);
//		SI HAY PROCESO FALLIDOS LOS RESTAURA
		if(!flag){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"%s[%d][%s]\n",msg,ret,err);
			verify_error(mpid,&cart_2D,&col_comm,&row_comm,&rowM,&colN,&rowNum,&colNum,currRow,'U');
			continue;
		}
#else
		MPI_Barrier(mpid->world);
		if(!flag){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"%s[%d][%s]\n",msg,ret,err);
			free_timers(mpid->world_size);
//			free_fttslu_data(ftd);
			MPI_Comm_free(&col_comm);
			MPI_Comm_free(&row_comm);
			MPI_Comm_free(&cart_2D);
			MPI_Finalize();
			return(ret);
		}
#endif /* _OMPI_ */
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
		if(stage==STAGE_LAST_BLOCK_LU){
			currRow++;
			currCol++;
		}
		stage=next_stage(stage,TOTAL_STAGES_LU);
	}
	time_calu_final+=MPI_Wtime()-time_calu;
//	MPI_Win_free(&window);
	return(MPI_SUCCESS);
}

