#ifndef _FT_CALU_H_
#define _FT_CALU_H_

#include "ca_util.h"

int ft_calu(fttslu_data *ftd,MPI_data *mpid,int M,int N,int l_c,char *in,int argc,char **argv);

#endif /* _FT_CALU_H_ */

