#ifndef _CA_UTIL_H_
#define _CA_UTIL_H_

#include "ft_tslu.h"
#include "ft_tsqr.h"
#include "ft_tsch.h"

#define STAGE_COMM_DIVISION				0
#define STAGE_PANEL_GENERATION			1

#define STAGE_FTTSLU_INITIALIZATION		2
#define STAGE_FTTSLU_EXECUTION			3
#define STAGE_SEND_RECEIVE_L			4
#define STAGE_SEND_RECEIVE_U			5
#define STAGE_UPDATE_TM					6
#define STAGE_LAST_BLOCK_LU				7

#define STAGE_FTTSQR_INITIALIZATION		2
#define STAGE_FTTSQR_EXECUTION			3
#define STAGE_SEND_RECEIVE_HV_TAU_SIZE	4
#define STAGE_SEND_RECEIVE_HV_TAU		5
#define STAGE_SPLIT_COL_COMM			6
#define STAGE_UPDATE_TM_HOUSEHOLDER		7
#define STAGE_LAST_BLOCK_QR				8

#define STAGE_FTTSCH_INITIALIZATION		2
#define STAGE_FTTSCH_EXECUTION			3
#define STAGE_SPLIT_COL_COMM_CH			5
#define STAGE_LAST_BLOCK_CH				7

#define TOTAL_STAGES_LU					8
#define TOTAL_STAGES_QR					9
#define TOTAL_STAGES_CH					8

double *get_panel(matrix_data *md,int panelM,int panelN,MPI_Comm *panel_comm);
int create_grid_process(MPI_data *mpid,MPI_Comm *cart_2D,MPI_Comm *row_comm,MPI_Comm *col_comm,int *rowM,int *colN,int *rowNum,int *colNum);
int read_subblock(matrix_data *md,MPI_data *mpid,int row,int col,char *option,char fact);
int write_subblock(double *A,int M,int N,MPI_data *mpid,int row,int col,char *option,char fact);
int next_stage(int curr_s,int total);

#ifndef _OMPI_
void verify_error(MPI_data *mpid,MPI_Comm *cart_2D,MPI_Comm *col_comm,MPI_Comm *row_comm,int *rowM,int *colN,int *rowNum,int *colNum,int curr,char fact);
#endif /* _OMPI_ */

MPI_data *mpid_col;
matrix_data *md_local;

MPI_Comm cart_2D;
MPI_Comm row_comm;
MPI_Comm col_comm;
MPI_Comm panel_comm;

int stage;

int rowM;
int colN;
int rowNum;
int colNum;
int currCol;
int currRow;

int panelRowNum;
int panelIndex;
int panelM;
int panelN;

double *panel;
double *sub;
double *subR;
double *temp;

#endif /* _CA_UTIL_H_ */

