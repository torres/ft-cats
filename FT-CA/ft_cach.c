#include "ft_cach.h"

int ft_cach(fttsch_data *ftd,MPI_data *mpid,int M,int N,char *in,int argc,char **argv){
	char msg[MPI_MAX_ERROR_STRING*10];
	char err[MPI_MAX_ERROR_STRING];
	int ret=0,err_len;
	int flag;
	time_cach=MPI_Wtime();
	while(currCol<colN-1){
#ifdef _VERB_
		if(colNum==currCol)
			printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Procesando fila/columna[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
		switch(stage){
			case STAGE_COMM_DIVISION:
//				if(mpid->world_rank==1 && currCol==0){
//					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Muriendo...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					raise(SIGKILL);
//				}
//				SI ME CORRESPONDE CALCULAR UN PANEL
				if(colNum==currCol){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Dividiendo comunicador de columnas[%d]...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					DIVIDE COMUNICADOR EXCLUYENDO PROCESOS QUE YA COMPLETARON SU CALCULO
					time_comm=MPI_Wtime();
					ret=MPI_Comm_split(col_comm,(rowNum>=currRow),rowNum,&panel_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Esperando division de comunicador de columnas[%d]...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Error al dividir comunicador de columnas[%d]",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
			break;
			case STAGE_PANEL_GENERATION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Generando panel\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(md_local->A,md_local->M,md_local->N);
#endif /* _VERB_ */
//					GENERA PANEL A PROCESAR CON FT-TSCH
//					panelM=M-currRow*md_local->M;
					panelM=md_local->M*(rowM-currRow);
					panel=get_panel(md_local,panelM,panelN,&panel_comm);
					flag=(panel!=NULL);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Esperando generacion de panel...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					panelM=md_local->M*(rowM-currRow);
					ret=MPI_SUCCESS;
					flag=(ret==MPI_SUCCESS);
				}
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Error al generar panel",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_FTTSCH_INITIALIZATION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Panel generado. Iniciando FT-TSCH\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//					INCIALIZA AMBIENTE PARA EJECUTAR FT-TSCH
					mpid_col=MPI_data_init_with_comm(panel_comm,&argc,&argv);
					ftd->mpid=mpid_col;
					ret=ft_tsch_init(ftd,panel,panelM,panelN,in,argc,argv);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Esperando inicializacion FT-TSCH...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Error de inicializacion FT-TSCH",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_FTTSCH_EXECUTION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - FT-TSCH sobre panel\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(panel,panelM,panelN);
#endif /* _VERB_ */
//					EJECUCION DE FT-TSCH SOBRE EL PANEL
					ret=ft_tsch(ftd);
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - FT-TSCH sobre panel finalizado[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,ret);

//					if(mpid->world_rank==2){
//						usleep(WAIT_TIME*mpid->world_rank);
//						printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - ftd->md->A\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//						print_matrix(ftd->md->A,ftd->md->M,ftd->md->N);
//					}
#endif /* _VERB_ */
//					MPI_Comm_rank(panel_comm,&panelRowNum);
//					panelIndex=get_block_index(md_local->Mb,md_local->Nb,panelM,panelN,panelRowNum);

//					if(mpid->world_rank==13){
//						usleep(WAIT_TIME*mpid->world_rank);
//						printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - panelIndex:[%d] - md_local->Mb:[%d] - md_local->Nb:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,panelIndex,md_local->Mb,md_local->Nb);
//					}

//					sub=(double *)calloc(md_local->Mb*md_local->Nb,sizeof(double));
//					get_sub_block(ftd->md->A,ftd->md->M,panelIndex,sub,md_local->Mb,md_local->Nb);

//					if(mpid->world_rank==13){
//						usleep(WAIT_TIME*mpid->world_rank);
//						printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - sub\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//						print_matrix(sub,md_local->Mb,md_local->Nb);
//					}
//					free(sub);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Esperando fin FT-TSCH en row_comm[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					panelRowNum=rowNum-currRow;
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Error al ejecutar FT-TSCH",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_SEND_RECEIVE_L:
				time_exec_tm=MPI_Wtime();
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Distribuyendo L de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					DISTRIBUYE L SOBRE LINEAS
					time_comm=MPI_Wtime();
//					ret=MPI_Bcast(sub,md_local->Mb*md_local->Nb,MPI_DOUBLE,currCol,row_comm);
					ret=MPI_Bcast(ftd->md->A,ftd->md->M*ftd->md->N,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
//					free(sub);
				}
				else if(colNum>currCol && rowNum>currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Recibiendo L de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					RECIBE L SOBRE LINEAS
//					sub=(double *)calloc(md_local->Mb*md_local->Nb,sizeof(double));
					sub=(double *)calloc(panelM*panelN,sizeof(double));
					time_comm=MPI_Wtime();
//					ret=MPI_Bcast(sub,md_local->Mb*md_local->Nb,MPI_DOUBLE,currCol,row_comm);
					ret=MPI_Bcast(sub,panelM*panelN,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;

//					usleep(WAIT_TIME*mpid->world_rank);
//					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - sub\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(sub,panelM,panelN);

				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Recibiendo L de proceso[%d] en row_comm para descartar\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					RECIBE L SOBRE LINEAS
//					sub=(double *)calloc(md_local->Mb*md_local->Nb,sizeof(double));
					sub=(double *)calloc(panelM*panelN,sizeof(double));
					time_comm=MPI_Wtime();
//					ret=MPI_Bcast(sub,md_local->Mb*md_local->Nb,MPI_DOUBLE,currCol,row_comm);
					ret=MPI_Bcast(sub,panelM*panelN,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;

//					usleep(WAIT_TIME*mpid->world_rank);
//					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - sub\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(sub,panelM,panelN);

					free(sub);
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Error al %s L de proceso[%d] en row_comm",mpid->world_rank,mpid->processor_name,rowNum,colNum,(colNum==currCol && rowNum>=currRow) ? "distribuir" : (colNum>currCol && rowNum>=currRow) ? "recibir" : "distribuir/recibir",currCol);
			break;
			case STAGE_SPLIT_COL_COMM_CH:
				time_exec_tm=MPI_Wtime();
				if(colNum>currCol){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Dividiendo col_comm[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					time_comm=MPI_Wtime();
					ret=MPI_Comm_split(col_comm,colNum>currCol && rowNum>currRow && rowNum-colNum>=0,mpid->world_rank,&panel_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Esperando division de col_comm[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Error al dividir col_comm[%d]",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
			break;
			case STAGE_UPDATE_TM:
				time_exec_tm=MPI_Wtime();
//				SI ME CORRESPONDE ACTUALIZAR LA TRAILING MATRIX
				if(colNum>currCol && rowNum>currRow && rowNum-colNum>=0){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Actualizando trailing matrix\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					mpid_col=MPI_data_init_with_comm(panel_comm,&argc,&argv);
					ret=update_trailing_matrix(mpid_col,sub);
					free(sub);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Esperando actualizacion trailing matrix...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Error al actualizar la trailing matrix",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_LAST_BLOCK_CH:
				time_exec_tm=MPI_Wtime();
				if(rowNum+1==rowM && colNum+1==colN && rowNum==currRow+1 && colNum==currCol+1){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Procesando ultimo bloque\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ch_matrix(md_local->A,md_local->N);
//					RESPALDA EL RESULTADO DE Q EN EL ARCHIVO LOCAL
					write_subblock(md_local->A,md_local->M,md_local->N,mpid,rowNum,colNum,"FTCACH_L",'L');
/*
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - L\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
					print_matrix(md_local->A,md_local->Mb,md_local->Nb);
*/
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Esperando ultimo bloque...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				ret=MPI_SUCCESS;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - Error al procesar ultimo subbloque",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
		}
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
		MPIX_Comm_agree(mpid->world,&flag);
//		SI HAY PROCESO FALLIDOS LOS RESTAURA
		if(!flag){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"%s[%d][%s]\n",msg,ret,err);
			verify_error(mpid,&cart_2D,&col_comm,&row_comm,&rowM,&colN,&rowNum,&colNum,currRow,'L');
			continue;
		}
#else
		MPI_Barrier(mpid->world);
		if(!flag){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"%s[%d][%s]\n",msg,ret,err);
//			free_timers(mpid->world_size);
			MPI_Comm_free(&col_comm);
			MPI_Comm_free(&row_comm);
			MPI_Comm_free(&cart_2D);
			MPI_Finalize();
			return(ret);
		}
#endif /* _OMPI_ */
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
		if(stage==STAGE_LAST_BLOCK_CH){
			currRow++;
			currCol++;
		}
		stage=next_stage(stage,TOTAL_STAGES_CH);
	}
	time_cach_final+=MPI_Wtime()-time_cach;
	return(MPI_SUCCESS);
}

int update_trailing_matrix(MPI_data *mpid,double *panelL){
//	matrix_data *md_sub=NULL;
	double *L;
	double *Lt;
	double *L_Lt;

	int ret,j;
	int flag;
#ifndef _OMPI_
#ifdef _VERB_
	char msg[1024];
	char temp[1024];
#endif /* _VERB_ */
	char err[MPI_MAX_ERROR_STRING];
	int i,err_len;
	int indexS;
	int send_tries;
	int dead_list_size;
	int surv_list_size;
	int *dead_rank_list;
	int *surv_rank_list;
	if(mpid->spawned){
		time_rest=MPI_Wtime();
		mpid->spawned=!mpid->spawned;
		time_rest_final+=MPI_Wtime()-time_rest;
	}
#endif /* _OMPI_ */
//	INTENTA	INTERCAMBIAR
	while(1){
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Ejecutando paso [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step);
#endif /* _VERB_ */
		if(mpid->step_finished)
			ret=MPI_SUCCESS;
		else{
			L=(double *)calloc(md_local->Mb*md_local->Nb,sizeof(double));
			Lt=(double *)calloc(md_local->Mb*md_local->Nb,sizeof(double));
			L_Lt=(double *)calloc(md_local->Mb*md_local->Nb,sizeof(double));
//			i=mpid->world_rank+(rowM-mpid->world_size)-currRow;
//			ret=!MPI_SUCCESS;
//			while(ret!=MPI_SUCCESS){
//#ifdef _VERB_
//				usleep(WAIT_TIME*mpid->world_rank);
//				printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Recuperando resultado L:[%dx%d][%d/%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,md_local->Mb,md_local->Nb,rowM-currRow,i);
//#endif /* _VERB_ */
//				ret=read_L(L,md_local->Mb,md_local->Nb,rowM-currRow,i,0);
//			}
			panelIndex=get_block_index(md_local->Mb,md_local->Nb,panelM,panelN,panelRowNum);
			get_sub_block(panelL,panelM,panelIndex,L,md_local->Mb,md_local->Nb);
			if(mpid->world_rank==0){
//#ifdef _VERB_
//				usleep(WAIT_TIME*mpid->world_rank);
//				printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Proceso en diagonal Lt:[%dx%d][%d/%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,md_local->Mb,md_local->Nb,rowM-currRow,i);
//#endif /* _VERB_ */
				memcpy(Lt,L,md_local->Mb*md_local->Nb*sizeof(double));
			}	
			else{
				j=colNum-currRow;
//				ret=!MPI_SUCCESS;
//				while(ret!=MPI_SUCCESS){
//#ifdef _VERB_
//					usleep(WAIT_TIME*mpid->world_rank);
//					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Recuperando resultado Lt:[%dx%d][%d/%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,md_local->Mb,md_local->Nb,rowM-currRow,j);
//#endif /* _VERB_ */
//					ret=read_L(Lt,md_local->Mb,md_local->Nb,rowM-currRow,j,0);
//				}
				panelIndex=get_block_index(md_local->Mb,md_local->Nb,panelM,panelN,j);
				get_sub_block(panelL,panelM,panelIndex,Lt,md_local->Mb,md_local->Nb);
			}
/*
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - L\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			print_matrix(L,md_local->Mb,md_local->Nb);

			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Lt\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			print_matrix(Lt,md_local->Mb,md_local->Nb);
*/
//			APLICA LA ECUACION C'i = Ci - Lrow * Lcol'
			mult_matrix_trans(L,md_local->Mb,md_local->Nb,Lt,md_local->Mb,md_local->Nb,L_Lt,0,'N','T');
/*
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - L_Lt\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			print_matrix(L_Lt,md_local->Mb,md_local->Nb);
*/
			add_matrix(md_local->A,L_Lt,md_local->Mb,md_local->Nb,'S');
/*
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - L_Lt RES\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			print_matrix(L_Lt,md_local->Mb,md_local->Nb);
*/
			memcpy(md_local->A,L_Lt,md_local->Mb*md_local->Nb*sizeof(double));
//			RESPALDA EL RESULTADO DE TM EN EL ARCHIVO LOCAL
			write_subblock(L_Lt,md_local->Mb,md_local->Nb,mpid,rowNum,colNum,"FTCACH_TM",'L');
//			LIBERA RECURSOS
			free(L_Lt);
			free(Lt);
//			free(L);
			ret=MPI_SUCCESS;
		}
		mpid->step_finished=flag=(ret==MPI_SUCCESS);
#ifdef _VERB_
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Ejecutado paso [%d] - ret:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step,ret);
#endif /* _VERB_ */
#ifndef _OMPI_
			MPIX_Comm_agree(mpid->world,&flag);
#else
			if(!flag){
				fprintf(stderr,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step);
				return(ret);
			}
			MPI_Barrier(mpid->world);
#endif /* _OMPI_ */
//			SI NO OCURRIO ERROR, CONTINUA
			if(flag)
				break;
#ifndef _OMPI_
			time_rest=MPI_Wtime();
			fprintf(stderr,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step);
			while(1){
//				CADA PROCESO OBTIENE UNA LISTA DE PROCESOS FALLIDOS
				dead_rank_list=dead_list(mpid,&dead_list_size);
//				CADA PROCESO OBTIENE UNA LISTA DE PROCESOS SOBREVIVIENTES
				surv_rank_list=survivor_list(mpid,dead_rank_list,dead_list_size,&surv_list_size);
#ifdef _VERB_
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Procesos fallidos: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
				for(i=0;i<dead_list_size;i++){
					sprintf(temp,"[%d]",dead_rank_list[i]);
					strcat(msg,temp);
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
				sprintf(msg,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Procesos sobrevivientes: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
				for(i=0;i<surv_list_size;i++){
					sprintf(temp,"[%d]",surv_rank_list[i]);
					strcat(msg,temp);
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
#endif /* _VERB_ */
//				RESTAURA A LOS PROCESOS FALLIDOS
				ret=replace_partners(mpid,dead_rank_list,dead_list_size);
//				SI LA RESTAURACION FALLO, INTENTA DE NUEVO
				if(ret!=MPI_SUCCESS){
					free(dead_rank_list);
					free(surv_rank_list);
					MPI_Error_string(ret,err,&err_len);
					fprintf(stderr,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Error al restaurar procesos [%d][%s]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,ret,err);
					continue;
				}
				break;
			}
//			SE BUSCA LA POSICION EN LA LISTA DE SOBREVIVIENTES
			indexS=0;
			while(mpid->world_rank!=surv_rank_list[indexS]){
				indexS++;
			}
//			SI ME CORRESPONDE ENVIAR DATOS A UN PROCESO FALLIDO
			if(indexS<dead_list_size){
				for(i=indexS;i<dead_list_size;i+=surv_list_size){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Enviando paso inicial a [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,dead_rank_list[i]);
#endif /* _VERB_ */
//					INTENTA ENVIAR EL PASO DE INICIO
					ret=safe_send_times((void *)(&(mpid->step)),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//					SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
					if(ret!=MPI_SUCCESS){
						MPI_Error_string(ret,err,&err_len);
						fprintf(stderr,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Error al distribuir paso inicial a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,dead_rank_list[i],ret,err);
						continue;
					}
//					INTENTA ENVIAR STAGE ACTUAL
					ret=safe_send_times((void *)(&(stage)),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//					SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
					if(ret!=MPI_SUCCESS){
						MPI_Error_string(ret,err,&err_len);
						fprintf(stderr,"Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Error al distribuir stage actual a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,dead_rank_list[i],ret,err);
						continue;
					}
					printf("Proceso [%d] en [%s]: CACH - grid:[%d,%d] - TMU - Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,dead_rank_list[i]);
				}
			}
			free(surv_rank_list);
			free(dead_rank_list);
			time_rest_final+=MPI_Wtime()-time_rest;
#endif /* _OMPI_ */
		}
		mpid->step_finished=0;
	return(ret);
}

