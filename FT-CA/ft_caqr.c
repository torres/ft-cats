#include "ft_caqr.h"

int ft_caqr(fttsqr_data *ftd,MPI_data *mpid,int M,int N,int q_c,char *in,int argc,char **argv){
	char msg[MPI_MAX_ERROR_STRING*10];
	char err[MPI_MAX_ERROR_STRING];
	int ret=0,err_len;
	int flag;
	int i,avail;
//	int HvM,HvN;
	time_caqr=MPI_Wtime();
	while(currCol<colN-1){
#ifdef _VERB_
		if(colNum==currCol)
			printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Procesando fila/columna[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
		switch(stage){
			case STAGE_COMM_DIVISION:
/*				if(mpid->world_rank==1 && currCol==0){
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Muriendo...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
					raise(SIGKILL);
				}*/
//				SI ME CORRESPONDE CALCULAR UN PANEL
				if(colNum==currCol){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Dividiendo comunicador de columnas[%d]...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					DIVIDE COMUNICADOR EXCLUYENDO PROCESOS QUE YA COMPLETARON SU CALCULO
					time_comm=MPI_Wtime();
					ret=MPI_Comm_split(col_comm,(rowNum>=currRow),rowNum,&panel_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Esperando division de comunicador de columnas[%d]...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error al dividir comunicador de columnas[%d]",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
			break;
			case STAGE_PANEL_GENERATION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Generando panel\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(md_local->A,md_local->M,md_local->N);
#endif /* _VERB_ */
//					GENERA PANEL A PROCESAR CON FT-TSQR
//					panelM=M-currRow*md_local->M;
					panelM=md_local->M*(rowM-currRow);
					panel=get_panel(md_local,panelM,panelN,&panel_comm);
					flag=(panel!=NULL);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Esperando generacion de panel...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
					flag=(ret==MPI_SUCCESS);
				}
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error al generar panel",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_FTTSQR_INITIALIZATION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Panel generado. Iniciando FT-TSQR\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(panel,panelM,panelN);
#endif /* _VERB_ */
//					INCIALIZA AMBIENTE PARA EJECUTAR FT-TSQR
					mpid_col=MPI_data_init_with_comm(panel_comm,&argc,&argv);
					ftd->mpid=mpid_col;
					ret=ft_tsqr_init(ftd,panel,panelM,panelN,q_c,in,argc,argv);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Esperando inicializacion FT-TSQR...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error de inicializacion FT-TSQR",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_FTTSQR_EXECUTION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - FT-TSQR sobre panel\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(panel,panelM,panelN);
#endif /* _VERB_ */
//					EJECUCION DE FT-TSQR SOBRE EL PANEL
					ret=ft_tsqr(ftd,q_c);
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - FT-TSQR sobre panel finalizado[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,ret);

//					if(mpid->world_rank==13){
//						usleep(WAIT_TIME*mpid->world_rank);
//						printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - ftd->md->A_init\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//						print_matrix(ftd->md->A_init,ftd->md->M,ftd->md->N);
//					}
/*					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - ftd->md->R\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
					print_matrix(ftd->md->R,ftd->md->M,ftd->md->N);*/
#endif /* _VERB_ */
					MPI_Comm_rank(panel_comm,&panelRowNum);
					panelIndex=get_block_index(md_local->Mb,md_local->Nb,panelM,panelN,panelRowNum);

//					if(mpid->world_rank==13){
//						usleep(WAIT_TIME*mpid->world_rank);
//						printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - panelIndex:[%d] - md_local->Mb:[%d] - md_local->Nb:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,panelIndex,md_local->Mb,md_local->Nb);
//					}

					sub=(double *)calloc(md_local->Mb*md_local->Nb,sizeof(double));
					get_sub_block(ftd->md->A_init,ftd->md->M,panelIndex,sub,md_local->Mb,md_local->Nb);

//					if(mpid->world_rank==13){
//						usleep(WAIT_TIME*mpid->world_rank);
//						printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - sub\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//						print_matrix(sub,md_local->Mb,md_local->Nb);
//					}

//					RESPALDA EL RESULTADO DE Q EN EL ARCHIVO LOCAL
					write_subblock(sub,md_local->Mb,md_local->Nb,mpid,rowNum,colNum,"FTCAQR_Q",'R');
					free(sub);
//					SI SOY EL PROCESO EN LA DIAGONAL, RESPALDA EL RESULTADO DE R EN EL ARCHIVO LOCAL
					if(panelRowNum==0){
						sub=(double *)malloc(md_local->Mb*md_local->Nb*sizeof(double));
						get_sub_block(ftd->md->R,ftd->md->M,0,sub,md_local->Mb,md_local->Nb);
//						RESPALDA EL RESULTADO DE R EN EL ARCHIVO LOCAL
						write_subblock(sub,md_local->Mb,md_local->Nb,mpid,rowNum,colNum,"FTCAQR_R",'R');
						free(sub);
					}
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Esperando fin FT-TSQR en row_comm[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					panelRowNum=rowNum-currRow;
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error al ejecutar FT-TSQR",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_SEND_RECEIVE_HV_TAU_SIZE:
				time_exec_tm=MPI_Wtime();
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Distribuyendo numero de Sizes/Hv/TAU de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
//					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Hv_list_size[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,Hv_list_size);
#endif /* _VERB_ */
//					DISTRIBUYE SOBRE LINEAS NUMERO DE VECTORES HOUSEHOLDER GENERADOS EN FT-TSQR
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(&Hv_list_size,1,MPI_INT,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else if(colNum>currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Recibiendo numero de Sizes/Hv/TAU de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					RECIBE SOBRE LINEAS NUMERO DE VECTORES HOUSEHOLDER GENERADOS EN FT-TSQR
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(&Hv_list_size,1,MPI_INT,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;

//					usleep(WAIT_TIME*mpid->world_rank);
//					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Hv_list_size[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,Hv_list_size);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Recibiendo numero de Sizes/Hv/TAU de proceso[%d] en row_comm para descartar\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					RECIBE SOBRE LINEAS NUMERO DE VECTORES HOUSEHOLDER GENERADOS EN FT-TSQR
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(&Hv_list_size,1,MPI_INT,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
					ret=MPI_SUCCESS;

//					usleep(WAIT_TIME*mpid->world_rank);
//					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Hv_list_size[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,Hv_list_size);
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error al %s numero de Hv/TAU de proceso[%d] en row_comm",mpid->world_rank,mpid->processor_name,rowNum,colNum,(colNum==currCol && rowNum>=currRow) ? "distribuir" : (colNum>currCol && rowNum>=currRow) ? "recibir" : "distribuir/recibir",currCol);
			break;
			case STAGE_SEND_RECEIVE_HV_TAU:
				time_exec_tm=MPI_Wtime();
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Distribuyendo Sizes/Hv/TAU de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					DISTRIBUYE SOBRE LINEAS LAS DIMENSIONES, VECTORES HOUSEHOLDER Y VALORES TAU GENERADOS EN FT-TSQR
					for(i=0;i<Hv_list_size;i++){
						avail=(Hv_sizes[i]==NULL) ? 0 : 1;
						time_comm=MPI_Wtime();
						ret=MPI_Bcast(&avail,1,MPI_INT,currCol,row_comm);
						time_comm_final+=MPI_Wtime()-time_comm;
						if(!avail){
//							usleep(WAIT_TIME*mpid->world_rank);
//							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Hv_sizes[%d] no almacenado\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i);
							continue;
						}
						time_comm=MPI_Wtime();
						ret=MPI_Bcast(Hv_sizes[i],3,MPI_INT,currCol,row_comm);
//						ret=MPI_Bcast(Hv_list[i],Hv_sizes[i][0]*Hv_sizes[i][1],MPI_DOUBLE,currCol,row_comm);
//						ret=MPI_Bcast(TAU_list[i],min(Hv_sizes[i][0],Hv_sizes[i][1]),MPI_DOUBLE,currCol,row_comm);
						time_comm_final+=MPI_Wtime()-time_comm;
					}
				}
				else if(colNum>currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Recibiendo Sizes/Hv/TAU de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					reset_lists();
					Hv_sizes=(int **)calloc(Hv_list_size,sizeof(int *));
//					Hv_list=(double **)calloc(Hv_list_size,sizeof(double *));
//					TAU_list=(double **)calloc(Hv_list_size,sizeof(double *));
//					RECIBE SOBRE LINEAS LAS DIMENSIONES, VECTORES HOUSEHOLDER Y VALORES TAU GENERADOS EN FT-TSQR
					for(i=0;i<Hv_list_size;i++){
						time_comm=MPI_Wtime();
						ret=MPI_Bcast(&avail,1,MPI_INT,currCol,row_comm);
						time_comm_final+=MPI_Wtime()-time_comm;
						if(!avail){
//							usleep(WAIT_TIME*mpid->world_rank);
//							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Hv_sizes[%d] no almacenado\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i);
							continue;
						}
						Hv_sizes[i]=(int *)calloc(3,sizeof(int));
						time_comm=MPI_Wtime();
						ret=MPI_Bcast(Hv_sizes[i],3,MPI_INT,currCol,row_comm);
						time_comm_final+=MPI_Wtime()-time_comm;
//						Hv_list[i]=(double *)calloc(Hv_sizes[i][0]*Hv_sizes[i][1],sizeof(double));
//						TAU_list[i]=(double *)calloc(min(Hv_sizes[i][0],Hv_sizes[i][1]),sizeof(double));
//						time_comm=MPI_Wtime();
//						ret=MPI_Bcast(Hv_list[i],Hv_sizes[i][0]*Hv_sizes[i][1],MPI_DOUBLE,currCol,row_comm);
//						ret=MPI_Bcast(TAU_list[i],min(Hv_sizes[i][0],Hv_sizes[i][1]),MPI_DOUBLE,currCol,row_comm);
//						time_comm_final+=MPI_Wtime()-time_comm;
					}
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Recibiendo Hv/TAU de proceso[%d] en row_comm para descartar\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					reset_lists();
					Hv_sizes=(int **)calloc(Hv_list_size,sizeof(int *));
//					Hv_list=(double **)calloc(Hv_list_size,sizeof(double *));
//					TAU_list=(double **)calloc(Hv_list_size,sizeof(double *));
//					RECIBE SOBRE LINEAS LAS DIMENSIONES, VECTORES HOUSEHOLDER Y VALORES TAU GENERADOS EN FT-TSQR
					for(i=0;i<Hv_list_size;i++){
						time_comm=MPI_Wtime();
						ret=MPI_Bcast(&avail,1,MPI_INT,currCol,row_comm);
						time_comm_final+=MPI_Wtime()-time_comm;
						if(!avail){
//							usleep(WAIT_TIME*mpid->world_rank);
//							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Hv_sizes[%d] no almacenado\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i);
							continue;
						}
						Hv_sizes[i]=(int *)calloc(3,sizeof(int));
						time_comm=MPI_Wtime();
						ret=MPI_Bcast(Hv_sizes[i],3,MPI_INT,currCol,row_comm);
						time_comm_final+=MPI_Wtime()-time_comm;
//						Hv_list[i]=(double *)calloc(Hv_sizes[i][0]*Hv_sizes[i][1],sizeof(double));
//						TAU_list[i]=(double *)calloc(min(Hv_sizes[i][0],Hv_sizes[i][1]),sizeof(double));
//						time_comm=MPI_Wtime();
//						ret=MPI_Bcast(Hv_list[i],Hv_sizes[i][0]*Hv_sizes[i][1],MPI_DOUBLE,currCol,row_comm);
//						ret=MPI_Bcast(TAU_list[i],min(Hv_sizes[i][0],Hv_sizes[i][1]),MPI_DOUBLE,currCol,row_comm);
//						time_comm_final+=MPI_Wtime()-time_comm;
					}
					ret=MPI_SUCCESS;
				}
#ifdef _VERB_
				for(i=0;i<Hv_list_size;i++){
					avail=(Hv_sizes[i]==NULL) ? 0 : 1;
					if(avail){
						usleep(WAIT_TIME*mpid->world_rank);
						printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Hv_list[%d][%dx%d][%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i,Hv_sizes[i][0],Hv_sizes[i][1],Hv_sizes[i][2]);
//						print_matrix(Hv_list[i],Hv_sizes[i][0],Hv_sizes[i][1]);
					}
				}
#endif /* _VERB_ */
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error al %s Hv/TAU de proceso[%d] en row_comm",mpid->world_rank,mpid->processor_name,rowNum,colNum,(colNum==currCol && rowNum>=currRow) ? "distribuir" : (colNum>currCol && rowNum>=currRow) ? "recibir" : "distribuir/recibir",currCol);
			break;
			case STAGE_SPLIT_COL_COMM:
				time_exec_tm=MPI_Wtime();
				if(colNum>currCol){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Dividiendo col_comm[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					time_comm=MPI_Wtime();
					ret=MPI_Comm_split(col_comm,rowNum>=currRow,mpid->world_rank,&panel_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Esperando division de col_comm[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error al dividir col_comm[%d]",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
			break;
			case STAGE_UPDATE_TM_HOUSEHOLDER:
				time_exec_tm=MPI_Wtime();
//				SI ME CORRESPONDE ACTUALIZAR EL RESTO DE LA TRAILING MATRIX
				if(colNum>currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Actualizando trailing matrix\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//					INICIALIZA AMBIENTE PARA INTERCAMBIAR SUBBLOQUES C'i

//					if(colNum==currCol+1 && rowNum>=currRow){
					mpid_col=MPI_data_init_with_comm(panel_comm,&argc,&argv);
					ret=update_trailing_matrix(mpid_col);
//					}
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Esperando actualizacion trailing matrix...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error al actualizar la trailing matrix",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_LAST_BLOCK_QR:
				time_exec_tm=MPI_Wtime();
				if(rowNum==rowM && colNum==colN && rowNum==currRow+1 && colNum==currCol+1){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Procesando ultimo bloque\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					qr_matrix(md_local->A,md_local->M,md_local->N,md_local->TAU,md_local->WORK);
					solve_matrix(md_local->A_init,md_local->R,md_local->M,md_local->N);
//					RESPALDA EL RESULTADO DE Q EN EL ARCHIVO LOCAL
					write_subblock(md_local->A_init,md_local->M,md_local->N,mpid,rowNum,colNum,"FTCAQR_Q",'R');
//					RESPALDA EL RESULTADO DE R EN EL ARCHIVO LOCAL
					write_subblock(md_local->R,md_local->M,md_local->N,mpid,rowNum,colNum,"FTCAQR_R",'R');
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Esperando ultimo bloque...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				ret=MPI_SUCCESS;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Error al procesar ultimo subbloque",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
		}
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
		MPIX_Comm_agree(mpid->world,&flag);
//		SI HAY PROCESO FALLIDOS LOS RESTAURA
		if(!flag){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"%s[%d][%s]\n",msg,ret,err);
			verify_error(mpid,&cart_2D,&col_comm,&row_comm,&rowM,&colN,&rowNum,&colNum,currRow,'R');
			continue;
		}
#else
		MPI_Barrier(mpid->world);
		if(!flag){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"%s[%d][%s]\n",msg,ret,err);
//			free_timers(mpid->world_size);
			MPI_Comm_free(&col_comm);
			MPI_Comm_free(&row_comm);
			MPI_Comm_free(&cart_2D);
			MPI_Finalize();
			return(ret);
		}
#endif /* _OMPI_ */
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
		if(stage==STAGE_LAST_BLOCK_QR){
			currRow++;
			currCol++;
		}
		stage=next_stage(stage,TOTAL_STAGES_QR);
	}
	time_caqr_final+=MPI_Wtime()-time_caqr;
	return(MPI_SUCCESS);
}

int update_trailing_matrix(MPI_data *mpid){
	step_lim=((int)ceil(log2(mpid->world_size)))+1;
	matrix_data *md_sub=NULL;
#ifdef _VERB_
	char msg[1024];
	char temp[1024];
#endif /* _VERB_ */

	double *T;
	double *Hv_C;
	double *T_Hv_C;
	double *Ci;
	double *Cj;
	double *CjR;

	double *Hv1;
	double *Hv1t_C1;
	double *W;
	double *Hv1_W;

	int broadcast_list[mpid->world_size];
	int prev_alone_list[mpid->world_size];
	int broadcast_count;
	int alone_count;
	int dest;
	int first_alone;
	int forget_broadcast;

//	int finished[mpid->world_size];
	int index,indexD;
	int index_prev,indexD_prev;
	int ret,i;
	int flag;
//	int count;
	int HvM=0,HvN=0;
	int mini;
	int broadcast=0;
#ifndef _OMPI_
	char err[MPI_MAX_ERROR_STRING];
	int err_len,j;
	int curr_step;
	int indexS;
	int send_tries;
	int dead_list_size;
	int surv_list_size;
	int *dead_rank_list;
	int *surv_rank_list;
	if(mpid->spawned){
		time_rest=MPI_Wtime();
		curr_step=mpid->step;
		mpid->step=0;
		for(ret=0;ret<curr_step;ret++){
			update_dest(mpid);
		}
		mpid->step=curr_step;
		mpid->spawned=!mpid->spawned;
		time_rest_final+=MPI_Wtime()-time_rest;
	}
#endif /* _OMPI_ */
	while(mpid->step<step_lim-1){
		index=get_Hv_index(mpid->step,mpid->world_rank,mpid->dest[mpid->world_rank]);
		indexD=get_Hv_index(mpid->step,mpid->dest[mpid->world_rank],mpid->world_rank);
		if(Hv_sizes[index]!=NULL){
			HvM=Hv_sizes[index][0];
			HvN=Hv_sizes[index][1];
		}
//		INTENTA	INTERCAMBIAR
		while(1){
#ifdef _VERB_
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Ejecutando paso [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step);
#endif /* _VERB_ */
			if(mpid->step_finished)
				ret=MPI_SUCCESS;
			else if(mpid->step==0){
				md_sub=matrix_data_init(HvM,HvN,0,0,0,'R');
				ret=!MPI_SUCCESS;
				while(ret!=MPI_SUCCESS){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Recuperando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,HvM,HvN,mpid->world_size,mpid->world_rank,Hv_sizes[index][2]);
#endif /* _VERB_ */
					ret=read_R_Hv_TAU(md_sub->A,md_sub->TAU,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,Hv_sizes[index][2]);
				}
				set_Hv(md_sub);
//				CALCULA LA MATRIZ T
//				T=get_T_matrix(Hv_list[index],TAU_list[index],HvM,HvN);
				T=get_T_matrix(md_sub->Hv,md_sub->TAU,HvM,HvN);
				Hv_C=(double *)calloc(HvM*HvN,sizeof(double));
				T_Hv_C=(double *)calloc(HvM*HvN,sizeof(double));
				Ci=(double *)calloc(HvM*HvN,sizeof(double));
//				APLICA LA ECUACION C'i = Ci + Hvi * (Ti^T * (Hvi^T * Ci))
//				mult_matrix_trans(Hv_list[index],HvM,HvN,md_local->A,md_local->M,md_local->N,Hv_C,0,'T','N');
				mult_matrix_trans(md_sub->Hv,HvM,HvN,md_local->A,md_local->M,md_local->N,Hv_C,0,'T','N');
				mult_matrix_trans(T,HvN,HvN,Hv_C,HvM,HvN,T_Hv_C,0,'T','N');
//				mult_matrix(Hv_list[index],HvM,HvN,T_Hv_C,HvM,HvN,Ci,0);
				mult_matrix(md_sub->Hv,HvM,HvN,T_Hv_C,HvM,HvN,Ci,0);
				add_matrix(md_local->A,Ci,HvM,HvN,'A');
//				LIBERA RECURSOS
				if(T_Hv_C!=NULL)
					free(T_Hv_C);
				if(Hv_C!=NULL)
					free(Hv_C);
				if(T!=NULL)
					free(T);
				matrix_data_free(md_sub);
//				SI EL PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO LOCAL Y DESTINO ES IGUAL
				if(index==indexD){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Sin compañero. Preparado para broadcast en proximo paso\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//					INICIALIZA EL SUBBLOQUE A RECIBIR EN PROXIMOS PASOS
					Cj=(double *)calloc(HvM*HvN,sizeof(double));
//					SIMULAR EL PRIMER PASO
					ret=MPI_SUCCESS;
//					ACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
					broadcast=1;
				}
				else{
//					INICIALIZA EL SUBBLOQUE A RECIBIR
					Cj=(double *)calloc(HvM*HvN,sizeof(double));
//					INTERCAMBIA MATRICES Ci,Cj
					ret=send_receive_matrix(Ci,Cj,HvM,HvN,mpid);
//					usleep(WAIT_TIME*mpid->world_rank);
//					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Cj 0\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(Cj,HvM,HvN);
				}
//				RESPALDA EL RESULTADO DE TM EN EL ARCHIVO LOCAL
				write_subblock(Ci,HvM,HvN,mpid,rowNum,colNum,"FTCAQR_TM",'R');
//				usleep(WAIT_TIME*mpid->world_rank);
//				printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Ci init 0\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//				print_matrix(Ci,HvM,HvN);
			}
			else{
//				CALCULA LISTA DE PROCESOS SIN COMPAÑERO EN EL PASO ACTUAL Y EL ANTERIOR
				memset(broadcast_list,0,mpid->world_size*sizeof(int));
				memset(prev_alone_list,0,mpid->world_size*sizeof(int));
				broadcast_count=0;
				alone_count=0;
				for(i=0;i<mpid->world_size;i++){
					dest=get_dest(i,mpid->step,mpid->world_size);
					if(i==dest && i!=mpid->world_rank){
						broadcast_list[i]=1;
						broadcast_count++;
					}
					dest=get_dest(i,mpid->step-1,mpid->world_size);
					if(i==dest){
						prev_alone_list[i]=1;
						alone_count++;
					}
				}
//				CALCULA EL PRIMER PROCESO SIN COMPAÑERO
				first_alone=0;
				while(!prev_alone_list[first_alone]){
					first_alone++;
				}
#ifdef _VERB_
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Estado de broadcast:[%d] - first_alone:[%d] - broadcast_count:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,broadcast,first_alone,broadcast_count);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - broadcast_list: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
				for(i=0;i<mpid->world_size;i++){
					if(broadcast_list[i]){
						sprintf(temp,"[%d]",i);
						strcat(msg,temp);
					}
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - prev_alone_list: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
				for(i=0;i<mpid->world_size;i++){
					if(prev_alone_list[i]){
						sprintf(temp,"[%d]",i);
						strcat(msg,temp);
					}
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - mpid->mirror: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
				for(i=0;i<mpid->world_size;i++){
					if(mpid->mirror[i]){
						sprintf(temp,"[%d]",i);
						strcat(msg,temp);
					}
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
#endif /* _VERB_ */
//				SI EL PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO LOCAL Y DESTINO ES IGUAL
				if(index==indexD){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Sin compañero. Preparado para broadcast en proximo paso\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//					SI ME CORRESPONDE RECIBIR BROADCAST
					if(broadcast==0){
//						VERIFICA QUE NO TENGA LA MISMA INFORMACION QUE EL PRIMER PROCESO SIN COMPAÑERO
						forget_broadcast=0;
						for(i=0;i<mpid->world_size;i++){
//							SI YA TENGO LA MISMA INFORMACION, NO RECIBO EL BROADCAST
							if(mpid->mirror[i] && i==first_alone){
#ifdef _VERB_
								usleep(WAIT_TIME*mpid->world_rank);
								printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Omitiendo broadcast de [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i);
#endif /* _VERB_ */
								forget_broadcast=1;
								break;
							}
						}
//						SI EN UN PASO INTERMEDIO ME QUEDE SIN COMPAÑERO Y NO RECIBO BROADCAST
						if(mpid->step+1<step_lim-1 && alone_count==0){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Preparado para broadcast en proximo paso\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
							ret=MPI_SUCCESS;
//							ACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=1;
						}
//						SI RECIBO INFORMACION DE UN PROCESO SIN COMPAÑERO DEL PASO ANTERIOR
						else if(!forget_broadcast){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Recibiendo bloque faltante\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//							MPI_Request request;
							mini=min(HvM,HvN);
//							INICIALIZA EL SUBBLOQUE A RECIBIR
							CjR=(double *)calloc(mini*mini,sizeof(double));
//							RECIBE LAS DIMENSIONES DE LA MATRIZ FALTANTE
							ret=safe_receive_times_with_tag((void *)CjR,mini*mini,MPI_DOUBLE,MPI_ANY_SOURCE,mpid->world_rank,mpid->world,mpid->status,MAX_TRY);
//							ret=MPI_Irecv((void *)CjR,mini*mini,MPI_DOUBLE,MPI_ANY_SOURCE,mpid->world_rank,mpid->world,&request);
//							ret=MPI_Wait(&request,&(mpid->status));
							if(ret!=MPI_SUCCESS){
								fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Error en recepcion de broadcast\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
								continue;
							}
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - mpid->status.MPI_SOURCE:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->status.MPI_SOURCE);
							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Bloque faltante recibido\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - CjR c\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//							print_matrix(Cj,mini,mini);
							if(Hv_sizes[index]==NULL){
								fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Hv_sizes[%d] no disponible\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,index);
							}
							else{
								md_sub=matrix_data_init(HvM,HvN,0,0,0,'R');
								ret=!MPI_SUCCESS;
								while(ret!=MPI_SUCCESS){
#ifdef _VERB_
									usleep(WAIT_TIME*mpid->world_rank);
									printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Recuperando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,HvM,HvN,mpid->world_size,mpid->world_rank,Hv_sizes[index][2]);
#endif /* _VERB_ */
									ret=read_R_Hv_TAU(md_sub->A,md_sub->TAU,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,Hv_sizes[index][2]);
								}
								set_Hv(md_sub);
//								CALCULA LA MATRIZ T
//								T=get_T_matrix(Hv_list[index],TAU_list[index],HvM,HvN);
								T=get_T_matrix(md_sub->Hv,md_sub->TAU,HvM,HvN);
								Hv1=(double *)calloc(mini*mini,sizeof(double));
								Hv1t_C1=(double *)calloc(mini*mini,sizeof(double));
								W=(double *)calloc(mini*mini,sizeof(double));
//								OBTIENE EL SUBBLOQUE INFERIOR DEL HOUSEHOLDER VECTOR
//								get_sub_block(Hv_list[index],HvM,HvM-mini,Hv1,mini,mini);
								get_sub_block(md_sub->Hv,HvM,HvM-mini,Hv1,mini,mini);
								if(index_prev<indexD_prev){
//									APLICA LA ECUACION Ĉ'i = C'i +        Ti,j' * (C'i + Hv'j' * C'j)
									mult_matrix_trans(Hv1,mini,mini,Cj,mini,mini,Hv1t_C1,0,'T','N');
									add_matrix(Ci,Hv1t_C1,mini,mini,'A');
									mult_matrix_trans(T,mini,mini,Hv1t_C1,mini,mini,W,0,'T','N');
									add_matrix(Ci,W,mini,mini,'A');
//									memcpy(Ci,W,mini*mini*sizeof(double));
									free(Ci);
									Ci=W;
//									RESPALDA EL RESULTADO DE TM EN EL ARCHIVO LOCAL
									write_subblock(Ci,mini,mini,mpid,rowNum,colNum,"FTCAQR_TM",'R');
//									usleep(WAIT_TIME*mpid->world_rank);
//									printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Ci a\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//									print_matrix(Ci,mini,mini);
								}
								else{
									Hv1_W=(double *)calloc(mini*mini,sizeof(double));
//									APLICA LA ECUACION Ĉ'j = C'j + Hv'j * Ti,j' * (C'i + Hv'j' * C'j)
									mult_matrix_trans(Hv1,mini,mini,Ci,mini,mini,Hv1t_C1,0,'T','N');
									add_matrix(Cj,Hv1t_C1,mini,mini,'A');
									mult_matrix_trans(T,mini,mini,Hv1t_C1,mini,mini,W,0,'T','N');
									mult_matrix(Hv1,mini,mini,W,mini,mini,Hv1_W,0);
									add_matrix(Ci,Hv1_W,mini,mini,'A');
//									memcpy(Ci,Hv1_W,mini*mini*sizeof(double));
//									if(Hv1_W!=NULL)
//										free(Hv1_W);
									free(Ci);
									Ci=Hv1_W;
//									RESPALDA EL RESULTADO DE TM EN EL ARCHIVO LOCAL
									write_subblock(Ci,mini,mini,mpid,rowNum,colNum,"FTCAQR_TM",'R');
//									usleep(WAIT_TIME*mpid->world_rank);
//									printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Ci b\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//									print_matrix(Ci,mini,mini);
								}
//								memcpy(Cj,CjR,mini*mini*sizeof(double));
//								LIBERA RECURSOS
								free(Cj);
								Cj=CjR;
//								if(CjR!=NULL)
//									free(CjR);
//								if(W!=NULL)
//									free(W);
								if(Hv1t_C1!=NULL)
									free(Hv1t_C1);
								if(Hv1!=NULL)
									free(Hv1);
								if(T!=NULL)
									free(T);
//								free(Cj);
								matrix_data_free(md_sub);
							}
//							AGREGA EL PROCESO FUENTE A SU LISTA DE MIRRORS
							add_mirror(mpid,mpid->status.MPI_SOURCE);
//							ACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=1;
//							already_back=1;
						}
//						SI YA RECIBI PREVIAMENTE EL BROADCAST
						else{
//							ACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=1;
//							already_back=1;
						}
					}
					else{
//						ELIMINA LOS PROCESOS QUE YA TIENEN LA MISMA INFORMACION QUE EL PROCESO SIN COMPAÑERO
						for(i=0;i<mpid->world_size;i++){
							if((broadcast_list[i] && mpid->mirror[i]) || (prev_alone_list[i] && i!=mpid->world_rank)){
#ifdef _VERB_
								usleep(WAIT_TIME*mpid->world_rank);
								printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Eliminando a [%d] de broadcast_list\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i);
#endif /* _VERB_ */
								broadcast_list[i]=0;
								broadcast_count--;
							}
						}
#ifdef _VERB_
						sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - broadcast_list restante: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
						for(i=0;i<mpid->world_size;i++){
							if(broadcast_list[i]){
								sprintf(temp,"[%d]",i);
								strcat(msg,temp);
							}
						}
						usleep(WAIT_TIME*mpid->world_rank);
						printf("%s\n",msg);
						printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - broadcast_count restante:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,broadcast_count);
#endif /* _VERB_ */
//						SI ME CORRESPONDE ENVIAR BROADCAST Y HAY PROCESOS QUE LO ESPERAN
						if(mpid->world_rank==first_alone && broadcast_count>0){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Enviando bloque faltante\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
							mini=min(HvM,HvN);
//							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Broadcast Ci d\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//							print_matrix(Ci,mini,mini);
//							ENVIA LA MATRIZ FALTANTE
							for(i=0;i<mpid->world_size;i++){
								if(broadcast_list[i]){
//									ENVIA EL BLOQUE FALTANTE
									ret=safe_send_times_with_tag((void *)Ci,mini*mini,MPI_DOUBLE,i,i,mpid->world,MAX_TRY);
									if(ret!=MPI_SUCCESS)
										fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Error al enviar subbloque a proceso[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i);
								}
							}
							if(ret!=MPI_SUCCESS){
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en envio de broadcast\n",mpid->world_rank,mpid->processor_name);
								continue;
							}
//							DESACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=0;
//							already_back=1;
						}
						else if(broadcast_count>0){
//							DESACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=0;
//							already_back=1;
						}
//						SI AUN NO ENCUENTRO COMPAÑERO
						else{
							index=index_prev;
							indexD=indexD_prev;
							ret=MPI_SUCCESS;
//							already_back=1;
						}
					}
				}
				else{
					mini=min(HvM,HvN);
//					SI ENVIO INFORMACION POR NO TENER COMPAÑERO EN EL PASO ANTERIOR
					if(broadcast==1){
//						ELIMINA LOS PROCESOS QUE YA TIENEN LA MISMA INFORMACION QUE EL PROCESO SIN COMPAÑERO
						for(i=0;i<mpid->world_size;i++){
							if((broadcast_list[i] && mpid->mirror[i]) || (prev_alone_list[i] && i!=mpid->world_rank)){
#ifdef _VERB_
								usleep(WAIT_TIME*mpid->world_rank);
								printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Eliminando a [%d] de broadcast_list\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i);
#endif /* _VERB_ */
								broadcast_list[i]=0;
								broadcast_count--;
							}
						}
#ifdef _VERB_
						sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - broadcast_list restante: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
						for(i=0;i<mpid->world_size;i++){
							if(broadcast_list[i]){
								sprintf(temp,"[%d]",i);
								strcat(msg,temp);
							}
						}
						usleep(WAIT_TIME*mpid->world_rank);
						printf("%s\n",msg);
						printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - broadcast_count restante:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,broadcast_count);
#endif /* _VERB_ */
//						SI ME CORRESPONDE ENVIAR BROADCAST Y HAY PROCESOS QUE LO ESPERAN
						if(mpid->world_rank==first_alone && broadcast_count>0){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Enviando bloque faltante\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Broadcast Ci d\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//							print_matrix(Ci,mini,mini);
//							ENVIA LA MATRIZ FALTANTE
							for(i=0;i<mpid->world_size;i++){
								if(broadcast_list[i]){
//									ENVIA EL BLOQUE FALTANTE
									ret=safe_send_times_with_tag((void *)Ci,mini*mini,MPI_DOUBLE,i,i,mpid->world,MAX_TRY);
									if(ret!=MPI_SUCCESS)
										fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Error al enviar subbloque a proceso[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,i);
								}
							}
							if(ret!=MPI_SUCCESS){
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en envio de broadcast\n",mpid->world_rank,mpid->processor_name);
								continue;
							}
//							DESACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=0;
//							already_back=1;
						}
						else if(broadcast_count>0){
//							DESACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=0;
//							already_back=1;
						}
//						SI AUN NO ENCUENTRO COMPAÑERO
						else{
//							index=index_prev;
//							indexD=indexD_prev;
							ret=MPI_SUCCESS;
//							already_back=1;
						}
					}
					if(Hv_sizes[index]==NULL){
						usleep(WAIT_TIME*mpid->world_rank);
						printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Hv_sizes[%d] no disponible\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,index);
					}
					else{
						md_sub=matrix_data_init(HvM,HvN,0,0,0,'R');
						ret=!MPI_SUCCESS;
						while(ret!=MPI_SUCCESS){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Recuperando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,HvM,HvN,mpid->world_size,mpid->world_rank,Hv_sizes[index][2]);
#endif /* _VERB_ */
							ret=read_R_Hv_TAU(md_sub->A,md_sub->TAU,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,Hv_sizes[index][2]);
						}
						set_Hv(md_sub);
//						CALCULA LA MATRIZ T
//						T=get_T_matrix(Hv_list[index],TAU_list[index],HvM,HvN);
						T=get_T_matrix(md_sub->Hv,md_sub->TAU,HvM,HvN);
						Hv1=(double *)calloc(mini*mini,sizeof(double));
						Hv1t_C1=(double *)calloc(mini*mini,sizeof(double));
						W=(double *)calloc(mini*mini,sizeof(double));
//						OBTIENE EL SUBBLOQUE INFERIOR DEL HOUSEHOLDER VECTOR
//						get_sub_block(Hv_list[index],HvM,HvM-mini,Hv1,mini,mini);
						get_sub_block(md_sub->Hv,HvM,HvM-mini,Hv1,mini,mini);
						if(index_prev<indexD_prev){
//							APLICA LA ECUACION Ĉ'i = C'i +        Ti,j' * (C'i + Hv'j' * C'j)
							mult_matrix_trans(Hv1,mini,mini,Cj,mini,mini,Hv1t_C1,0,'T','N');
							add_matrix(Ci,Hv1t_C1,mini,mini,'A');
							mult_matrix_trans(T,mini,mini,Hv1t_C1,mini,mini,W,0,'T','N');
							add_matrix(Ci,W,mini,mini,'A');
//								memcpy(Ci,W,mini*mini*sizeof(double));
							free(Ci);
							Ci=W;
//							RESPALDA EL RESULTADO DE TM EN EL ARCHIVO LOCAL
							write_subblock(Ci,mini,mini,mpid,rowNum,colNum,"FTCAQR_TM",'R');
//							usleep(WAIT_TIME*mpid->world_rank);
//							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Ci e\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//							print_matrix(Ci,mini,mini);
						}
						else{
							Hv1_W=(double *)calloc(mini*mini,sizeof(double));
//							APLICA LA ECUACION Ĉ'j = C'j + Hv'j * Ti,j' * (C'i + Hv'j' * C'j)
							mult_matrix_trans(Hv1,mini,mini,Ci,mini,mini,Hv1t_C1,0,'T','N');
							add_matrix(Cj,Hv1t_C1,mini,mini,'A');
							mult_matrix_trans(T,mini,mini,Hv1t_C1,mini,mini,W,0,'T','N');
							mult_matrix(Hv1,mini,mini,W,mini,mini,Hv1_W,0);
							add_matrix(Ci,Hv1_W,mini,mini,'A');
//							memcpy(Ci,Hv1_W,mini*mini*sizeof(double));
//							if(Hv1_W!=NULL)
//								free(Hv1_W);
							free(Ci);
							Ci=Hv1_W;
//							RESPALDA EL RESULTADO DE TM EN EL ARCHIVO LOCAL
							write_subblock(Ci,mini,mini,mpid,rowNum,colNum,"FTCAQR_TM",'R');
//							usleep(WAIT_TIME*mpid->world_rank);
//							printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Ci f\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//							print_matrix(Ci,mini,mini);
						}
//						LIBERA RECURSOS
//						if(W!=NULL)
//							free(W);
						if(Hv1!=NULL)
							free(Hv1);
						if(Hv1t_C1!=NULL)
							free(Hv1t_C1);
						if(T!=NULL)
							free(T);
//						free(Cj);
						matrix_data_free(md_sub);
					}
//					INICIALIZA EL SUBBLOQUE A RECIBIR
					memset(Cj,0,mini*mini*sizeof(double));
//					INTERCAMBIA MATRICES Ci,Cj
					ret=send_receive_matrix(Ci,Cj,mini,mini,mpid);
//					usleep(WAIT_TIME*mpid->world_rank);
//					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Cj g\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(Cj,mini,mini);
				}
			}
			mpid->step_finished=flag=(ret==MPI_SUCCESS);
#ifdef _VERB_
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Ejecutado paso [%d] - ret:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step,ret);
#endif /* _VERB_ */
#ifndef _OMPI_
			MPIX_Comm_agree(mpid->world,&flag);
#else
			if(!flag){
				fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step);
				return(ret);
			}
			MPI_Barrier(mpid->world);
#endif /* _OMPI_ */
//			SI NO OCURRIO ERROR, CONTINUA
			if(flag)
				break;
#ifndef _OMPI_
#ifdef _TAU_
			TAU_START(TAU_time_rest);
#endif /* _TAU_ */
			time_rest=MPI_Wtime();
			fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step);
			while(1){
//				CADA PROCESO OBTIENE UNA LISTA DE PROCESOS FALLIDOS
				dead_rank_list=dead_list(mpid,&dead_list_size);
//				CADA PROCESO OBTIENE UNA LISTA DE PROCESOS SOBREVIVIENTES
				surv_rank_list=survivor_list(mpid,dead_rank_list,dead_list_size,&surv_list_size);
#ifdef _VERB_
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Procesos fallidos: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
				for(i=0;i<dead_list_size;i++){
					sprintf(temp,"[%d]",dead_rank_list[i]);
					strcat(msg,temp);
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
				sprintf(msg,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Procesos sobrevivientes: ",mpid->world_rank,mpid->processor_name,rowNum,colNum);
				for(i=0;i<surv_list_size;i++){
					sprintf(temp,"[%d]",surv_rank_list[i]);
					strcat(msg,temp);
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
#endif /* _VERB_ */
//				RESTAURA A LOS PROCESOS FALLIDOS
				ret=replace_partners(mpid,dead_rank_list,dead_list_size);
//				SI LA RESTAURACION FALLO, INTENTA DE NUEVO
				if(ret!=MPI_SUCCESS){
					free(dead_rank_list);
					free(surv_rank_list);
					MPI_Error_string(ret,err,&err_len);
					fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Error al restaurar procesos [%d][%s]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,ret,err);
					continue;
				}
				break;
			}
//			SE BUSCA LA POSICION EN LA LISTA DE SOBREVIVIENTES
			indexS=0;
			while(mpid->world_rank!=surv_rank_list[indexS]){
				indexS++;
			}
//			SI ME CORRESPONDE ENVIAR DATOS A UN PROCESO FALLIDO
			if(indexS<dead_list_size){
				for(i=indexS;i<dead_list_size;i+=surv_list_size){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Enviando paso inicial a [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,dead_rank_list[i]);
#endif /* _VERB_ */
//					INTENTA ENVIAR EL PASO DE INICIO
					ret=safe_send_times((void *)(&(mpid->step)),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//					SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
					if(ret!=MPI_SUCCESS){
						MPI_Error_string(ret,err,&err_len);
						fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Error al distribuir paso inicial a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,dead_rank_list[i],ret,err);
						continue;
					}
//					INTENTA ENVIAR STAGE ACTUAL
					ret=safe_send_times((void *)(&(stage)),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//					SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
					if(ret!=MPI_SUCCESS){
						MPI_Error_string(ret,err,&err_len);
						fprintf(stderr,"Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Error al distribuir stage actual a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,dead_rank_list[i],ret,err);
						continue;
					}
					printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,dead_rank_list[i]);
				}
			}
			free(surv_rank_list);
			free(dead_rank_list);
			time_rest_final+=MPI_Wtime()-time_rest;
#ifdef _TAU_
			TAU_STOP(TAU_time_rest);
#endif /* _TAU_ */
#endif /* _OMPI_ */
		}
//		ACTUALIZA DESTINOS DE TODOS LOS PROCESOS
		index_prev=index;
		indexD_prev=indexD;
		add_mirror(mpid,mpid->dest[mpid->world_rank]);
		update_dest(mpid);
		mpid->step_finished=0;
	}
#ifdef _VERB_
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Ejecutando ultimo paso [%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,mpid->step);
#endif /* _VERB_ */
	index=get_Hv_index(mpid->step,mpid->world_rank,mpid->dest[mpid->world_rank]);
	indexD=get_Hv_index(mpid->step,mpid->dest[mpid->world_rank],mpid->world_rank);

	if(Hv_sizes[index]==NULL){
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Hv_sizes[%d] no disponible\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,index);
	}
	else{
		HvM=Hv_sizes[index][0];
		HvN=Hv_sizes[index][1];
//		printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - get_panel[%d] - HvM:[%d] - HvN:[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,index,HvM,HvN);
		md_sub=matrix_data_init(HvM,HvN,0,0,0,'R');
		ret=!MPI_SUCCESS;
		while(ret!=MPI_SUCCESS){
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - TMU - Recuperando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,HvM,HvN,mpid->world_size,mpid->world_rank,Hv_sizes[index][2]);
#endif /* _VERB_ */
			ret=read_R_Hv_TAU(md_sub->A,md_sub->TAU,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,Hv_sizes[index][2]);
		}
		set_Hv(md_sub);
//		CALCULA LA MATRIZ T
//		T=get_T_matrix(Hv_list[index],TAU_list[index],HvM,HvN);
		T=get_T_matrix(md_sub->Hv,md_sub->TAU,HvM,HvN);
		mini=min(HvM,HvN);
		Hv1=(double *)calloc(mini*mini,sizeof(double));
		Hv1t_C1=(double *)calloc(mini*mini,sizeof(double));
		W=(double *)calloc(mini*mini,sizeof(double));
//		OBTIENE EL SUBBLOQUE INFERIOR DEL HOUSEHOLDER VECTOR
//		get_sub_block(Hv_list[index],HvM,HvM-mini,Hv1,mini,mini);
		get_sub_block(md_sub->Hv,HvM,HvM-mini,Hv1,mini,mini);
		if(index_prev<indexD_prev){
//			APLICA LA ECUACION Ĉ'i = C'i +        Ti,j' * (C'i + Hv'j' * C'j)
			mult_matrix_trans(Hv1,mini,mini,Cj,mini,mini,Hv1t_C1,0,'T','N');
			add_matrix(Ci,Hv1t_C1,mini,mini,'A');
			mult_matrix_trans(T,mini,mini,Hv1t_C1,mini,mini,W,0,'T','N');
			add_matrix(Ci,W,mini,mini,'A');
//			memcpy(Ci,W,mini*mini*sizeof(double));
			free(Ci);
			Ci=W;
//			RESPALDA EL RESULTADO DE TM EN EL ARCHIVO LOCAL
			write_subblock(Ci,mini,mini,mpid,rowNum,colNum,"FTCAQR_R",'R');
		}
		else{
			Hv1_W=(double *)calloc(mini*mini,sizeof(double));
//			APLICA LA ECUACION Ĉ'j = C'j + Hv'j * Ti,j' * (C'i + Hv'j' * C'j)
			mult_matrix_trans(Hv1,mini,mini,Ci,mini,mini,Hv1t_C1,0,'T','N');
			add_matrix(Cj,Hv1t_C1,mini,mini,'A');
			mult_matrix_trans(T,mini,mini,Hv1t_C1,mini,mini,W,0,'T','N');
			mult_matrix(Hv1,mini,mini,W,mini,mini,Hv1_W,0);
			add_matrix(Ci,Hv1_W,mini,mini,'A');
//			memcpy(Ci,Hv1_W,mini*mini*sizeof(double));
//			if(Hv1_W!=NULL)
//				free(Hv1_W);
			free(Ci);
			Ci=Hv1_W;
//			RESPALDA EL RESULTADO DE TM EN EL ARCHIVO LOCAL
			write_subblock(Ci,mini,mini,mpid,rowNum,colNum,"FTCAQR_TM",'R');
		}
//		usleep(WAIT_TIME*mpid->world_rank);
//		printf("Proceso [%d] en [%s]: CAQR - grid:[%d,%d] - Ci final\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//		print_matrix(Ci,mini,mini);
		memcpy(md_local->A,Ci,mini*mini*sizeof(double));
//		LIBERA RECURSOS
//		if(W!=NULL)
//			free(W);
		if(Hv1!=NULL)
			free(Hv1);
		if(Hv1t_C1!=NULL)
			free(Hv1t_C1);
		if(T!=NULL)
			free(T);
		if(Cj!=NULL)
			free(Cj);
		if(Ci!=NULL)
			free(Ci);
		matrix_data_free(md_sub);
	}
	return(ret);
}

double *get_T_matrix(double *Hv,double *TAU,int HvM,int HvN){
	int i;
	int mini=min(HvM,HvN);
	double *T=(double *)calloc(mini*mini,sizeof(double));
	double *vj=(double *)calloc(HvM,sizeof(double));
	double *Yt;
	double *Yt_vj;
	double *T_sub;
	double *z;
	for(i=0;i<mini;i++)
		T[i*mini+i]=-TAU[i];
	for(i=1;i<mini;i++){
		Yt=(double *)calloc(HvM*i,sizeof(double));
		Yt_vj=(double *)calloc(i,sizeof(double));
		T_sub=(double *)calloc(i*i,sizeof(double));
		z=(double *)calloc(i,sizeof(double));
		memcpy(vj,Hv+HvM*i,HvM*sizeof(double));
		memcpy(Yt,Hv,HvM*i*sizeof(double));
		get_sub_block(T,mini,0,T_sub,i,i);
//		z = -TAU * T_sub * Y^T * vj
		mult_matrix_trans(Yt,i,HvM,vj,HvM,1,Yt_vj,0,'T','N');
		mult_matrix(T_sub,i,i,Yt_vj,i,1,z,0);
		mult_matrix_scalar(z,i,1,-TAU[i]);
		set_sub_block(T,mini,mini*i,z,i,1);
		free(z);
		free(T_sub);
		free(Yt_vj);
		free(Yt);
	}
	free(vj);
	return(T);
}

