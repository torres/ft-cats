#ifndef _FT_CACH_H_
#define _FT_CACH_H_

#include "ca_util.h"

int ft_cach(fttsch_data *ftd,MPI_data *mpid,int M,int N,char *in,int argc,char **argv);
int update_trailing_matrix(MPI_data *mpid,double *L);

#endif /* _FT_CACH_H_ */

