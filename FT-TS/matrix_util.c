#include "matrix_util.h"

double *concat_U_blocks_first_step(double *A,int MA,int NA,double *B,int MB,int NB,char up){
	int M=NA+NB;
	int i,j,upperM,lowerM;
	double *upper;
	double *lower;
	double *AB=(double *)calloc(M*NA,sizeof(double));
	if(up=='A'){
		upperM=MA;
		lowerM=MB;
		upper=A;
		lower=B;
	}
	else{
		upperM=MB;
		lowerM=MA;
		upper=B;
		lower=A;
	}
	time_copy=MPI_Wtime();
	for(j=0;j<NA;j++){
		for(i=0;i<=j;i++){
			AB[j*M+i]=upper[j*upperM+i];
			AB[j*M+(i+NA)]=lower[j*lowerM+i];
		}
	}
	time_copy_final+=MPI_Wtime()-time_copy;
	return(AB);
}

double *concat_U_blocks(double *A,int NA,double *B,int NB,char up){
	int M=NA+NB;
	int i,upperM,lowerM;
	double *upper;
	double *lower;
	double *AB=(double *)calloc(M*NA,sizeof(double));
	if(up=='A'){
		upperM=NA;
		lowerM=NB;
		upper=A;
		lower=B;
	}
	else{
		upperM=NB;
		lowerM=NA;
		upper=B;
		lower=A;
	}
	time_copy=MPI_Wtime();
	for(i=0;i<NA;i++){
		memcpy(AB+i*M,upper+i*upperM,upperM*sizeof(double));
		memcpy(AB+i*M+upperM,lower+i*lowerM,lowerM*sizeof(double));
	}
	time_copy_final+=MPI_Wtime()-time_copy;
	return(AB);
}

double *concat_U_blocks_horizontal(double *A,int MA,int NA,double *B,int MB,int NB,char l){
	int N=NA+NB;
	int leftM,rightM,leftN,rightN;
	double *left;
	double *right;
	double *AB=(double *)calloc(MA*N,sizeof(double));
	if(l=='A'){
		leftM=MA;
		rightM=MB;
		leftN=NA;
		rightN=NB;
		left=A;
		right=B;
	}
	else{
		leftM=MB;
		rightM=MA;
		leftN=NB;
		rightN=NA;
		left=B;
		right=A;
	}
	time_copy=MPI_Wtime();
	memcpy(AB,left,leftM*leftN*sizeof(double));
	memcpy(AB+leftM*leftN,right,rightM*rightN*sizeof(double));
	time_copy_final+=MPI_Wtime()-time_copy;
	return(AB);
}

double *concat_R_blocks_first_step(double *A,int MA,int NA,double *B,int MB,int NB,char up){
	int M=MA+MB;
	int i,j,upperM,lowerM;
	double *upper;
	double *lower;
	double *AB=(double *)calloc(M*NA,sizeof(double));
	if(up=='A'){
		upperM=MA;
		lowerM=MB;
		upper=A;
		lower=B;
	}
	else{
		upperM=MB;
		lowerM=MA;
		upper=B;
		lower=A;
	}
	time_copy=MPI_Wtime();
	for(j=0;j<NA;j++){
		for(i=0;i<=j;i++){
			AB[j*M+i]=upper[j*upperM+i];
			AB[j*M+(i+NA)]=lower[j*lowerM+i];
		}
	}
	time_copy_final+=MPI_Wtime()-time_copy;
	return(AB);
}

double *concat_R_blocks(double *A,int MA,int NA,double *B,int MB,int NB,char up){
	int M=MA+MB;
	int i,upperM,lowerM;
	double *upper;
	double *lower;
	double *AB=(double *)calloc(M*NA,sizeof(double));
	if(up=='A'){
		upperM=MA;
		lowerM=MB;
		upper=A;
		lower=B;
	}
	else{
		upperM=MB;
		lowerM=MA;
		upper=B;
		lower=A;
	}
	time_copy=MPI_Wtime();
	for(i=0;i<NA;i++){
		memcpy(AB+i*M,upper+i*upperM,upperM*sizeof(double));
		memcpy(AB+i*M+upperM,lower+i*lowerM,lowerM*sizeof(double));
	}
	time_copy_final+=MPI_Wtime()-time_copy;
	return(AB);
}

double *concat_R_blocks_horizontal(double *A,int MA,int NA,double *B,int MB,int NB,char l){
	int N=NA+NB;
	int leftM,rightM,leftN,rightN;
	double *left;
	double *right;
	double *AB=(double *)calloc(MA*N,sizeof(double));
	if(l=='A'){
		leftM=MA;
		rightM=MB;
		leftN=NA;
		rightN=NB;
		left=A;
		right=B;
	}
	else{
		leftM=MB;
		rightM=MA;
		leftN=NB;
		rightN=NA;
		left=B;
		right=A;
	}
	time_copy=MPI_Wtime();
	memcpy(AB,left,leftM*leftN*sizeof(double));
	memcpy(AB+leftM*leftN,right,rightM*rightN*sizeof(double));
	time_copy_final+=MPI_Wtime()-time_copy;
	return(AB);
}

double *pack_U(double *U,int M,int N){
	int i,j,k=0;
	int size=N+((int)((N*(N-1))/2));
	double *packed=(double *)calloc(size,sizeof(double));
	time_copy=MPI_Wtime();
	for(j=0;j<N;j++)
		for(i=0;i<=j;i++,k++)
			packed[k]=U[j*M+i];
	time_copy_final+=MPI_Wtime()-time_copy;
	return(packed);
}

double *unpack_U(double *U,int N){
	int i,j,k=0;
	double *matrix=(double *)calloc(N*N,sizeof(double));
	time_copy=MPI_Wtime();
	for(j=0;j<N;j++)
		for(i=0;i<=j;i++,k++)
			matrix[j*N+i]=U[k];
	time_copy_final+=MPI_Wtime()-time_copy;
	return(matrix);
}

double *pad_U(double *U,int MU,int M,int N){
	int i,j;
	double *U_new=(double *)malloc(M*N*sizeof(double));
	memset(U_new,0,M*N*sizeof(double));
	time_copy=MPI_Wtime();
	for(i=0;i<MU;i++){
		for(j=i;j<MU;j++){
			U_new[j*M+i]=U[j*MU+i];
		}
	}
	time_copy_final+=MPI_Wtime()-time_copy;
	return(U_new);
}

void get_sub_block(double *A,int MA,int index,double *sub,int M,int N){
	int sub_tam=M*N;
	int count=0;
	time_copy=MPI_Wtime();
	while(count<sub_tam){
		memcpy(sub+count,A+index,M*sizeof(double));
		count+=M;
		index+=MA;
	}
	time_copy_final+=MPI_Wtime()-time_copy;
}

void set_sub_block(double *A,int MA,int index,double *sub,int M,int N){
	int sub_tam=M*N;
	int count=0;
	time_copy=MPI_Wtime();
	while(count<sub_tam){
		memcpy(A+index,sub+count,M*sizeof(double));
		count+=M;
		index+=MA;
	}
	time_copy_final+=MPI_Wtime()-time_copy;
}

void print_matrix(double *A,int M,int N){
	int i,j;
	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			if(A[j*M+i]>=0.0)
				printf("+");
			printf("%.4lf ",A[j*M+i]);
		}
		printf("\n");
	}
}

void print_IPIV(int *IPIV,int n){
	int i;
	for(i=0;i<n;i++)
		printf("%d ",IPIV[i]);
	printf("\n");
}

void print_TAU(double *TAU,int n){
	int i;
	for(i=0;i<n;i++)
		printf("%.6f ",TAU[i]);
	printf("\n");
}

