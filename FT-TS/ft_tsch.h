#ifndef _FT_TSCH_H_
#define _FT_TSCH_H_

#include "lapack.h"
#include "mpi_data.h"
#include "matrix_data.h"

typedef struct _fttsch_data{
	matrix_data *md;
	MPI_data *mpid;
}fttsch_data;

int ft_tsch_init(fttsch_data *ftd,double *A,int M,int N,char *in,int argc,char **argv);
int ft_tsch(fttsch_data *ftd);
int send_broadcast(MPI_data *mpid,matrix_data *md_sub);
matrix_data *receive_broadcast(MPI_data *mpid,matrix_data *md_sub);
void broadcast_block(MPI_data *mpid,matrix_data *md,matrix_data *md_sub);
int write_L(double *A,int M,int N,int size,int rank,int step);
int read_L(double *A,int M,int N,int size,int rank,int step);

void backup_results_alone(MPI_data *mpid,matrix_data *md,matrix_data *md_sub,int index);
void free_fttsch_data(fttsch_data *ftd);
void init_variables_ch(int world_size);

int step_lim;

#endif /* _FT_TSCH_H_ */

