#ifndef _FT_TSQR_H_
#define _FT_TSQR_H_

#include "lapack.h"
#include "mpi_data.h"
#include "matrix_data.h"

#define MR_POS				0
#define NR_POS				1

typedef struct _fttsqr_data{
	matrix_data *md;
	MPI_data *mpid;
}fttsqr_data;

int ft_tsqr_init(fttsqr_data *ftd,double *A,int M,int N,int q_c,char *in,int argc,char **argv);
int ft_tsqr_spawned(fttsqr_data *ftd);
int ft_tsqr(fttsqr_data *ftd,int q_c);
int restore_data_qr(fttsqr_data *ftd);
int send_broadcast(MPI_data *mpid,int *broadcast_list,int *broadcast_dim,int broadcast_count);
matrix_data *receive_broadcast(MPI_data *mpid,int *broadcast_dim);
matrix_data *first_middle_step_qr(matrix_data *md_sub,MPI_data *mpid);
int last_step_qr(matrix_data *md_sub);
int get_Hv_index(int step,int world_rank,int world_rankR);
int write_R_Hv_TAU(double *A,double *TAU,int M,int N,int size,int rank,int step);
int read_R_Hv_TAU(double *A,double *TAU,int M,int N,int size,int rank,int step);

void backup_results_qr(MPI_data *mpid,matrix_data *md,matrix_data *md_sub,int index,matrix_data *md_subR,int indexD,char up,int change_dest,int new_dest);
void backup_results_alone(MPI_data *mpid,matrix_data *md,matrix_data *md_sub,int index);
void concatenate_matrices_qr(matrix_data *md_sub,matrix_data *md_subR,int step,char up);
void free_fttsqr_data(fttsqr_data *ftd);
void init_variables_qr(int q_c,int world_size);
void reset_lists();

int concat_sizes[MAX_CONCAT_SIZES];
int q_calc;
int step_lim;
int MR,NR;
int broadcast_index;
double *concatR;

//double **TAU_list;
//double **Hv_list;
int **Hv_sizes;
int Hv_list_size;

#ifndef _OMPI_
int MR_prev,NR_prev;
double *concatR_prev;
double *concatR_ptr;
#endif /* _OMPI_ */

#endif /* _FT_TSQR_H_ */

