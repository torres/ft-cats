#include "ft_tsqr.h"

int ft_tsqr_init(fttsqr_data *ftd,double *A,int M,int N,int q_c,char *in,int argc,char **argv){
	MPI_data *mpid=ftd->mpid;
	ftd->md=matrix_data_init_with_matrix(M,N,M/ftd->mpid->world_size,N,A,1,'R');
	if(ftd->md==NULL){
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al inicializar panel FT-TSQR\n",mpid->world_rank,mpid->processor_name);
		return(EXIT_FAILURE);
	}
	init_variables_qr(q_c,mpid->world_size);
//	int ret=ft_tsqr_spawned(ftd);
//	if(ret!=MPI_SUCCESS){
//		free_timers(mpid->world_size);
//		return(EXIT_FAILURE);
//	}
	reset_lists();
	Hv_list_size=(int)pow(2,step_lim-1)+1;
//	printf("Proceso [%d] en [%s]: TSQR - Hv_list_size_init:[%d]\n",mpid->world_rank,mpid->processor_name,Hv_list_size);
	Hv_sizes=(int **)calloc(Hv_list_size,sizeof(int *));
//	Hv_list=(double **)calloc(Hv_list_size,sizeof(double *));
//	TAU_list=(double **)calloc(Hv_list_size,sizeof(double *));
	return(MPI_SUCCESS);
}

int ft_tsqr_spawned(fttsqr_data *ftd){
#ifndef _OMPI_
	int ret;
	MPI_data *mpid=ftd->mpid;
	if(mpid->spawned){
		time_rest=MPI_Wtime();
		ret=restore_data_qr(ftd);
		time_rest_final+=MPI_Wtime()-time_rest;
		if(ret!=MPI_SUCCESS){
			fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al restaurar datos\n",mpid->world_rank,mpid->processor_name);
			return(EXIT_FAILURE);
		}
	}
#endif /* _OMPI_ */
	return(MPI_SUCCESS);
}

int ft_tsqr(fttsqr_data *ftd,int q_c){
	MPI_data *mpid=ftd->mpid;
	matrix_data *md=ftd->md;
//	init_variables(q_c,mpid->world_size);
	time_exec=MPI_Wtime();
	matrix_data *md_sub=NULL;
	matrix_data *md_subR=NULL;
#ifdef _VERB_
	char msg[1024];
	char temp[1024];
#endif /* _VERB_ */
	double *subblock;
	int index,indexD;
	int ret=0,i;
	int flag;
	int already_back=0;
	int broadcast=0;
	int broadcast_dim[3];

	int broadcast_list[mpid->world_size];
	int prev_alone_list[mpid->world_size];
	int broadcast_count;
	int alone_count;
	int dest;
	int first_alone;
	int forget_broadcast;

	int working_block[mpid->world_size];
	for(i=0;i<mpid->world_size;i++)
		working_block[i]=i;
#ifndef _OMPI_
	char err[MPI_MAX_ERROR_STRING];
	int err_len,k;
	int curr_step;
	int indexS;
	int avail;
	int dead_list_size;
	int surv_list_size;
	int *dead_rank_list;
	int *surv_rank_list;
	if(mpid->spawned){
		time_rest=MPI_Wtime();
		curr_step=mpid->step;
		mpid->step=0;
		for(ret=0;ret<curr_step;ret++){
//			ACTUALIZA INDICES DE TRABAJO DE TODOS LOS PROCESOS
			for(i=0;i<mpid->world_size;i++)
				working_block[i]=min(working_block[i],working_block[mpid->dest[i]]);
//			ACTUALIZA DESTINOS DE TODOS LOS PROCESOS
			add_mirror(mpid,mpid->dest[i]);
			update_dest(mpid);
		}
		mpid->step=curr_step;
		mpid->spawned=!mpid->spawned;
		time_rest_final+=MPI_Wtime()-time_rest;
	}
#endif /* _OMPI_ */
	while(mpid->step<step_lim-1){
//		PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO
		index=get_block_index(md->Mb,md->Nb,md->M,md->N,working_block[mpid->world_rank]);
//		PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO DESTINO
		indexD=get_block_index(md->Mb,md->Nb,md->M,md->N,working_block[mpid->dest[mpid->world_rank]]);
//		INTENTA	EJECUTAR UN PASO DEL ALGORITMO
		while(1){
#ifdef _VERB_
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: TSQR - Ejecutando paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
#endif /* _VERB_ */
/*			if(mpid->world_rank==1 && mpid->step==0 && !mpid->spawned){
				mpid->spawned=!mpid->spawned;
				printf("Proceso [%d] en [%s]: TSQR - Muriendo...\n",mpid->world_rank,mpid->processor_name);
				raise(SIGKILL);
			}*/
			if(mpid->step_finished)
				ret=MPI_SUCCESS;
			else if(mpid->step==0){
				subblock=(double *)calloc(md->Mb*md->Nb,sizeof(double));
//				OBTENER EL PRIMER SUBBLOQUE A PARTIR DEL ELEMENTO index
				get_sub_block(md->A,md->M,index,subblock,md->Mb,md->Nb);
//				INICIALIZA EL SUBBLOQUE A PROCESAR LOCALMENTE
				md_sub=matrix_data_init_with_matrix(md->Mb,md->Nb,0,0,subblock,0,'R');
				free(subblock);
//				SI EL PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO LOCAL Y DESTINO ES IGUAL
				if(index==indexD){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: TSQR - Sin compañero. Preparado para broadcast en proximo paso\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
//					EJECUTA EL PRIMER PASO EN SOLITARIO
					qr_matrix(md_sub->A,md_sub->M,md_sub->N,md_sub->TAU,md_sub->WORK);
					ret=MPI_SUCCESS;
//					ACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
					broadcast=1;
				}
				else{
//					EJECUTA EL PRIMER PASO
					time_step=MPI_Wtime();
//					EJECUTA DGEQRF SOBRE EL SUBBLOQUE
					qr_matrix(md_sub->A,md_sub->M,md_sub->N,md_sub->TAU,md_sub->WORK);
					md_subR=first_middle_step_qr(md_sub,mpid);
					time_steps[mpid->step]+=MPI_Wtime()-time_step;
					ret=(md_subR!=NULL) ? MPI_SUCCESS : !MPI_SUCCESS;
/*
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: TSQR - md_subR\n",mpid->world_rank,mpid->processor_name);
					print_matrix(md_subR->A,md_subR->M,md_subR->N);
*/
				}
/*
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: TSQR - md_sub\n",mpid->world_rank,mpid->processor_name);
				print_matrix(md_sub->A,md_sub->M,md_sub->N);
*/
			}
			else{
//				INICIALIZA EL SUBBLOQUE A PROCESAR LOCALMENTE
				md_sub=matrix_data_init_with_matrix(MR,NR,0,0,concatR,0,'R');
/*
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: TSQR - md_sub\n",mpid->world_rank,mpid->processor_name);
				print_matrix(md_sub->A,md_sub->M,md_sub->N);
*/
//				CALCULA LISTA DE PROCESOS SIN COMPAÑERO EN EL PASO ACTUAL Y EL ANTERIOR
				memset(broadcast_list,0,mpid->world_size*sizeof(int));
				memset(prev_alone_list,0,mpid->world_size*sizeof(int));
				broadcast_count=0;
				alone_count=0;
				for(i=0;i<mpid->world_size;i++){
					dest=get_dest(i,mpid->step,mpid->world_size);
					if(i==dest && i!=mpid->world_rank){
						broadcast_list[i]=1;
						broadcast_count++;
					}
					dest=get_dest(i,mpid->step-1,mpid->world_size);
					if(i==dest){
						prev_alone_list[i]=1;
						alone_count++;
					}
				}
//				CALCULA EL PRIMER PROCESO SIN COMPAÑERO
				first_alone=0;
				while(!prev_alone_list[first_alone]){
					first_alone++;
				}
#ifdef _VERB_
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: TSQR - Estado de broadcast:[%d] - first_alone:[%d] - broadcast_count:[%d]\n",mpid->world_rank,mpid->processor_name,broadcast,first_alone,broadcast_count);
				sprintf(msg,"Proceso [%d] en [%s]: TSQR - broadcast_list: ",mpid->world_rank,mpid->processor_name);
				for(i=0;i<mpid->world_size;i++){
					if(broadcast_list[i]){
						sprintf(temp,"[%d]",i);
						strcat(msg,temp);
					}
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
				sprintf(msg,"Proceso [%d] en [%s]: TSQR - prev_alone_list: ",mpid->world_rank,mpid->processor_name);
				for(i=0;i<mpid->world_size;i++){
					if(prev_alone_list[i]){
						sprintf(temp,"[%d]",i);
						strcat(msg,temp);
					}
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
				sprintf(msg,"Proceso [%d] en [%s]: TSQR - mpid->mirror: ",mpid->world_rank,mpid->processor_name);
				for(i=0;i<mpid->world_size;i++){
					if(mpid->mirror[i]){
						sprintf(temp,"[%d]",i);
						strcat(msg,temp);
					}
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
#endif /* _VERB_ */
//				SI EL PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO LOCAL Y DESTINO ES IGUAL
				if(index==indexD){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: TSQR - Sin compañero. Preparado para broadcast en proximo paso\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */

//					SI ME CORRESPONDE RECIBIR BROADCAST
					if(broadcast==0){
//						VERIFICA QUE NO TENGA LA MISMA INFORMACION QUE EL PRIMER PROCESO SIN COMPAÑERO
						forget_broadcast=0;
						for(i=0;i<mpid->world_size;i++){
//							SI YA TENGO LA MISMA INFORMACION, NO RECIBO EL BROADCAST
							if(mpid->mirror[i] && i==first_alone){
#ifdef _VERB_
								usleep(WAIT_TIME*mpid->world_rank);
								printf("Proceso [%d] en [%s]: TSQR - Omitiendo broadcast de [%d]\n",mpid->world_rank,mpid->processor_name,i);
#endif /* _VERB_ */
								forget_broadcast=1;
								break;
							}
						}
//						SI EN UN PASO INTERMEDIO ME QUEDE SIN COMPAÑERO Y NO RECIBO BROADCAST
						if(mpid->step+1<step_lim-1 && alone_count==0){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - Preparado para broadcast en proximo paso\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
//							EJECUTA UN PASO INTERMEDIO
							qr_matrix(md_sub->A,md_sub->M,md_sub->N,md_sub->TAU,md_sub->WORK);
							ret=MPI_SUCCESS;
//							ACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=1;
						}
//						SI RECIBO INFORMACION DE UN PROCESO SIN COMPAÑERO DEL PASO ANTERIOR
						else if(!forget_broadcast){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - Recibiendo dimensiones de bloque faltante\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
							md_subR=receive_broadcast(mpid,broadcast_dim);
							if(md_subR==NULL){
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en receive_broadcast\n",mpid->world_rank,mpid->processor_name);
								continue;
							}
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - Bloque faltante recibido\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
/*
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - md_subR->R\n",mpid->world_rank,mpid->processor_name);
							print_matrix(md_subR->R,md_subR->M,md_subR->N);
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - md_subR->Hv\n",mpid->world_rank,mpid->processor_name);
							print_matrix(md_subR->Hv,md_subR->M,md_subR->N);
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - md_subR->TAU\n",mpid->world_rank,mpid->processor_name);
							print_matrix(md_subR->TAU,1,min(md_subR->M,md_subR->N));
*/
//							EJECUTA UN PASO INTERMEDIO
							qr_matrix(md_sub->A,md_sub->M,md_sub->N,md_sub->TAU,md_sub->WORK);
//							RESPALDA RESULTADOS
							indexD=get_block_index(md->Mb,md->Nb,md->M,md->N,working_block[mpid->status.MPI_SOURCE]);
							backup_results_qr(mpid,md,md_sub,index,md_subR,indexD,(mpid->world_rank<=mpid->status.MPI_SOURCE) ? 'A' : 'B',1,mpid->status.MPI_SOURCE);
							concatenate_matrices_qr(md_sub,md_subR,mpid->step,(mpid->world_rank<=mpid->status.MPI_SOURCE) ? 'A' : 'B');
//							AGREGA EL PROCESO FUENTE A SU LISTA DE MIRRORS
							add_mirror(mpid,mpid->status.MPI_SOURCE);
//							LIBERA RECURSOS
							matrix_data_free(md_subR);
//							ACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=1;
							already_back=1;
						}
//						SI YA RECIBI PREVIAMENTE EL BROADCAST
						else{
//							ACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=1;
							already_back=1;
						}
					}
//					SI ME CORRESPONDE ENVIAR BROADCAST
					else{
//						ELIMINA LOS PROCESOS QUE YA TIENEN LA MISMA INFORMACION QUE EL PROCESO SIN COMPAÑERO
						for(i=0;i<mpid->world_size;i++){
							if((broadcast_list[i] && mpid->mirror[i]) || (prev_alone_list[i] && i!=mpid->world_rank)){
#ifdef _VERB_
								usleep(WAIT_TIME*mpid->world_rank);
								printf("Proceso [%d] en [%s]: TSQR - Eliminando a [%d] de broadcast_list\n",mpid->world_rank,mpid->processor_name,i);
#endif /* _VERB_ */
								broadcast_list[i]=0;
								broadcast_count--;
							}
						}
#ifdef _VERB_
						sprintf(msg,"Proceso [%d] en [%s]: TSQR - broadcast_list restante: ",mpid->world_rank,mpid->processor_name);
						for(i=0;i<mpid->world_size;i++){
							if(broadcast_list[i]){
								sprintf(temp,"[%d]",i);
								strcat(msg,temp);
							}
						}
						usleep(WAIT_TIME*mpid->world_rank);
						printf("%s\n",msg);
						printf("Proceso [%d] en [%s]: TSQR - broadcast_count restante:[%d]\n",mpid->world_rank,mpid->processor_name,broadcast_count);
#endif /* _VERB_ */
//						SI ME CORRESPONDE ENVIAR BROADCAST Y HAY PROCESOS QUE LO ESPERAN
						if(mpid->world_rank==first_alone && broadcast_count>0){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - Enviando dimensiones de bloque faltante\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
							ret=send_broadcast(mpid,broadcast_list,broadcast_dim,broadcast_count);
							if(ret!=MPI_SUCCESS){
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en send_broadcast\n",mpid->world_rank,mpid->processor_name);
								continue;
							}
//							DESACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=0;
							already_back=1;
						}
						else if(broadcast_count>0){
//							DESACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=0;
							already_back=1;
						}
//						SI AUN NO ENCUENTRO COMPAÑERO
						else{
							ret=MPI_SUCCESS;
							already_back=1;
						}
					}
				}
				else{
//					EJECUTA UN PASO INTERMEDIO
					time_step=MPI_Wtime();
					if(broadcast==0){
//						EJECUTA DGEQRF SOBRE EL SUBBLOQUE
						qr_matrix(md_sub->A,md_sub->M,md_sub->N,md_sub->TAU,md_sub->WORK);
					}
					md_subR=first_middle_step_qr(md_sub,mpid);
					time_steps[mpid->step]+=MPI_Wtime()-time_step;
					ret=(md_subR!=NULL) ? MPI_SUCCESS : !MPI_SUCCESS;
					flag=ret;
/*
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: TSQR - md_subR\n",mpid->world_rank,mpid->processor_name);
					print_matrix(md_subR->A,md_subR->M,md_subR->N);
*/
//					SI ENVIO INFORMACION POR NO TENER COMPAÑERO EN EL PASO ANTERIOR
					if(broadcast==1){
//						ELIMINA LOS PROCESOS QUE YA TIENEN LA MISMA INFORMACION QUE EL PROCESO SIN COMPAÑERO
						for(i=0;i<mpid->world_size;i++){
							if((broadcast_list[i] && mpid->mirror[i]) || (prev_alone_list[i] && i!=mpid->world_rank)){
#ifdef _VERB_
								usleep(WAIT_TIME*mpid->world_rank);
								printf("Proceso [%d] en [%s]: TSQR - Eliminando a [%d] de broadcast_list\n",mpid->world_rank,mpid->processor_name,i);
#endif /* _VERB_ */
								broadcast_list[i]=0;
								broadcast_count--;
							}
						}
#ifdef _VERB_
						sprintf(msg,"Proceso [%d] en [%s]: TSQR - broadcast_list restante: ",mpid->world_rank,mpid->processor_name);
						for(i=0;i<mpid->world_size;i++){
							if(broadcast_list[i]){
								sprintf(temp,"[%d]",i);
								strcat(msg,temp);
							}
						}
						usleep(WAIT_TIME*mpid->world_rank);
						printf("%s\n",msg);
						printf("Proceso [%d] en [%s]: TSQR - broadcast_count restante:[%d]\n",mpid->world_rank,mpid->processor_name,broadcast_count);
#endif /* _VERB_ */
//						SI ME CORRESPONDE ENVIAR BROADCAST Y HAY PROCESOS QUE LO ESPERAN
						if(mpid->world_rank==first_alone && broadcast_count>0){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - Enviando dimensiones de bloque faltante\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
							ret=send_broadcast(mpid,broadcast_list,broadcast_dim,broadcast_count);
							if(ret!=MPI_SUCCESS){
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en send_broadcast\n",mpid->world_rank,mpid->processor_name);
								continue;
							}
//							DESACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=0;
//							already_back=1;
						}
						else if(broadcast_count>0){
//							DESACTIVA BANDERA DE TRANSMISION AL SIGUIENTE PASO
							broadcast=0;
//							already_back=1;
						}
//						SI AUN NO ENCUENTRO COMPAÑERO
						else{
							ret=MPI_SUCCESS;
//							already_back=1;
						}
					}
					ret=flag;
				}
			}
			mpid->step_finished=flag=(ret==MPI_SUCCESS);
#ifdef _VERB_
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: TSQR - Ejecutado paso [%d] - ret:[%d]\n",mpid->world_rank,mpid->processor_name,mpid->step,ret);
#endif /* _VERB_ */
#ifndef _OMPI_
			MPIX_Comm_agree(mpid->world,&flag);
#else
			if(!flag){
				fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
				return(ret);
			}
			MPI_Barrier(mpid->world);
#endif /* _OMPI_ */
//			SI NO OCURRIO ERROR, TERMINA EL PASO DEL ALGORITMO
			if(flag){
				if(index!=indexD && !already_back){
					backup_results_qr(mpid,md,md_sub,index,md_subR,indexD,(mpid->world_rank<=mpid->dest[mpid->world_rank]) ? 'A' : 'B',0,0);
					concatenate_matrices_qr(md_sub,md_subR,mpid->step,(mpid->world_rank<=mpid->dest[mpid->world_rank]) ? 'A' : 'B');
//					LIBERA RECURSOS
					matrix_data_free(md_subR);
					already_back=0;
				}
				else if(!already_back){
					backup_results_alone(mpid,md,md_sub,index);
					already_back=0;
				}
				break;
			}
#ifndef _OMPI_
			time_rest=MPI_Wtime();
			fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
			while(1){
//				CADA PROCESO OBTIENE UNA LISTA DE PROCESOS FALLIDOS
				dead_rank_list=dead_list(mpid,&dead_list_size);
//				CADA PROCESO OBTIENE UNA LISTA DE PROCESOS SOBREVIVIENTES
				surv_rank_list=survivor_list(mpid,dead_rank_list,dead_list_size,&surv_list_size);
#ifdef _VERB_
				sprintf(msg,"Proceso [%d] en [%s]: TSQR - Procesos fallidos: ",mpid->world_rank,mpid->processor_name);
				for(i=0;i<dead_list_size;i++){
					sprintf(temp,"[%d]",dead_rank_list[i]);
					strcat(msg,temp);
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
				sprintf(msg,"Proceso [%d] en [%s]: TSQR - Procesos sobrevivientes: ",mpid->world_rank,mpid->processor_name);
				for(i=0;i<surv_list_size;i++){
					sprintf(temp,"[%d]",surv_rank_list[i]);
					strcat(msg,temp);
				}
				usleep(WAIT_TIME*mpid->world_rank);
				printf("%s\n",msg);
#endif /* _VERB_ */
//				RESTAURA A LOS PROCESOS FALLIDOS
				ret=replace_partners(mpid,dead_rank_list,dead_list_size);
//				SI LA RESTAURACION FALLO, INTENTA DE NUEVO
				if(ret!=MPI_SUCCESS){
					free(dead_rank_list);
					free(surv_rank_list);
					MPI_Error_string(ret,err,&err_len);
					fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al restaurar procesos [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
					continue;
				}
				break;
			}
//			SE BUSCA LA POSICION EN LA LISTA DE SOBREVIVIENTES
			indexS=0;
			while(mpid->world_rank!=surv_rank_list[indexS]){
				indexS++;
			}
//			SI EL ERROR OCURRIO EN EL PRIMER PASO
			if(mpid->step==0){
//				SI ME CORRESPONDE ENVIAR DATOS A UN PROCESO FALLIDO
				if(indexS<dead_list_size){
					for(i=indexS;i<dead_list_size;i+=surv_list_size){
#ifdef _VERB_
						usleep(WAIT_TIME*mpid->world_rank);
						printf("Proceso [%d] en [%s]: TSQR - Enviando paso inicial a [%d]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
#endif /* _VERB_ */
//						INTENTA ENVIAR EL PASO DE INICIO
						ret=safe_send_times((void *)(&(mpid->step)),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//						SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
						if(ret!=MPI_SUCCESS){
							MPI_Error_string(ret,err,&err_len);
							fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al distribuir paso inicial a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
							continue;
						}
						printf("Proceso [%d] en [%s]: TSQR - Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
					}
				}
			}
//			SI EL ERROR OCURRIO EN UN PASO INTERMEDIO
			else{
				if(mpid->step_finished){
					concat_sizes[MR_POS]=MR_prev;
					concat_sizes[NR_POS]=NR_prev;
					concatR_ptr=concatR_prev;
				}
				else{
					concat_sizes[MR_POS]=MR;
					concat_sizes[NR_POS]=NR;
					concatR_ptr=concatR;
				}
//				BUSCA SI ALGUN PROCESO FALLIDO ES AQUEL CON EL QUE HE COMPARTIDO DATOS
				for(i=0;i<dead_list_size;i++){
//					for(j=0;j<mpid->step;j++){
//						SI HE COMPARTIDO DATOS CON EL PROCESO FALLIDO, LE ENVIO MIS DATOS ACTUALES
//						if(dead_rank_list[i]==mpid->mirror[j]){
						if(mpid->mirror[dead_rank_list[i]]){
#ifdef _VERB_
							usleep(WAIT_TIME*mpid->world_rank);
							printf("Proceso [%d] en [%s]: TSQR - Enviando datos a [%d]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
#endif /* _VERB_ */
//							INTENTA ENVIAR EL PASO DE INICIO
							ret=safe_send_times((void *)(&(mpid->step)),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al distribuir paso inicial a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								continue;
							}
//							INTENTA ENVIAR LA MATRIZ A EN EL PASO ACTUAL
							ret=safe_send_times((void *)md->A,md->M*md->N,MPI_DOUBLE,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al distribuir matriz A a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								break;
							}
//							INTENTA ENVIAR LAS DIMENSIONES DE LAS CONCATENACIONES EN EL PASO ACTUAL
							ret=safe_send_times((void *)concat_sizes,MAX_CONCAT_SIZES,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al distribuir dimensiones de concatenaciones a [%d] [%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								break;
							}
//							INTENTA ENVIAR LA CONCATENACION DE MATRICES R EN EL PASO ACTUAL
							ret=safe_send_times((void *)concatR_ptr,concat_sizes[MR_POS]*concat_sizes[NR_POS],MPI_DOUBLE,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al distribuir concatenacion R a [%d] [%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								break;
							}
//							INTENTA ENVIAR TAMAÑO DE LISTA DE HOUSEHOLDER VECTORS
							ret=safe_send_times((void *)(&Hv_list_size),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al distribuir numero de Sizes/Hv/TAU a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								continue;
							}
//							INTENTA ENVIAR LISTA DE DIMENSIONES, HOUSEHOLDER VECTORS Y VALORES TAU
							for(k=0;k<Hv_list_size;k++){
								avail=(Hv_sizes[k]==NULL) ? 0 : 1;
								ret=safe_send_times((void *)(&avail),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
								if(!avail)
									continue;
								ret=safe_send_times((void *)Hv_sizes[k],3,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//								ret=safe_send_times((void *)Hv_list[k],Hv_sizes[k][0]*Hv_sizes[k][1],MPI_DOUBLE,dead_rank_list[i],mpid->world,MAX_TRY);
//								ret=safe_send_times((void *)TAU_list[k],min(Hv_sizes[i][0],Hv_sizes[i][1]),MPI_DOUBLE,dead_rank_list[i],mpid->world,MAX_TRY);
							}
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al distribuir Sizes/Hv/TAU a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								continue;
							}
							printf("Proceso [%d] en [%s]: TSQR - Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
							break;
						}
//					}
				}
			}
			free(surv_rank_list);
			free(dead_rank_list);
			time_rest_final+=MPI_Wtime()-time_rest;
#endif /* _OMPI_ */
		}
//		ACTUALIZA INDICES DE TRABAJO DE TODOS LOS PROCESOS
		for(i=0;i<mpid->world_size;i++)
			working_block[i]=min(working_block[i],working_block[mpid->dest[i]]);
//		ACTUALIZA DESTINOS DE TODOS LOS PROCESOS
		add_mirror(mpid,mpid->dest[mpid->world_rank]);
		update_dest(mpid);
		mpid->step_finished=0;
#ifndef _OMPI_
		MR_prev=MR;
		NR_prev=NR;
		if(concatR_prev!=NULL)
			free(concatR_prev);
		concatR_prev=(double *)calloc(MR_prev*NR_prev,sizeof(double));
		time_copy=MPI_Wtime();
		memcpy(concatR_prev,concatR,MR_prev*NR_prev*sizeof(double));
		time_copy_final+=MPI_Wtime()-time_copy;
#endif /* _OMPI_ */
	}
#ifdef _VERB_
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: TSQR - Ejecutando ultimo paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
#endif /* _VERB_ */
//	INICIALIZA EL SUBBLOQUE A PROCESAR LOCALMENTE
	md_sub=matrix_data_init_with_matrix(MR,NR,0,0,concatR,0,'R');
	time_step=MPI_Wtime();
	ret=last_step_qr(md_sub);
	time_steps[mpid->step]+=MPI_Wtime()-time_step;
//	ASIGNA LOS BLOQUES FINALES
	set_R(md_sub);
	set_sub_block(md->R,md->M,0,md_sub->R,md_sub->M,md_sub->N);
	ret=!MPI_SUCCESS;
//	RESPALDA R/HOUSEHOLDER/TAU EN ARCHIVO LOCAL
	while(ret!=MPI_SUCCESS){
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: TSQR - Respaldando ultimo resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,mpid->step);
#endif /* _VERB_ */
		ret=write_R_Hv_TAU(md_sub->A,md_sub->TAU,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,mpid->step);
	}
	int Hv_index=get_Hv_index(mpid->step,mpid->world_rank,mpid->dest[mpid->world_rank]);
	Hv_sizes[Hv_index]=(int *)calloc(3,sizeof(int));
//	Hv_list[Hv_index]=(double *)calloc(max(md_sub->M,md_sub->N)*min(md_sub->M,md_sub->N),sizeof(double));
//	TAU_list[Hv_index]=(double *)calloc(min(md_sub->M,md_sub->N),sizeof(double));
//	memcpy(TAU_list[Hv_index],md_sub->TAU,min(md_sub->M,md_sub->N)*sizeof(double));
//	set_Hv_in_matrix(md_sub,Hv_list[Hv_index]);
	Hv_sizes[Hv_index][0]=max(md_sub->M,md_sub->N);
	Hv_sizes[Hv_index][1]=min(md_sub->M,md_sub->N);
	Hv_sizes[Hv_index][2]=mpid->step;
	if(q_calc){
		solve_matrix(md->A_init,md->R,md->M,md->N);
//		RESTABLECE EL BLOQUE A PARTIR DE Q Y R
//		set_A(md,1);
	}
//	else{
//		RESTABLECE EL BLOQUE A PARTIR DE Q Y R
//		set_A(md,0);
//	}
/*
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: TSQR - last md->A [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
	print_matrix(md->A,md->M,md->N);
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: TSQR - last md->A_init [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
	print_matrix(md->A_init,md->M,md->N);
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: TSQR - last md->R [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
	print_matrix(md->R,md->M,md->N);
*/
	time_exec_final+=MPI_Wtime()-time_exec;
//	LIBERA RECURSOS
	matrix_data_free(md_sub);
	if(concatR!=NULL)
		free(concatR);
#ifndef _OMPI_
	if(concatR_prev!=NULL)
		free(concatR_prev);
#endif /* _OMPI_ */
	return(ret);
}

int restore_data_qr(fttsqr_data *ftd){
	MPI_data *mpid=ftd->mpid;
	matrix_data *md=ftd->md;
	char err[MPI_MAX_ERROR_STRING];
	int err_len;
	int ret;
	int avail;
	int i;
//	RECIBO PASO DE INICIO
	ret=safe_receive_times((void *)(&(mpid->step)),1,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//	SI OCURRIO ALGUN ERROR EN LA RECEPCION
	if(ret!=MPI_SUCCESS){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al recibir el paso inicial [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		return(ret);
	}
	if(mpid->step>0){
//		RECIBO ESTADO ACTUAL DE LA MATRIZ A
		ret=safe_receive_times((void *)md->A,md->M*md->N,MPI_DOUBLE,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al recibir matriz A [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		set_R(md);
//		RECIBO DIMENSIONES DE LAS CONCATENACIONES
		ret=safe_receive_times((void *)concat_sizes,MAX_CONCAT_SIZES,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al recibir dimensiones de concatenaciones [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		MR=concat_sizes[MR_POS];
		NR=concat_sizes[NR_POS];
		concatR=(double *)calloc(MR*NR,sizeof(double));
//		RECIBO ESTADO ACTUAL DE LA CONCATENACION R
		ret=safe_receive_times((void *)concatR,MR*NR,MPI_DOUBLE,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al recibir concatenacion R [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			free(concatR);
			return(ret);
		}
//		RECIBE TAMAÑO DE LISTA DE HOUSEHOLDER VECTORS
		ret=safe_receive_times((void *)(&Hv_list_size),1,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al recibir numero de Sizes/Hv/TAU a [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			free(concatR);
			return(ret);
		}
//		RECIBE LISTA DE DIMENSIONES, HOUSEHOLDER VECTORS Y VALORES TAU
		for(i=0;i<Hv_list_size;i++){
			avail=(Hv_sizes[i]==NULL) ? 0 : 1;
			ret=safe_receive_times((void *)(&avail),1,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
			if(!avail)
				continue;
			ret=safe_receive_times((void *)Hv_sizes[i],3,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//			ret=safe_receive_times((void *)Hv_list[i],Hv_sizes[i][0]*Hv_sizes[i][1],MPI_DOUBLE,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//			ret=safe_receive_times((void *)TAU_list[i],min(Hv_sizes[i][0],Hv_sizes[i][1]),MPI_DOUBLE,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
		}
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al recibir Sizes/Hv/TAU a [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			free(concatR);
			return(ret);
		}
	}
	return(MPI_SUCCESS);
}

int send_broadcast(MPI_data *mpid,int *broadcast_list,int *broadcast_dim,int broadcast_count){
	MPI_Status stats[broadcast_count];
	MPI_Request reqs[broadcast_count];
	int finished[mpid->world_size];
	broadcast_dim[0]=Hv_sizes[broadcast_index][0];
	broadcast_dim[1]=Hv_sizes[broadcast_index][1];
	broadcast_dim[2]=Hv_sizes[broadcast_index][2];
//	broadcast_dim[0]=MR;
//	broadcast_dim[1]=NR;
//	matrix_data *md_subBcast=matrix_data_init(broadcast_dim[0],broadcast_dim[1],0,0,0);
//	memcpy(md_subBcast->R,concatR,md_subBcast->M*md_subBcast->N*sizeof(double));
//	memcpy(md_subBcast->Hv,Hv_list[broadcast_index],Hv_sizes[broadcast_index][0]*Hv_sizes[broadcast_index][1]*sizeof(double));
//	memcpy(md_subBcast->TAU,TAU_list[broadcast_index],Hv_sizes[broadcast_index][1]*sizeof(double));
	int count=0;
	int ret,i;
//	ENVIA LAS DIMENSIONES DE LA MATRIZ FALTANTE
	for(i=0;i<mpid->world_size;i++){
		if(broadcast_list[i]){
			ret=MPI_Isend((void *)broadcast_dim,3,MPI_INT,i,i,mpid->world,&reqs[count]);
			count++;
			if(ret!=MPI_SUCCESS){
				fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al enviar dimensiones de broadcast a proceso[%d]\n",mpid->world_rank,mpid->processor_name,i);
				continue;
			}
//			AGREGA EL PROCESO A SU LISTA DE MIRRORS
			add_mirror(mpid,i);
#ifdef _VERB_
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: TSQR - Broadcast enviado a [%d]\n",mpid->world_rank,mpid->processor_name,i);
#endif /* _VERB_ */
		}
	}
	ret=MPI_Waitsome(broadcast_count,reqs,&count,finished,stats);
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en espera de envio de broadcast\n",mpid->world_rank,mpid->processor_name);
		return(ret);
	}
/*	for(i=0;i<mpid->world_size;i++){
		if(broadcast_list[i]){
//			ENVIA LA MATRIZ FALTANTE
			ret=safe_send_times_with_tag((void *)md_subBcast->R,md_subBcast->M*md_subBcast->N,MPI_DOUBLE,i,i,mpid->world,MAX_TRY);
			if(ret!=MPI_SUCCESS){
				fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al enviar broadcast de matrix R a[%d]\n",mpid->world_rank,mpid->processor_name,i);
				continue;
			}
//			ENVIA LOS HOULSEHOLDER VECTORS FALTANTES
			ret=safe_send_times_with_tag((void *)md_subBcast->Hv,md_subBcast->M*md_subBcast->N,MPI_DOUBLE,i,i,mpid->world,MAX_TRY);
			if(ret!=MPI_SUCCESS){
				fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al enviar broadcast de householder a proceso[%d]\n",mpid->world_rank,mpid->processor_name,i);
				continue;
			}
//			ENVIA LOS VALORES TAU FALTANTES
			ret=safe_send_times_with_tag((void *)md_subBcast->TAU,min(md_subBcast->M,md_subBcast->N),MPI_DOUBLE,i,i,mpid->world,MAX_TRY);
			if(ret!=MPI_SUCCESS){
				fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al enviar broadcast de valores TAU a proceso[%d]\n",mpid->world_rank,mpid->processor_name,i);
				continue;
			}
//			AGREGA EL PROCESO A SU LISTA DE MIRRORS
			add_mirror(mpid,i);
#ifdef _VERB_
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: TSQR - Broadcast enviado a [%d]\n",mpid->world_rank,mpid->processor_name,i);*/
//#endif /* _VERB_ */
//		}
//	}
//	LIBERA RECURSOS
//	matrix_data_free(md_subBcast);	
	return(ret);
}

matrix_data *receive_broadcast(MPI_data *mpid,int *broadcast_dim){
	MPI_Request request;
//	RECIBE LAS DIMENSIONES DE LA MATRIZ FALTANTE
	int ret=MPI_Irecv((void *)broadcast_dim,3,MPI_INT,MPI_ANY_SOURCE,mpid->world_rank,mpid->world,&request);
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en recepcion de dimensiones de broadcast\n",mpid->world_rank,mpid->processor_name);
		return(NULL);
	}
	ret=MPI_Wait(&request,&(mpid->status));
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en espera de recepcion de broadcast\n",mpid->world_rank,mpid->processor_name);
		return(NULL);
	}
#ifdef _VERB_
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: TSQR - broadcast_dim:[%dx%d][%d] - mpid->status.MPI_SOURCE:[%d]\n",mpid->world_rank,mpid->processor_name,broadcast_dim[0],broadcast_dim[1],broadcast_dim[2],mpid->status.MPI_SOURCE);
#endif /* _VERB_ */
//	INICIALIZA EN CEROS UN SUBBLOQUE A RECUPERAR
	matrix_data *md_subR=matrix_data_init(broadcast_dim[0],broadcast_dim[1],0,0,0,'R');
	ret=!MPI_SUCCESS;
	while(ret!=MPI_SUCCESS){
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: TSQR - Recuperando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,md_subR->M,md_subR->N,mpid->world_size,mpid->status.MPI_SOURCE,broadcast_dim[2]);
#endif /* _VERB_ */
		ret=read_R_Hv_TAU(md_subR->A,md_subR->TAU,md_subR->M,md_subR->N,mpid->world_size,mpid->status.MPI_SOURCE,broadcast_dim[2]);
	}
//	RECIBE LA MATRIZ R FALTANTE
/*	ret=safe_receive_times_with_tag((void *)md_subR->R,md_subR->M*md_subR->N,MPI_DOUBLE,mpid->status.MPI_SOURCE,mpid->world_rank,mpid->world,mpid->status,MAX_TRY);
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en recepcion de R\n",mpid->world_rank,mpid->processor_name);
		return(NULL);
	}
//	RECIBE LOS HOULSEHOLDER VECTORS FALTANTES
	ret=safe_receive_times_with_tag((void *)md_subR->Hv,md_subR->M*md_subR->N,MPI_DOUBLE,mpid->status.MPI_SOURCE,mpid->world_rank,mpid->world,mpid->status,MAX_TRY);
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en recepcion de Hv\n",mpid->world_rank,mpid->processor_name);
		return(NULL);
	}
//	RECIBE LOS VALORES TAU FALTANTES
	ret=safe_receive_times_with_tag((void *)md_subR->TAU,min(md_subR->M,md_subR->N),MPI_DOUBLE,mpid->status.MPI_SOURCE,mpid->world_rank,mpid->world,mpid->status,MAX_TRY);
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error en recepcion de TAU\n",mpid->world_rank,mpid->processor_name);
		return(NULL);
	}*/
	return(md_subR);
}

matrix_data *first_middle_step_qr(matrix_data *md_sub,MPI_data *mpid){
//	ENVIA SUBBLOQUE RESULTANTE Y RECIBE SUBBLOQUE CALCULADO POR OTRO PROCESO
	matrix_data *md_temp=send_receive_matrix_data(mpid,md_sub);
	if(md_temp==NULL){
		fprintf(stderr,"Proceso [%d] en [%s]: TSQR - Error al intercambiar subbloques en paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
		return(NULL);
	}
	return(md_temp);
}

int last_step_qr(matrix_data *md_sub){
//	EJECUTA DGEQRF SOBRE EL SUBBLOQUE
	qr_matrix(md_sub->A,md_sub->M,md_sub->N,md_sub->TAU,md_sub->WORK);
	return(MPI_SUCCESS);
}

int get_Hv_index(int step,int world_rank,int world_rankR){
	return((step==0) ? world_rank%2 : (step==step_lim-1) ? Hv_list_size-1 : (world_rank<world_rankR) ? 2*step : 2*step+1);
}

int write_R_Hv_TAU(double *A,double *TAU,int M,int N,int size,int rank,int step){
	char filename[MAX_MSG_SIZE];
	sprintf(filename,"[%s][%dx%d][%d]_Process[%d]_Step[%d]_Backup.dump","FT-TSQR_Hv_TAU",M,N,size,rank,step);
	FILE *out=fopen(filename,"w");
	if(out==NULL){
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
		return(EXIT_FAILURE);
	}
	else{
		time_write=MPI_Wtime();
		fwrite(A,sizeof(double),M*N,out);
		fwrite(TAU,sizeof(double),min(M,N),out);
		time_write_final+=MPI_Wtime()-time_write;
		fclose(out);
	}
	return(MPI_SUCCESS);
}

int read_R_Hv_TAU(double *A,double *TAU,int M,int N,int size,int rank,int step){
	char filename[MAX_MSG_SIZE];
	sprintf(filename,"[%s][%dx%d][%d]_Process[%d]_Step[%d]_Backup.dump","FT-TSQR_Hv_TAU",M,N,size,rank,step);
	FILE *in=fopen(filename,"r");
	if(in==NULL){
		fprintf(stderr,"Error al leer el archivo [%s]\n",filename);
		return(EXIT_FAILURE);
	}
	else{
		time_read=MPI_Wtime();
		fread(A,sizeof(double),M*N,in);
		fread(TAU,sizeof(double),min(M,N),in);
		time_read_final+=MPI_Wtime()-time_read;
		fclose(in);
	}
	return(MPI_SUCCESS);
}

void backup_results_qr(MPI_data *mpid,matrix_data *md,matrix_data *md_sub,int index,matrix_data *md_subR,int indexD,char up,int change_dest,int new_dest){
//	RESPALDA LOS RESULTADOS DE A
	set_sub_block(md->A,md->M,index,md_sub->A,md_sub->M,md_sub->N);
	if(index!=indexD)
		set_sub_block(md->A,md->M,indexD,md_subR->A,md_subR->M,md_subR->N);
//	RESPALDA EL RESULTADO DE R DEL PROCESO CON MENOR RANK
	set_R(md_sub);
	if(!change_dest)
		set_R(md_subR);
	if(up=='A')
		set_sub_block(md->R,max(md->M,md->N),0,md_sub->R,max(md_sub->M,md_sub->N),min(md_sub->M,md_sub->N));
	else
		set_sub_block(md->R,max(md->M,md->N),0,md_subR->R,max(md_subR->M,md_subR->N),min(md_subR->M,md_subR->N));
	int Hv_index=get_Hv_index(mpid->step,mpid->world_rank,(change_dest) ? new_dest : mpid->dest[mpid->world_rank]);
	int Hv_indexR=get_Hv_index(mpid->step,(change_dest) ? new_dest : mpid->dest[mpid->world_rank],mpid->world_rank);
//	TAU_list[Hv_index]=(double *)calloc(min(md_sub->M,md_sub->N),sizeof(double));
//	Hv_list[Hv_index]=(double *)calloc(max(md_sub->M,md_sub->N)*min(md_sub->M,md_sub->N),sizeof(double));
	Hv_sizes[Hv_index]=(int *)calloc(3,sizeof(int));
//	memcpy(TAU_list[Hv_index],md_sub->TAU,min(md_sub->M,md_sub->N)*sizeof(double));
//	set_Hv_in_matrix(md_sub,Hv_list[Hv_index]);
	Hv_sizes[Hv_index][0]=max(md_sub->M,md_sub->N);
	Hv_sizes[Hv_index][1]=min(md_sub->M,md_sub->N);
	Hv_sizes[Hv_index][2]=mpid->step;
//	RESPALDA R/HOUSEHOLDER/TAU EN ARCHIVO LOCAL
	int ret=!MPI_SUCCESS;
	while(ret!=MPI_SUCCESS){
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: TSQR - Respaldando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,mpid->step);
#endif /* _VERB_ */
		ret=write_R_Hv_TAU(md_sub->A,md_sub->TAU,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,mpid->step);
	}
	if(Hv_index!=Hv_indexR){
//		TAU_list[Hv_indexR]=(double *)calloc(min(md_subR->M,md_subR->N),sizeof(double));
//		Hv_list[Hv_indexR]=(double *)calloc(max(md_subR->M,md_subR->N)*min(md_subR->M,md_subR->N),sizeof(double));
		Hv_sizes[Hv_indexR]=(int *)calloc(3,sizeof(int));
//		memcpy(TAU_list[Hv_indexR],md_subR->TAU,min(md_subR->M,md_subR->N)*sizeof(double));
//		if(!change_dest)
//			set_Hv_in_matrix(md_subR,Hv_list[Hv_indexR]);
//		else
//			memcpy(Hv_list[Hv_indexR],md_subR->Hv,max(md_subR->M,md_subR->N)*min(md_subR->M,md_subR->N)*sizeof(double));
		Hv_sizes[Hv_indexR][0]=max(md_subR->M,md_subR->N);
		Hv_sizes[Hv_indexR][1]=min(md_subR->M,md_subR->N);
		Hv_sizes[Hv_indexR][2]=mpid->step;
//		RESPALDA R/HOUSEHOLDER/TAU EN ARCHIVO LOCAL
		ret=!MPI_SUCCESS;
		while(ret!=MPI_SUCCESS){
#ifdef _VERB_
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: TSQR - Respaldando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,md_subR->M,md_subR->N,mpid->world_size,mpid->dest[mpid->world_rank],mpid->step);
#endif /* _VERB_ */
			ret=write_R_Hv_TAU(md_subR->A,md_subR->TAU,md_subR->M,md_subR->N,mpid->world_size,mpid->dest[mpid->world_rank],mpid->step);
		}
	}
}

void backup_results_alone(MPI_data *mpid,matrix_data *md,matrix_data *md_sub,int index){
//	RESPALDA LOS RESULTADOS DE A
	set_sub_block(md->A,md->M,index,md_sub->A,md_sub->M,md_sub->N);
//	RESPALDA EL RESULTADO DE R
	set_R(md_sub);
	set_sub_block(md->R,max(md->M,md->N),0,md_sub->R,max(md_sub->M,md_sub->N),min(md_sub->M,md_sub->N));
//	OBTIENE LA POSICION PARA RESPALDAR LOS RESULTADOS TAU/HV
	int Hv_index=get_Hv_index(mpid->step,mpid->world_rank,mpid->dest[mpid->world_rank]);
//	TAU_list[Hv_index]=(double *)calloc(min(md_sub->M,md_sub->N),sizeof(double));
//	Hv_list[Hv_index]=(double *)calloc(max(md_sub->M,md_sub->N)*min(md_sub->M,md_sub->N),sizeof(double));
	Hv_sizes[Hv_index]=(int *)calloc(3,sizeof(int));
//	RESPALDA EL RESULTADO DE TAU
//	memcpy(TAU_list[Hv_index],md_sub->TAU,min(md_sub->M,md_sub->N)*sizeof(double));
//	RESPALDA EL RESULTADO DE HV
//	set_Hv_in_matrix(md_sub,Hv_list[Hv_index]);
//	ACTUALIZA DIMENSIONES
	Hv_sizes[Hv_index][0]=max(md_sub->M,md_sub->N);
	Hv_sizes[Hv_index][1]=min(md_sub->M,md_sub->N);
	Hv_sizes[Hv_index][2]=mpid->step;
//	RESPALDA R/HOUSEHOLDER/TAU EN ARCHIVO LOCAL
	int ret=!MPI_SUCCESS;
	while(ret!=MPI_SUCCESS){
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: TSQR - Respaldando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,mpid->step);
#endif /* _VERB_ */
		ret=write_R_Hv_TAU(md_sub->A,md_sub->TAU,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,mpid->step);
	}
	MR=max(md_sub->M,md_sub->N);
	NR=min(md_sub->M,md_sub->N);
	if(concatR!=NULL)
		free(concatR);
	concatR=(double *)calloc(MR*NR,sizeof(double));
//	RESPALDA EL RESULTADO DE R EN LA CONCATENACION
	memcpy(concatR,md_sub->R,MR*NR*sizeof(double));
//	ALMACENA LA POSICION DE RESPALDOS
	broadcast_index=Hv_index;
}

void concatenate_matrices_qr(matrix_data *md_sub,matrix_data *md_subR,int step,char up){
	if(concatR!=NULL)
		free(concatR);
	if(step==0)
		concatR=concat_R_blocks_first_step(md_sub->R,max(md_sub->M,md_sub->N),min(md_sub->M,md_sub->N),md_subR->R,max(md_subR->M,md_subR->N),min(md_subR->M,md_subR->N),up);
	else
		concatR=concat_R_blocks(md_sub->R,max(md_sub->M,md_sub->N),min(md_sub->M,md_sub->N),md_subR->R,max(md_subR->M,md_subR->N),min(md_subR->M,md_subR->N),up);
	MR=max(md_sub->M,md_sub->N)+max(md_subR->M,md_subR->N);
	NR=min(md_sub->M,md_sub->N);
}

void free_fttsqr_data(fttsqr_data *ftd){
	if(ftd->md!=NULL)
		matrix_data_free(ftd->md);
	if(ftd->mpid!=NULL)
		MPI_data_free(ftd->mpid);
	if(ftd!=NULL)
		free(ftd);
}

void init_variables_qr(int q_c,int world_size){
	q_calc=q_c;
	step_lim=((int)ceil(log2(world_size)))+1;
}

void reset_lists(){
	int i;
	if(Hv_sizes!=NULL){
//	if(Hv_sizes!=NULL && Hv_list!=NULL && TAU_list!=NULL){
		for(i=0;i<Hv_list_size;i++){
			if(Hv_sizes!=NULL)
				free(Hv_sizes[i]);
//			if(Hv_list!=NULL)
//				free(Hv_list[i]);
//			if(TAU_list!=NULL)
//				free(TAU_list[i]);
		}
		free(Hv_sizes);
//		free(Hv_list);
//		free(TAU_list);
	}
}

