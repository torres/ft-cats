#include "mpi_data.h"

MPI_data *MPI_data_init(int *argc,char **argv[]){
	int i;
	MPI_data *mpid=(MPI_data *)calloc(1,sizeof(MPI_data));
	mpid->argc=*argc;
	char **tmp_argv=*argv;
	mpid->argv=(char **)calloc(mpid->argc+1,sizeof(char *));
	for(i=0;i<mpid->argc;i++){
		mpid->argv[i]=(char *)calloc(strlen(tmp_argv[i]),sizeof(char));
		strcpy(mpid->argv[i],tmp_argv[i]);
	}
	mpid->argv[mpid->argc]=NULL;
	MPI_Init(&mpid->argc,&mpid->argv);
	MPI_Get_processor_name(mpid->processor_name,&(mpid->processor_name_len));
	MPI_Comm_set_errhandler(MPI_COMM_WORLD,MPI_ERRORS_RETURN);
	MPI_Comm_get_parent(&(mpid->world));
	mpid->spawned=(mpid->world!=MPI_COMM_NULL);
	if(!mpid->spawned){
		MPI_Comm_dup(MPI_COMM_WORLD,&(mpid->world));
		MPI_Comm_rank(mpid->world,&(mpid->world_rank));
		MPI_Comm_size(mpid->world,&(mpid->world_size));
		mpid->step=0;
	}
#ifndef _OMPI_
	else{
		time_rest=MPI_Wtime();
		i=replace_partners(mpid,NULL,-1);
		time_rest_final+=MPI_Wtime()-time_rest;
		if(i!=MPI_SUCCESS){
			if(mpid->world!=MPI_COMM_NULL)
				MPI_Comm_free(&(mpid->world));
			free(mpid);
			return(NULL);
		}
	}
#endif /* _OMPI_ */
	mpid->step_finished=0;
	mpid->dest=(int *)calloc(mpid->world_size,sizeof(int));
	mpid->mirror=(int *)calloc(mpid->world_size,sizeof(int));
	for(i=0;i<mpid->world_size;i++){
		mpid->dest[i]=i+((((i>>mpid->step)&1)==0) ? (1<<mpid->step) : -(1<<mpid->step));
		mpid->dest[i]=(mpid->dest[i]>mpid->world_size-1) ? i : mpid->dest[i];
	}
//	memset(mpid->mirror,-1,mpid->world_size*sizeof(int));
	return(mpid);
}

MPI_data *MPI_data_init_with_comm(MPI_Comm comm,int *argc,char **argv[]){
	int i;
	MPI_data *mpid=(MPI_data *)calloc(1,sizeof(MPI_data));
	mpid->argc=*argc;
	char **tmp_argv=*argv;
	mpid->argv=(char **)calloc(mpid->argc+1,sizeof(char *));
	for(i=0;i<mpid->argc;i++){
		mpid->argv[i]=(char *)calloc(strlen(tmp_argv[i]),sizeof(char));
		strcpy(mpid->argv[i],tmp_argv[i]);
	}
	mpid->argv[mpid->argc]=NULL;
	MPI_Get_processor_name(mpid->processor_name,&(mpid->processor_name_len));
	MPI_Comm_get_parent(&(mpid->world));
	mpid->spawned=(mpid->world!=MPI_COMM_NULL);
	if(!mpid->spawned){
		MPI_Comm_dup(comm,&(mpid->world));
//		mpid->world=*comm;
		MPI_Comm_rank(mpid->world,&(mpid->world_rank));
		MPI_Comm_size(mpid->world,&(mpid->world_size));
		mpid->step=0;
	}
#ifndef _OMPI_
	else{
		time_rest=MPI_Wtime();
		i=replace_partners(mpid,NULL,-1);
		time_rest_final+=MPI_Wtime()-time_rest;
		if(i!=MPI_SUCCESS){
			if(mpid->world!=MPI_COMM_NULL)
				MPI_Comm_free(&(mpid->world));
			free(mpid);
			return(NULL);
		}
	}
#endif /* _OMPI_ */
	mpid->step_finished=0;
	mpid->dest=(int *)calloc(mpid->world_size,sizeof(int));
	mpid->mirror=(int *)calloc(mpid->world_size,sizeof(int));
	for(i=0;i<mpid->world_size;i++){
		mpid->dest[i]=i+((((i>>mpid->step)&1)==0) ? (1<<mpid->step) : -(1<<mpid->step));
		mpid->dest[i]=(mpid->dest[i]>mpid->world_size-1) ? i : mpid->dest[i];
	}
//	memset(mpid->mirror,-1,mpid->world_size*sizeof(int));
	return(mpid);
}

void MPI_data_reset(MPI_data *mpid){
	int i;
	mpid->step=0;
	mpid->step_finished=0;
	for(i=0;i<mpid->world_size;i++){
		mpid->dest[i]=i+((((i>>mpid->step)&1)==0) ? (1<<mpid->step) : -(1<<mpid->step));
		mpid->dest[i]=(mpid->dest[i]>mpid->world_size-1) ? i : mpid->dest[i];
	}
	memset(mpid->mirror,0,mpid->world_size*sizeof(int));
}

void MPI_data_free(MPI_data *mpid){
	int i;
	if(mpid->mirror!=NULL)
		free(mpid->mirror);
	if(mpid->dest!=NULL)
		free(mpid->dest);
	for(i=0;i<mpid->argc;i++)
		if(mpid->argv[i]!=NULL)
			free(mpid->argv[i]);
	if(mpid->argv!=NULL)
		free(mpid->argv);
	if(mpid->world!=MPI_COMM_NULL)
		MPI_Comm_free(&(mpid->world));
	if(mpid!=NULL)
		free(mpid);
}

void add_mirror(MPI_data *mpid,int mirror){
	mpid->mirror[mirror]=(mirror==mpid->world_rank) ? 0 : 1;
}

void update_dest(MPI_data *mpid){
	int i;
	mpid->step++;
	for(i=0;i<mpid->world_size;i++){
		mpid->dest[i]=i+((((i>>mpid->step)&1)==0) ? (1<<mpid->step) : -(1<<mpid->step));
		mpid->dest[i]=(mpid->dest[i]>mpid->world_size-1) ? i : mpid->dest[i];
	}
}

int get_dest(int rank,int step,int size){
	int dest=rank+((((rank>>step)&1)==0) ? (1<<step) : -(1<<step));
	dest=(dest>size-1) ? rank : dest;
	return(dest);
}

/*int find_incomplete(MPI_data *mpid){
	int i,j,k;
	for(i=0;i<mpid->world_size;i++){
		j=mpid->dest[i];
		if(j<mpid->world_size-1)
			return(i);
		k=mpid->dest[k];
		if(i!=k)
			return(i);
	}
	return(-1);
}*/

int safe_send(void *send,int size,MPI_Datatype type,int dest,MPI_Comm world){
	int ret,ok;
	while(1){
		time_comm=MPI_Wtime();
		ret=MPI_Send(send,size,type,dest,9999,world);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret!=MPI_SUCCESS)
			return(ret);
		ok=-1;
		time_comm=MPI_Wtime();
		ret=MPI_Recv(&ok,1,MPI_INT,dest,9999,world,MPI_STATUS_IGNORE);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret==MPI_SUCCESS && ok==0)
			break;
	}
	return(MPI_SUCCESS);
}

int safe_send_times(void *send,int size,MPI_Datatype type,int dest,MPI_Comm world,int times){
	int ret=0;
	while(1){
		if(times==0)
			return(ret);
		ret=safe_send(send,size,type,dest,world);
		if(ret!=MPI_SUCCESS){
			times--;
			continue;
		}
		break;
	}
	return(MPI_SUCCESS);
}

int safe_send_with_tag(void *send,int size,MPI_Datatype type,int dest,int tag,MPI_Comm world){
	int ret,ok;
	while(1){
		time_comm=MPI_Wtime();
		ret=MPI_Send(send,size,type,dest,tag,world);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret!=MPI_SUCCESS)
			return(ret);
		ok=-1;
		time_comm=MPI_Wtime();
		ret=MPI_Recv(&ok,1,MPI_INT,dest,tag,world,MPI_STATUS_IGNORE);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret==MPI_SUCCESS && ok==0)
			break;
	}
	return(MPI_SUCCESS);
}

int safe_send_times_with_tag(void *send,int size,MPI_Datatype type,int dest,int tag,MPI_Comm world,int times){
	int ret=0;
	while(1){
		if(times==0)
			return(ret);
		ret=safe_send_with_tag(send,size,type,dest,tag,world);
		if(ret!=MPI_SUCCESS){
			times--;
			continue;
		}
		break;
	}
	return(MPI_SUCCESS);
}

int safe_receive(void *recv,int size,MPI_Datatype type,int dest,MPI_Comm world,MPI_Status status){
	int ret,ok,count;
	while(1){
		time_comm=MPI_Wtime();
		ret=MPI_Recv(recv,size,type,dest,9999,world,&status);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret!=MPI_SUCCESS)
			return(ret);
		MPI_Get_count(&status,type,&count);
		ok=(count==size) ? 0 : -1;
		while(1){
			time_comm=MPI_Wtime();
			ret=MPI_Send(&ok,1,MPI_INT,(dest==MPI_ANY_SOURCE) ? status.MPI_SOURCE : dest,9999,world);
			time_comm_final+=MPI_Wtime()-time_comm;
			if(ret==MPI_SUCCESS)
				break;
		}
		if(ok==0)
			break;
	}
	return(MPI_SUCCESS);
}

int safe_receive_times(void *recv,int size,MPI_Datatype type,int dest,MPI_Comm world,MPI_Status status,int times){
	int ret=0;
	while(1){
		if(times==0)
			return(ret);
		ret=safe_receive(recv,size,type,dest,world,status);
		if(ret!=MPI_SUCCESS){
			times--;
			continue;
		}
		break;
	}
	return(MPI_SUCCESS);
}

int safe_receive_with_tag(void *recv,int size,MPI_Datatype type,int dest,int tag,MPI_Comm world,MPI_Status status){
	int ret,ok,count;
	while(1){
		time_comm=MPI_Wtime();
		ret=MPI_Recv(recv,size,type,dest,tag,world,&status);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret!=MPI_SUCCESS)
			return(ret);
		MPI_Get_count(&status,type,&count);
		ok=(count==size) ? 0 : -1;
		while(1){
			time_comm=MPI_Wtime();
			ret=MPI_Send(&ok,1,MPI_INT,(dest==MPI_ANY_SOURCE) ? status.MPI_SOURCE : dest,tag,world);
			time_comm_final+=MPI_Wtime()-time_comm;
			if(ret==MPI_SUCCESS)
				break;
		}
		if(ok==0)
			break;
	}
	return(MPI_SUCCESS);
}

int safe_receive_times_with_tag(void *recv,int size,MPI_Datatype type,int dest,int tag,MPI_Comm world,MPI_Status status,int times){
	int ret=0;
	while(1){
		if(times==0)
			return(ret);
		ret=safe_receive_with_tag(recv,size,type,dest,tag,world,status);
		if(ret!=MPI_SUCCESS){
			times--;
			continue;
		}
		break;
	}
	return(MPI_SUCCESS);
}

int send_receive(double *send,double *recv,int M,int N,int *IPIVsend,int *IPIVrecv,MPI_data *mpid){
	char err[MPI_MAX_ERROR_STRING];
	int ret,err_len;
	if(mpid->world_rank<mpid->dest[mpid->world_rank]){
//		ENVIO Y RECEPCION DE IPIV
		ret=safe_send_times((void *)IPIVsend,min(M,N),MPI_INT,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de IPIV [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		ret=safe_receive_times((void *)IPIVrecv,min(M,N),MPI_INT,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de IPIV [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
//		ENVIO Y RECEPCION DE MATRIZ
		ret=safe_send_times((void *)send,M*N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		ret=safe_receive_times((void *)recv,M*N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
	}
	else{
//		ENVIO Y RECEPCION DE IPIV
		ret=safe_receive_times((void *)IPIVrecv,min(M,N),MPI_INT,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de IPIV [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		ret=safe_send_times((void *)IPIVsend,min(M,N),MPI_INT,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de IPIV [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
//		ENVIO Y RECEPCION DE MATRIZ
		ret=safe_receive_times((void *)recv,M*N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		ret=safe_send_times((void *)send,M*N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
	}
	return(ret);
}

int send_receive_matrix(double *send,double *recv,int M,int N,MPI_data *mpid){
	char err[MPI_MAX_ERROR_STRING];
	int ret,err_len;
	if(mpid->world_rank<mpid->dest[mpid->world_rank]){
//		ENVIO Y RECEPCION DE MATRIZ
		ret=safe_send_times((void *)send,M*N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		ret=safe_receive_times((void *)recv,M*N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
	}
	else{
//		ENVIO Y RECEPCION DE MATRIZ
		ret=safe_receive_times((void *)recv,M*N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		ret=safe_send_times((void *)send,M*N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
	}
	return(ret);
}

matrix_data *send_receive_matrix_data(MPI_data *mpid,matrix_data *send){
	matrix_data *recv;
	char err[MPI_MAX_ERROR_STRING];
	int ret,err_len;
	int dimsSend[2];
	int dimsRecv[2];
	dimsSend[0]=send->M;
	dimsSend[1]=send->N;
	if(mpid->world_rank<mpid->dest[mpid->world_rank]){
//		ENVIO Y RECEPCION DE DIMENSIONES
		ret=safe_send_times((void *)dimsSend,2,MPI_INT,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de dimensiones [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(NULL);
		}
		ret=safe_receive_times((void *)dimsRecv,2,MPI_INT,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de dimensiones [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(NULL);
		}
		if(send->R!=NULL){
//			ENVIO DE TAU
			ret=safe_send_times((void *)send->TAU,min(send->M,send->N),MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
			if(ret!=MPI_SUCCESS){
				MPI_Error_string(ret,err,&err_len);
				fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de TAU [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
				return(NULL);
			}
		}
		recv=matrix_data_init(dimsRecv[0],dimsRecv[1],0,0,0,(send->U!=NULL) ? 'U' : (send->R!=NULL) ? 'R' : 'L');
		if(send->R!=NULL){
//			RECEPCION DE TAU
			ret=safe_receive_times((void *)recv->TAU,min(recv->M,recv->N),MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
			if(ret!=MPI_SUCCESS){
				MPI_Error_string(ret,err,&err_len);
				fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de TAU [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
				return(NULL);
			}
		}
//		ENVIO Y RECEPCION DE MATRIZ
		ret=safe_send_times((void *)send->A,send->M*send->N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(NULL);
		}
		ret=safe_receive_times((void *)recv->A,recv->M*recv->N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(NULL);
		}
	}
	else{
//		ENVIO Y RECEPCION DE DIMENSIONES
		ret=safe_receive_times((void *)dimsRecv,2,MPI_INT,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de dimensiones [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(NULL);
		}
		ret=safe_send_times((void *)dimsSend,2,MPI_INT,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de dimensiones [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(NULL);
		}
		recv=matrix_data_init(dimsRecv[0],dimsRecv[1],0,0,0,(send->U!=NULL) ? 'U' : (send->R!=NULL) ? 'R' : 'L');
		if(send->R!=NULL){
//			RECEPCION DE TAU
			ret=safe_receive_times((void *)recv->TAU,min(recv->M,recv->N),MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
			if(ret!=MPI_SUCCESS){
				MPI_Error_string(ret,err,&err_len);
				fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de TAU [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
				return(NULL);
			}
//			ENVIO DE TAU
			ret=safe_send_times((void *)send->TAU,min(send->M,send->N),MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
			if(ret!=MPI_SUCCESS){
				MPI_Error_string(ret,err,&err_len);
				fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de TAU [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
				return(NULL);
			}
		}
//		ENVIO Y RECEPCION DE MATRIZ
		ret=safe_receive_times((void *)recv->A,recv->M*recv->N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(NULL);
		}
		ret=safe_send_times((void *)send->A,send->M*send->N,MPI_DOUBLE,mpid->dest[mpid->world_rank],mpid->world,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de Matriz [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(NULL);
		}
	}
	return(recv);
}

#ifndef _OMPI_
int replace_partners(MPI_data *mpid,int *dead_list,int dead_list_size){
	MPI_Comm inter_world;
	MPI_Comm shrinked_world;
	MPI_Comm merged_world;
	MPI_Comm new_world;
	MPI_Group world_group;
	MPI_Group shrinked_world_group;
	MPI_Group dead_group;
	MPI_Errhandler errh;
	int shrinked_world_rank;
	int shrinked_world_size;
	int restored=0;
	const int data_size=2;
	int old_world_data[data_size];
	char err[MPI_MAX_ERROR_STRING];
	int err_len;
	int ret,flag,rflag,i;
	if(mpid->spawned){
		MPI_Comm_get_parent(&inter_world);
		shrinked_world=MPI_COMM_WORLD;
		ret=safe_receive_times((void *)old_world_data,data_size,MPI_INT,MPI_ANY_SOURCE,inter_world,mpid->status,MAX_TRY);
		if(ret!=MPI_SUCCESS){
			MPIX_Comm_revoke(inter_world);
			MPI_Comm_free(&inter_world);
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en recepcion de rank/size [%d][%s]\n",-1,mpid->processor_name,ret,err);
			return(ret);
		}
		mpid->world_rank=old_world_data[0];
		mpid->world_size=old_world_data[1];
		mpid->world=MPI_COMM_WORLD;
	}
	else{
//		printf("Proceso [%d] en [%s]: MPIX_Comm_shrink\n",mpid->world_rank,mpid->processor_name);
		ret=MPIX_Comm_shrink(mpid->world,&shrinked_world);
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en MPIX_Comm_shrink [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		MPI_Comm_set_errhandler(shrinked_world,MPI_ERRORS_RETURN);
		MPI_Comm_rank(shrinked_world,&shrinked_world_rank);
		MPI_Comm_size(shrinked_world,&shrinked_world_size);
//		MPI_Barrier(shrinked_world);
//		printf("Proceso [%d] en [%s]: MPI_Comm_spawn\n",mpid->world_rank,mpid->processor_name);
		ret=MPI_Comm_spawn(mpid->argv[0],&(mpid->argv[1]),dead_list_size,MPI_INFO_NULL,0,shrinked_world,&inter_world,MPI_ERRCODES_IGNORE);
		flag=(ret==MPI_SUCCESS);
//		printf("Proceso [%d] en [%s]: MPIX_Comm_agree\n",mpid->world_rank,mpid->processor_name);
		MPIX_Comm_agree(shrinked_world,&flag);
		MPI_Comm_set_errhandler(inter_world,MPI_ERRORS_RETURN);
		if(!flag){
			if(ret==MPI_SUCCESS){
				MPIX_Comm_revoke(inter_world);
				MPI_Comm_free(&inter_world);
			}
			MPI_Comm_free(&shrinked_world);
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error en MPI_Comm_spawn [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		if(shrinked_world_rank<dead_list_size){
			old_world_data[1]=mpid->world_size;
			for(i=shrinked_world_rank;i<dead_list_size;i+=shrinked_world_size){
				old_world_data[0]=dead_list[i];
				ret=safe_send_times((void *)old_world_data,data_size,MPI_INT,i,inter_world,MAX_TRY);
				if(ret!=MPI_SUCCESS){
					fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de rank/size a nuevo proceso. Continuando [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
					continue;
				}
				restored++;
			}
			if(restored==0){
				MPIX_Comm_revoke(inter_world);
				MPI_Comm_free(&inter_world);
				MPI_Comm_free(&shrinked_world);
				fprintf(stderr,"Proceso [%d] en [%s]: Error en envio de rank/size a nuevos procesos [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
				return(ret);
			}
		}
	}
//	printf("Proceso [%d] en [%s]: MPI_Intercomm_merge\n",mpid->world_rank,mpid->processor_name);
	ret=MPI_Intercomm_merge(inter_world,1,&merged_world);
	rflag=flag=(ret==MPI_SUCCESS);
	MPI_Comm_set_errhandler(merged_world,MPI_ERRORS_RETURN);
	MPIX_Comm_agree(shrinked_world,&flag);
	if(!mpid->spawned)
		MPI_Comm_free(&shrinked_world);
	MPIX_Comm_agree(inter_world,&rflag);
	MPI_Comm_free(&inter_world);
	if(!(flag && rflag)){
		if(ret==MPI_SUCCESS)
			MPI_Comm_free(&merged_world);
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error en MPI_Intercomm_merge [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		return(ret);
	}
//	printf("Proceso [%d] en [%s]: MPI_Comm_split\n",mpid->world_rank,mpid->processor_name);
	ret=MPI_Comm_split(merged_world,1,mpid->world_rank,&new_world);
	flag=(ret==MPI_SUCCESS);
	MPIX_Comm_agree(merged_world,&flag);
	MPI_Comm_set_errhandler(new_world,MPI_ERRORS_RETURN);
	MPI_Comm_free(&merged_world);
	if(!flag){
		if(ret==MPI_SUCCESS)
			MPI_Comm_free(&new_world);
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error en MPI_Comm_split [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		return(ret);
	}
	MPI_Comm_get_errhandler(mpid->world,&errh);
	MPI_Comm_set_errhandler(new_world,errh);
	mpid->world=new_world;
	return(MPI_SUCCESS);
}

int *dead_list(MPI_data *mpid,int *size){
	MPI_Group world_group;
	MPI_Group dead_group;
	int dead_group_size;
	int i;
	MPIX_Comm_failure_ack(mpid->world);
	MPIX_Comm_failure_get_acked(mpid->world,&dead_group);
	MPI_Group_size(dead_group,&dead_group_size);
	MPI_Comm_group(mpid->world,&world_group);
	int *dead_group_list=(int *)calloc(dead_group_size,sizeof(int));
	int *dead_list_final=(int *)calloc(dead_group_size,sizeof(int));
	for(i=0;i<dead_group_size;i++)
		dead_group_list[i]=i;
	MPI_Group_translate_ranks(dead_group,dead_group_size,dead_group_list,world_group,dead_list_final);
	free(dead_group_list);
	MPI_Group_free(&world_group);
	MPI_Group_free(&dead_group);
	*size=dead_group_size;
	return(dead_list_final);
}

int *survivor_list(MPI_data *mpid,int *dead_list,int dead_list_size,int *size){
	int surv_size=mpid->world_size-dead_list_size;
	int i,j;
	int *surv_list=(int *)calloc(mpid->world_size,sizeof(int));
	int *surv_list_final=(int *)calloc(surv_size,sizeof(int));
	memset(surv_list,0,mpid->world_size*sizeof(int));
	for(i=0;i<dead_list_size;i++)
		surv_list[dead_list[i]]=1;
	for(i=0,j=0;i<mpid->world_size;i++){
		if(surv_list[i]==0){
			surv_list_final[j]=i;
			j++;
		}
	}
	free(surv_list);
	*size=surv_size;
	return(surv_list_final);
}
#endif /* _OMPI_ */

