#include "util.h"

int get_block_index(int Mb,int Nb,int M,int N,int n_block){
	int m=(n_block*Mb)%M;
	int n=(((int)(n_block/(((int)ceil(M/Mb))%M)))%N)*Nb;
	return(n*M+m);
}

int min(int a,int b){
	return((a<b) ? a : b);
}

int max(int a,int b){
	return((a>b) ? a : b);
}

