#ifndef _TIMERS_H_
#define _TIMERS_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#include <mpi-ext.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

void init_timers(int size);
void free_timers(int size);

double time_exec;
double time_tslu;
double time_tsqr;
double time_tsch;
double time_mult;
double time_solv;
double time_inv;
double time_add;
double time_swap;
double time_rest;
double time_comm;
double time_copy;
double time_step;
double time_exec_final;
double time_tslu_final;
double time_tsqr_final;
double time_tsch_final;
double time_mult_final;
double time_solv_final;
double time_inv_final;
double time_add_final;
double time_swap_final;
double time_rest_final;
double time_comm_final;
double time_copy_final;
double *time_steps;

double time_calu;
double time_caqr;
double time_cach;
double time_exec_tm;
double time_read;
double time_write;
double time_calu_final;
double time_caqr_final;
double time_cach_final;
double time_exec_tm_final;
double time_read_final;
double time_write_final;

#endif /* _TIMERS_H_ */

