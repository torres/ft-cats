#ifndef _UTIL_H_
#define _UTIL_H_

#include "timers.h"

#define MAX_CONCAT_SIZES	2
#define MAX_MSG_SIZE		2048
#define WAIT_TIME			10000

int get_block_index(int Mb,int Nb,int M,int N,int n_block);
int min(int a,int b);
int max(int a,int b);

#endif /* _UTIL_H_ */

