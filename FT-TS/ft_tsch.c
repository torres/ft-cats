#include "ft_tsch.h"

int ft_tsch_init(fttsch_data *ftd,double *A,int M,int N,char *in,int argc,char **argv){
	MPI_data *mpid=ftd->mpid;
	ftd->md=matrix_data_init_with_matrix(M,N,M/ftd->mpid->world_size,N,A,1,'L');
	if(ftd->md==NULL){
		fprintf(stderr,"Proceso [%d] en [%s]: TSCH - Error al inicializar panel FT-TSCH\n",mpid->world_rank,mpid->processor_name);
		return(EXIT_FAILURE);
	}
	init_variables_ch(mpid->world_size);
	return(MPI_SUCCESS);
}

int ft_tsch(fttsch_data *ftd){
	MPI_data *mpid=ftd->mpid;
	matrix_data *md=ftd->md;
	time_exec=MPI_Wtime();
	matrix_data *md_sub=NULL;
	matrix_data *md_subR=NULL;
	double *subblock;
	int index;
	int ret=0,i;
	int flag;
	int calculated=0;

	int working_block[mpid->world_size];
	for(i=0;i<mpid->world_size;i++)
		working_block[i]=i;
#ifndef _OMPI_
#ifdef _VERB_
	char msg[1024];
	char temp[1024];
#endif /* _VERB_ */
	char err[MPI_MAX_ERROR_STRING];
	int err_len;
	int indexS;
	int dead_list_size;
	int surv_list_size;
	int *dead_rank_list;
	int *surv_rank_list;
	if(mpid->spawned){
		time_rest=MPI_Wtime();
		mpid->spawned=!mpid->spawned;
		time_rest_final+=MPI_Wtime()-time_rest;
	}
#endif /* _OMPI_ */
//	PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO
	index=get_block_index(md->Mb,md->Nb,md->M,md->N,working_block[mpid->world_rank]);
//	INTENTA	EJECUTAR UN PASO DEL ALGORITMO
	while(1){
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: TSCH - Ejecutando paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
#endif /* _VERB_ */
/*		if(mpid->world_rank==1 && mpid->step==0 && !mpid->spawned){
			mpid->spawned=!mpid->spawned;
			printf("Proceso [%d] en [%s]: TSCH - Muriendo...\n",mpid->world_rank,mpid->processor_name);
			raise(SIGKILL);
		}*/
		if(mpid->step_finished)
			ret=MPI_SUCCESS;
		else{
			subblock=(double *)calloc(md->Mb*md->Nb,sizeof(double));
//			OBTENER EL PRIMER SUBBLOQUE A PARTIR DEL ELEMENTO index
			get_sub_block(md->A,md->M,index,subblock,md->Mb,md->Nb);
//			INICIALIZA EL SUBBLOQUE A PROCESAR LOCALMENTE
			md_sub=matrix_data_init_with_matrix(md->Mb,md->Nb,0,0,subblock,0,'L');
			free(subblock);
/*
			usleep(WAIT_TIME*mpid->world_rank);
			printf("Proceso [%d] en [%s]: TSCH - md_sub\n",mpid->world_rank,mpid->processor_name);
			print_matrix(md_sub->A,md_sub->M,md_sub->N);
*/
//			SI EL PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO LOCAL Y DESTINO ES IGUAL
			if(index==0){
//				EJECUTA EL PRIMER PASO
				time_step=MPI_Wtime();
				if(!calculated){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: TSCH - Calculando diagonal principal\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
					ch_matrix(md_sub->A,md_sub->N);
					calculated=1;
				}
				ret=send_broadcast(mpid,md_sub);
				time_steps[mpid->step]+=MPI_Wtime()-time_step;
/*
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: TSCH - md_sub\n",mpid->world_rank,mpid->processor_name);
				print_matrix(md_sub->A,md_sub->M,md_sub->N);
*/
			}
			else{
				time_step=MPI_Wtime();
				md_subR=receive_broadcast(mpid,md_sub);
				if(md_subR!=NULL){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: TSCH - Calculando subbloque\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
//					CALCULA EL VALOR L
					solve_matrix(md_sub->A,md_subR->A,md_sub->M,md_sub->N);
//					LIBERA RECURSOS
					matrix_data_free(md_subR);
					ret=MPI_SUCCESS;
				}
				time_steps[mpid->step]+=MPI_Wtime()-time_step;
/*
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: TSCH - md_subR\n",mpid->world_rank,mpid->processor_name);
				print_matrix(md_subR->A,md_subR->M,md_subR->N);
*/
/*
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: TSCH - md_sub\n",mpid->world_rank,mpid->processor_name);
				print_matrix(md_sub->A,md_sub->M,md_sub->N);
*/
			}
		}
		mpid->step_finished=flag=(ret==MPI_SUCCESS);
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: TSCH - Ejecutado paso [%d] - ret:[%d]\n",mpid->world_rank,mpid->processor_name,mpid->step,ret);
#endif /* _VERB_ */
#ifndef _OMPI_
		MPIX_Comm_agree(mpid->world,&flag);
#else
		if(!flag){
			fprintf(stderr,"Proceso [%d] en [%s]: TSCH - Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
			return(ret);
		}
		MPI_Barrier(mpid->world);
#endif /* _OMPI_ */
//		SI NO OCURRIO ERROR, TERMINA EL PASO DEL ALGORITMO
		if(flag){
			backup_results_alone(mpid,md,md_sub,index);
			break;
		}
#ifndef _OMPI_
		time_rest=MPI_Wtime();
		fprintf(stderr,"Proceso [%d] en [%s]: TSCH - Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
		while(1){
//			CADA PROCESO OBTIENE UNA LISTA DE PROCESOS FALLIDOS
			dead_rank_list=dead_list(mpid,&dead_list_size);
//			CADA PROCESO OBTIENE UNA LISTA DE PROCESOS SOBREVIVIENTES
			surv_rank_list=survivor_list(mpid,dead_rank_list,dead_list_size,&surv_list_size);
#ifdef _VERB_
			sprintf(msg,"Proceso [%d] en [%s]: TSCH - Procesos fallidos: ",mpid->world_rank,mpid->processor_name);
			for(i=0;i<dead_list_size;i++){
				sprintf(temp,"[%d]",dead_rank_list[i]);
				strcat(msg,temp);
			}
			usleep(WAIT_TIME*mpid->world_rank);
			printf("%s\n",msg);
			sprintf(msg,"Proceso [%d] en [%s]: TSCH - Procesos sobrevivientes: ",mpid->world_rank,mpid->processor_name);
			for(i=0;i<surv_list_size;i++){
				sprintf(temp,"[%d]",surv_rank_list[i]);
				strcat(msg,temp);
			}
			usleep(WAIT_TIME*mpid->world_rank);
			printf("%s\n",msg);
#endif /* _VERB_ */
//			RESTAURA A LOS PROCESOS FALLIDOS
			ret=replace_partners(mpid,dead_rank_list,dead_list_size);
//			SI LA RESTAURACION FALLO, INTENTA DE NUEVO
			if(ret!=MPI_SUCCESS){
				free(dead_rank_list);
				free(surv_rank_list);
				MPI_Error_string(ret,err,&err_len);
				fprintf(stderr,"Proceso [%d] en [%s]: TSCH - Error al restaurar procesos [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
				continue;
			}
			break;
		}
//		SE BUSCA LA POSICION EN LA LISTA DE SOBREVIVIENTES
		indexS=0;
		while(mpid->world_rank!=surv_rank_list[indexS]){
			indexS++;
		}
//		SI ME CORRESPONDE ENVIAR DATOS A UN PROCESO FALLIDO
		if(indexS<dead_list_size){
			for(i=indexS;i<dead_list_size;i+=surv_list_size){
				printf("Proceso [%d] en [%s]: TSCH - Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
			}
		}
		free(surv_rank_list);
		free(dead_rank_list);
		time_rest_final+=MPI_Wtime()-time_rest;
#endif /* _OMPI_ */
	}
	mpid->step_finished=0;
	broadcast_block(mpid,md,md_sub);
	time_exec_final+=MPI_Wtime()-time_exec;
//	LIBERA RECURSOS
	matrix_data_free(md_sub);
	return(ret);
}

int send_broadcast(MPI_data *mpid,matrix_data *md_sub){
	MPI_Status stats[mpid->world_size-1];
	MPI_Request reqs[mpid->world_size-1];
	int finished[mpid->world_size-1];
	int count=0;
	int ret,i;
	time_comm=MPI_Wtime();
	for(i=1;i<mpid->world_size;i++){
		ret=MPI_Isend((void *)md_sub->A,md_sub->M*md_sub->N,MPI_DOUBLE,i,i,mpid->world,&reqs[count]);
		count++;
		if(ret!=MPI_SUCCESS){
			fprintf(stderr,"Proceso [%d] en [%s]: TSCH - Error en broadcast a proceso [%d]\n",mpid->world_rank,mpid->processor_name,i);
			continue;
		}
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: TSCH - Broadcast enviado a [%d]\n",mpid->world_rank,mpid->processor_name,i);
#endif /* _VERB_ */
	}
#ifdef _VERB_
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: TSCH - Esperando finalizacion de broadcast\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
	ret=MPI_Waitsome(mpid->world_size-1,reqs,&count,finished,stats);
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSCH - Error en espera de finalizacion de broadcast\n",mpid->world_rank,mpid->processor_name);
		return(ret);
	}
	time_comm_final+=MPI_Wtime()-time_comm;
	return(ret);
}

matrix_data *receive_broadcast(MPI_data *mpid,matrix_data *md_sub){
	MPI_Request request;
	matrix_data *md_subR=matrix_data_init(md_sub->M,md_sub->N,0,0,0,'L');
	time_comm=MPI_Wtime();
	int ret=MPI_Irecv((void *)md_subR->A,md_subR->M*md_subR->N,MPI_DOUBLE,0,mpid->world_rank,mpid->world,&request);
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSCH - Error en broadcast\n",mpid->world_rank,mpid->processor_name);
		return(NULL);
	}
#ifdef _VERB_
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: TSCH - Esperando broadcast\n",mpid->world_rank,mpid->processor_name);
#endif /* _VERB_ */
	ret=MPI_Wait(&request,&(mpid->status));
	if(ret!=MPI_SUCCESS){
		fprintf(stderr,"Proceso [%d] en [%s]: TSCH - Error en espera de broadcast\n",mpid->world_rank,mpid->processor_name);
		return(NULL);
	}
	time_comm_final+=MPI_Wtime()-time_comm;
#ifdef _VERB_
	usleep(WAIT_TIME*mpid->world_rank);
	printf("Proceso [%d] en [%s]: TSCH -mpid->status.MPI_SOURCE:[%d]\n",mpid->world_rank,mpid->processor_name,mpid->status.MPI_SOURCE);
#endif /* _VERB_ */
	return(md_subR);
}

void broadcast_block(MPI_data *mpid,matrix_data *md,matrix_data *md_sub){
	int i,ret;
	int index;
	double *sub=(double *)malloc(md_sub->M*md_sub->N*sizeof(double));
	for(i=0;i<mpid->world_size;i++){
		time_comm=MPI_Wtime();
		ret=MPI_Bcast((i==mpid->world_rank) ? md_sub->A : sub,md_sub->M*md_sub->N,MPI_DOUBLE,i,mpid->world);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret!=MPI_SUCCESS)
			return;
		MPI_Barrier(mpid->world);
		index=get_block_index(md_sub->M,md_sub->N,md->M,md->N,i);
		set_sub_block(md->A,md->M,index,(i==mpid->world_rank) ? md_sub->A : sub,md_sub->M,md_sub->N);
	}
	free(sub);
}

int write_L(double *A,int M,int N,int size,int rank,int step){
	char filename[MAX_MSG_SIZE];
	sprintf(filename,"[%s][%dx%d][%d]_Process[%d]_Step[%d]_Backup.dump","FT-TSCH_L",M,N,size,rank,step);
	FILE *out=fopen(filename,"w");
	if(out==NULL){
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
		return(EXIT_FAILURE);
	}
	else{
		time_write=MPI_Wtime();
		fwrite(A,sizeof(double),M*N,out);
		time_write_final+=MPI_Wtime()-time_write;
		fclose(out);
	}
	return(MPI_SUCCESS);
}

int read_L(double *A,int M,int N,int size,int rank,int step){
	char filename[MAX_MSG_SIZE];
	sprintf(filename,"[%s][%dx%d][%d]_Process[%d]_Step[%d]_Backup.dump","FT-TSCH_L",M,N,size,rank,step);
	FILE *in=fopen(filename,"r");
	if(in==NULL){
		fprintf(stderr,"Error al leer el archivo [%s]\n",filename);
		return(EXIT_FAILURE);
	}
	else{
		time_read=MPI_Wtime();
		fread(A,sizeof(double),M*N,in);
		time_read_final+=MPI_Wtime()-time_read;
		fclose(in);
	}
	return(MPI_SUCCESS);
}

void backup_results_alone(MPI_data *mpid,matrix_data *md,matrix_data *md_sub,int index){
//	RESPALDA LOS RESULTADOS DE L
	set_sub_block(md->A,md->M,index,md_sub->A,md_sub->M,md_sub->N);
//	RESPALDA L EN ARCHIVO LOCAL
	int ret=!MPI_SUCCESS;
	while(ret!=MPI_SUCCESS){
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		printf("Proceso [%d] en [%s]: TSCH - Respaldando resultado:[%dx%d][%d/%d/%d]\n",mpid->world_rank,mpid->processor_name,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,mpid->step);
#endif /* _VERB_ */
		ret=write_L(md_sub->A,md_sub->M,md_sub->N,mpid->world_size,mpid->world_rank,mpid->step);
	}
}

void free_fttsch_data(fttsch_data *ftd){
	if(ftd->md!=NULL)
		matrix_data_free(ftd->md);
	if(ftd->mpid!=NULL)
		MPI_data_free(ftd->mpid);
	if(ftd!=NULL)
		free(ftd);
}

void init_variables_ch(int world_size){
	step_lim=((int)ceil(log2(world_size)))+1;
}

