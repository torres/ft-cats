#ifndef _LAPACK_H_
#define _LAPACK_H_

#include "util.h"
#include "matrix_util.h"

void lu_matrix(double *A,int M,int N,int *IPIV);
void qr_matrix(double *A,int M,int N,double *TAU,double *WORK);
void ch_matrix(double *A,int N);

void mult_matrix(double *A,int MA,int NA,double *B,int MB,int NB,double *C,int index);
void mult_matrix_trans(double *A,int MA,int NA,double *B,int MB,int NB,double *C,int index,char TRANSA,char TRANSB);
void mult_matrix_scalar(double *A,int MA,int NA,double scalar);
void solve_matrix(double *A,double *U,int M,int N);
void inv_matrix(double *A,int M,int N,int *IPIV);
void add_matrix(double *A,double *B,int M,int N,char op);
void rand_matrix(double *A,int M,int N,int r);
double *rand_symmetric_matrix(int M,int N,int r);
void swap_sub_block(double *A,int MA,int index,int M,int N,int *IPIV,int INCX);
int random_number();

void dgetrf_(int *M,int *N,double *A,int *LDA,int *IPIV, int *INFO);
void dgeqrf_(int *M,int *N,double *A,int *LDA,double *TAU,double *WORK,int *LWORK,int *INFO);
void dpotrf_(char *UPLO,int *N,double *A,int *LDA,int *INFO);
void dgemm_(char *TRANSA,char *TRANSB,int *M,int *N,int *K,double *ALPHA,double *A,int *LDA,double *B,int *LDB,double *BETA,double *C,int *LDC);
void dtrsm_(char *SIDE,char *UPLO,char *TRANSA,char *DIAG,int *M,int *N,double *ALPHA,double *A,int *LDA,double *B,int *LDB);
void dgetri_(int *N,double *A,int *LDA,int *IPIV,double *WORK,int *LWORK,int *INFO);
void daxpy_(int *N,double *DA,double *DX,int *INCX,double *DY,int *INCY);
void dscal_(int *N,double *DA,double *DX,int *INCX);
void dlarnv_(int *IDIST,int *ISEED,int *N,double *X);
void dlaswp_(int *N,double *A,int *LDA,int *K1,int *K2,int *IPIV,int *INCX);

#endif /* _LAPACK_H_ */

