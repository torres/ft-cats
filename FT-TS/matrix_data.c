#include "matrix_data.h"

matrix_data *matrix_data_init_with_random(int M,int N,int Mb,int Nb,int rank,int copy,char fact){
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy,fact);
	rand_matrix(m->A,M,N,rank);
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init_with_random_symmetric(int M,int N,int Mb,int Nb,int rank,int copy){
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy,'L');
	if(M!=N){
		rand_matrix(m->A,M,N,rank);
		int min_md=min(M,N);
		double *A=rand_symmetric_matrix(min_md,min_md,rank);
		set_sub_block(m->A,m->M,0,A,min_md,min_md);
		free(A);
	}
	else
		m->A=rand_symmetric_matrix(M,N,rank);
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init_with_file(int M,int N,int Mb,int Nb,char *file,int copy,char fact){
	FILE *in=fopen(file,"r");
	if(in==NULL)
		return(NULL);
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy,fact);
	int i,j;
	for(i=0;i<m->M;i++)
		for(j=0;j<m->N;j++)
			fscanf(in,"%lf",&(m->A[j*m->M+i]));
	fclose(in);
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init_with_file_block(int M,int N,int Mb,int Nb,char *file,int copy,int block,char fact){
	FILE *in=fopen(file,"r");
	if(in==NULL){
		printf("errno:[%d][%s]\n",errno,strerror(errno));
		return(NULL);
	}
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy,fact);
	int i,j;
	for(i=0;i<m->M;i++)
		for(j=0;j<m->N;j++)
			fscanf(in,"%lf",&(m->A[j*m->M+i]));
	fclose(in);
	if(block>=0){
		matrix_data *m2=matrix_data_init(Mb,Nb,Mb,Nb,copy,fact);
		get_sub_block(m->A,m->M,get_block_index(Mb,Nb,M,N,block),m2->A,m2->M,m2->N);
		if(copy)
			set_A_init(m2);
		matrix_data_free(m);
		return(m2);
	}
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init_with_file_symmetric(int M,int N,int Mb,int Nb,char *file,int copy,int block){
	FILE *in=fopen(file,"r");
	if(in==NULL){
		printf("errno:[%d][%s]\n",errno,strerror(errno));
		return(NULL);
	}
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy,'L');
	fread(m->A,sizeof(double),m->M*m->N,in);
	fclose(in);
	if(block>=0){
		matrix_data *m2=matrix_data_init(Mb,Nb,Mb,Nb,copy,'L');
		get_sub_block(m->A,m->M,get_block_index(Mb,Nb,M,N,block),m2->A,m2->M,m2->N);
		if(copy)
			set_A_init(m2);
		matrix_data_free(m);
		return(m2);
	}
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init_with_matrix(int M,int N,int Mb,int Nb,double *A,int copy,char fact){
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy,fact);
	memcpy(m->A,A,M*N*sizeof(double));
//	free(m->A);
//	m->A=A;
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init(int M,int N,int Mb,int Nb,int copy,char fact){
	matrix_data *m=(matrix_data *)calloc(1,sizeof(matrix_data));
	m->M=M;
	m->N=N;
	m->A=(double *)calloc(m->M*m->N,sizeof(double));
	if(copy)
		m->A_init=(double *)calloc(m->M*m->N,sizeof(double));
	m->LDA=m->M;
	if(fact=='U'){
		m->IPIV=(int *)calloc(min(m->M,m->N),sizeof(int));
		m->U=(double *)calloc(min(m->M,m->N)*min(m->M,m->N),sizeof(double));
	}
	else if(fact=='R'){
		m->TAU=(double *)calloc(min(m->M,m->N),sizeof(double));
		m->WORK=(double *)calloc(min(m->M,m->N),sizeof(double));
		m->Hv=(double *)calloc(max(m->M,m->N)*min(m->M,m->N),sizeof(double));
		m->T=(double *)calloc(min(m->M,m->N)*min(m->M,m->N),sizeof(double));
		m->R=(double *)calloc(max(m->M,m->N)*min(m->M,m->N),sizeof(double));
	}
	m->INFO=0;
	m->Mb=Mb;
	m->Nb=Nb;
	return(m);
}

void matrix_data_free(matrix_data *md){
	if(md->R!=NULL)
		free(md->R);
	if(md->T!=NULL)
		free(md->T);
	if(md->Hv!=NULL)
		free(md->Hv);
	if(md->U!=NULL)
		free(md->U);
	if(md->WORK!=NULL)
		free(md->WORK);
	if(md->TAU!=NULL)
		free(md->TAU);
	if(md->IPIV!=NULL)
		free(md->IPIV);
	if(md->A_init!=NULL)
		free(md->A_init);
	if(md->A!=NULL)
		free(md->A);
	if(md!=NULL)
		free(md);
}

void set_U(matrix_data *md){
	int i,j,min_md=min(md->M,md->N);
	time_copy=MPI_Wtime();
	for(i=0;i<min_md;i++)
		for(j=0;j<min_md;j++)
			md->U[j*min_md+i]=(j>=i) ? md->A[j*md->M+i] : 0;
	time_copy_final+=MPI_Wtime()-time_copy;
}

void set_R(matrix_data *md){
	int i,j;
	int max_md=max(md->M,md->N),min_md=min(md->M,md->N);
	time_copy=MPI_Wtime();
	for(i=0;i<max_md;i++)
		for(j=0;j<min_md;j++)
			md->R[j*max_md+i]=(j>=i) ? md->A[j*max_md+i] : 0;
	time_copy_final+=MPI_Wtime()-time_copy;
}

void set_Hv(matrix_data *md){
	int i,j;
	int max_md=max(md->M,md->N),min_md=min(md->M,md->N);
	time_copy=MPI_Wtime();
	for(i=0;i<max_md;i++)
		for(j=0;j<min_md;j++)
			md->Hv[j*max_md+i]=(j<i) ? md->A[j*max_md+i] : (j==i) ? 1 : 0;
	time_copy_final+=MPI_Wtime()-time_copy;
}

void set_Hv_in_matrix(matrix_data *md,double *Hv){
	int i,j;
	int max_md=max(md->M,md->N),min_md=min(md->M,md->N);
	time_copy=MPI_Wtime();
	for(i=0;i<max_md;i++)
		for(j=0;j<min_md;j++)
			Hv[j*max_md+i]=(j<i) ? md->A[j*max_md+i] : (j==i) ? 1 : 0;
	time_copy_final+=MPI_Wtime()-time_copy;
}

void set_A(matrix_data *md,int src){
	int i,j,min_md=min(md->M,md->N);
	time_copy=MPI_Wtime();
	for(i=0;i<md->M;i++)
		for(j=0;j<md->N;j++)
			md->A[j*md->M+i]=(j<i && src==0) ? 0 : (j<i && src==1) ? md->A_init[j*md->M+i] : md->U[j*min_md+i];
	time_copy_final+=MPI_Wtime()-time_copy;
}

void set_A_init(matrix_data *md){
	time_copy=MPI_Wtime();
	memcpy(md->A_init,md->A,md->M*md->N*sizeof(double));
	time_copy_final+=MPI_Wtime()-time_copy;
}

void add_Hv_to_matrix(matrix_data *md){
	int i,j;
	time_copy=MPI_Wtime();
	for(j=0;j<md->N;j++)
		for(i=j+1;i<md->M;i++)
			md->A[j*md->M+i]=md->Hv[j*md->M+i];
	time_copy_final+=MPI_Wtime()-time_copy;
}

void print_matrix_data(matrix_data *md,char fact){
	int i,j;
	int max_md=max(md->M,md->N),min_md=min(md->M,md->N);
	for(i=0;i<md->M;i++){
		for(j=0;j<md->N;j++){
			if(md->A[j*md->M+i]>=0.0)
				printf("+");
			printf("%.4lf ",md->A[j*md->M+i]);
		}
		if(i<min_md){
			printf("\t|\t");
			if(fact=='U'){
				for(j=0;j<min_md;j++){
					if(md->U[j*min_md+i]>=0.0)
						printf("+");
					printf("%.4lf ",md->U[j*min_md+i]);
				}
			}
			else if(fact=='R'){
				for(j=0;j<min_md;j++){
					if(md->R[j*max_md+i]>=0.0)
						printf("+");
					printf("%.4lf ",md->R[j*max_md+i]);
				}
			}
		}
		printf("\n");
	}
}

