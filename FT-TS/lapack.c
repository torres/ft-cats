#include "lapack.h"

void lu_matrix(double *A,int M,int N,int *IPIV){
	int LDA=M;
	int INFO=0;
	time_tslu=MPI_Wtime();
	dgetrf_(&M,&N,A,&LDA,IPIV,&INFO);
	time_tslu_final+=MPI_Wtime()-time_tslu;
}

void qr_matrix(double *A,int M,int N,double *TAU,double *WORK){
	int LDA=M;
	int LWORK=N;
	int INFO=0;
	time_tsqr=MPI_Wtime();
	dgeqrf_(&M,&N,A,&LDA,TAU,WORK,&LWORK,&INFO);
	time_tsqr_final+=MPI_Wtime()-time_tsqr;
}

void ch_matrix(double *A,int N){
	char UPLO='L';
	int LDA=N;
	int INFO=0;
	time_tsch=MPI_Wtime();
	dpotrf_(&UPLO,&N,A,&LDA,&INFO);
	time_tsch_final+=MPI_Wtime()-time_tsch;
//	printf("INFO:[%d]\n",INFO);
}

void mult_matrix(double *A,int MA,int NA,double *B,int MB,int NB,double *C,int index){
	char TRANSA='N';
	char TRANSB='N';
	double ALPHA=1.0;
	double BETA=1.0;
	int LDAA=MA;
	int LDAB=MB;
	int LDAC=MA;
	time_mult=MPI_Wtime();
	dgemm_(&TRANSA,&TRANSB,&MA,&NB,&NA,&ALPHA,A,&LDAA,B+index,&LDAB,&BETA,C,&LDAC);
	time_mult_final+=MPI_Wtime()-time_mult;
}

void mult_matrix_trans(double *A,int MA,int NA,double *B,int MB,int NB,double *C,int index,char TRANSA,char TRANSB){
	double ALPHA=1.0;
	double BETA=1.0;
	int LDAA=(TRANSA=='N') ? MA : NA;
	int LDAB=(TRANSB=='N') ? MB : NB;
	int LDAC=(TRANSA=='N') ? MA : NA;
	time_mult=MPI_Wtime();
	dgemm_(&TRANSA,&TRANSB,&MA,&NB,&NA,&ALPHA,A,&LDAA,B+index,&LDAB,&BETA,C,&LDAC);
	time_mult_final+=MPI_Wtime()-time_mult;
}

void mult_matrix_scalar(double *A,int MA,int NA,double scalar){
	int MN=MA*NA;
	int INCX=1;
	time_mult=MPI_Wtime();
	dscal_(&MN,&scalar,A,&INCX);
	time_mult_final+=MPI_Wtime()-time_mult;
}

void solve_matrix(double *A,double *U,int M,int N){
	char SIDE='R';
	char UPLO='U';
	char TRANSA='N';
	char DIAG='N';
	double ALPHA=1.0;
	int LDA=M;
	int LDB=M;
	time_solv=MPI_Wtime();
	dtrsm_(&SIDE,&UPLO,&TRANSA,&DIAG,&M,&N,&ALPHA,U,&LDA,A,&LDB);
	time_solv_final+=MPI_Wtime()-time_solv;
}

void inv_matrix(double *A,int M,int N,int *IPIV){
	int LDA=M;
	int INFO=0;
	int LWORK=N*N;
	double *WORK=(double *)malloc(LWORK*sizeof(double));
	time_inv=MPI_Wtime();
	dgetri_(&N,A,&LDA,IPIV,WORK,&LWORK,&INFO);
	time_inv_final+=MPI_Wtime()-time_inv;
	free(WORK);
}

void add_matrix(double *A,double *B,int M,int N,char op){
	double DA=1.0;
	int INCX=1;
	int INCY=1;
	int MN=M*N;
	time_add=MPI_Wtime();
	if(op=='S')
		mult_matrix_scalar(B,M,N,-1.0f);
	daxpy_(&MN,&DA,A,&INCX,B,&INCY);
	time_add_final+=MPI_Wtime()-time_add;
}

void rand_matrix(double *A,int M,int N,int r){
	int seed[4]={0,0,0,2*r+1};
	int ONE=1;
	int MN=M*N;
	dlarnv_(&ONE,seed,&MN,A);
}

double *rand_symmetric_matrix(int M,int N,int r){
	int i,j;
	double *A=(double *)calloc(M*N,sizeof(double));
	double *At=(double *)calloc(M*N,sizeof(double));
	srand(time(NULL));
	rand_matrix(A,M,N,r+random_number()*rand());
	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			At[j*M+i]=A[i*M+j];
		}
	}
	add_matrix(A,At,M,N,'A');
	mult_matrix_scalar(At,M,N,0.5f);
	free(A);
	for(i=0;i<M;i++)
		At[i*M+i]+=M;
	return(At);
}

void swap_sub_block(double *A,int MA,int index,int M,int N,int *IPIV,int INCX){
	int LDA=M;
	int K1=1;
	int K2=min(M,N);
	double *sub=(double *)calloc(M*N,sizeof(double));
	get_sub_block(A,MA,index,sub,M,N);
	time_swap=MPI_Wtime();
	dlaswp_(&N,sub,&LDA,&K1,&K2,IPIV,&INCX);
	time_swap_final+=MPI_Wtime()-time_swap;
	set_sub_block(A,MA,index,sub,M,N);
	free(sub);
}

int random_number(){
	int i;
	int SIZE=100;
	int IDIST=1;
	int SEED_SIZE=4;
	int SEED[SEED_SIZE];
	int SEED_MAX=4096;
	int sum=0;
	for(i=0;i<SEED_SIZE;i++)
		SEED[i]=rand()%SEED_MAX;
	SEED[SEED_SIZE-1]=SEED[SEED_SIZE-1]+((SEED[SEED_SIZE-1]&1) ? 0 : 1);
	double *X=(double *)malloc(SIZE*sizeof(double));
	dlarnv_(&IDIST,SEED,&SIZE,X);
	for(i=0;i<SIZE;i++)
		sum+=(int)floor(X[i]*SIZE*SEED_MAX);
	free(X);
	return(sum%SIZE);
}

